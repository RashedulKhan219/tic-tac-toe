FROM openjdk:11-jdk-slim
#RUN addgroup -S spring && adduser -S spring -G spring
#USER spring:spring

ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} tic-tac-toe.jar
ENTRYPOINT ["java","-jar","/tic-tac-toe.jar"]
