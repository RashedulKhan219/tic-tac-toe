echo --- Pulling latest code ---
git pull

echo --- Building image ---
docker build --no-cache -t dev/tic-tac-toe .

echo --- Stoppping old app ---
docker kill tic-tac-toe
docker rm tic-tac-toe

echo --- Starting container ---
docker run -p 5050:8080 -d --name tic-tac-toe dev/tic-tac-toe

echo --- Done ---
