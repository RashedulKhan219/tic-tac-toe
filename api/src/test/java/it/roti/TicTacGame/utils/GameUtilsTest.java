package it.roti.TicTacGame.utils;

import it.roti.TicTacGame.model.Player;
import org.junit.Assert;
import org.junit.Test;

public class GameUtilsTest {


    @Test
    public void horizontalCheck() {
        // step 1: set up arguments
        boolean expected = true;
        Player[] row = {new Player("p1", 'X'), new Player("p1", 'X'), new Player("p1", 'X')};
        Player[] row1 = {new Player("p1", 'O'), new Player("p1", 'O'), new Player("p1", 'O')};
        // step 2: execute the method
        boolean result = GameUtils.horizontalCheck(row);
        boolean result1 = GameUtils.horizontalCheck(row1);
        // step 3: validate that the  test passed or failed
        Assert.assertEquals(expected, result);
        Assert.assertEquals(expected, result1);
    }

    @Test
    public void horizontalNotWinnerCheck() {
        // step 1: set up arguments
        boolean expected = false;
        Player[] row = {new Player("p1", 'O'), new Player("p1", 'O'), new Player("p1", 'X')};
        Player[] row1 = {new Player("p1", 'O'), new Player("p1", 'X'), new Player("p1", 'O')};
        Player[] row2 = {new Player("p1", 'X'), new Player("p1", 'O'), new Player("p1", 'O')};
        Player[] row3 = {new Player("p1", 'X'), new Player("p1", 'X'), new Player("p1", 'O')};
        Player[] row4 = {new Player("p1", 'O'), new Player("p1", 'X'), new Player("p1", 'X')};
        Player[] row5 = {new Player("p1", 'X'), new Player("p1", 'O'), new Player("p1", 'X')};

        // step 2: execute the method
        boolean result = GameUtils.horizontalCheck(row);
        boolean result1 = GameUtils.horizontalCheck(row1);
        boolean result2 = GameUtils.horizontalCheck(row2);
        boolean result3 = GameUtils.horizontalCheck(row3);
        boolean result4 = GameUtils.horizontalCheck(row4);
        boolean result5 = GameUtils.horizontalCheck(row5);

        // step 3: validate that the  test passed or failed
        Assert.assertEquals(expected, result);
        Assert.assertEquals(expected, result1);
        Assert.assertEquals(expected, result2);
        Assert.assertEquals(expected, result3);
        Assert.assertEquals(expected, result4);
        Assert.assertEquals(expected, result5);
    }

//
//    @Test
//    public void verticalCheck() {
//        // step 1: set up arguments
//        boolean expected = true;
//        Character[][] colArr = {{'x','o','x'},
//                {'x','o','x'},
//                {'x','o','x'}};
//        // step 2: execute the method
//        boolean result = GameUtils.verticalCheck(colArr, 0);
//        boolean result1 = GameUtils.verticalCheck(colArr, 1);
//        boolean result2 = GameUtils.verticalCheck(colArr, 2);
//        // step 3: validate that the  test passed or failed
//        Assert.assertEquals(expected, result);
//        Assert.assertEquals(expected, result1);
//        Assert.assertEquals(expected, result2);
//    }
//
//
//    @Test public void diagonalCheckOneTest() {
//        // Step 1: set up arguments
//        boolean expected = true;
//        Character[][] diagArray =  {{'o', ' ', ' '},
//                {' ', 'o', ' '},
//                {' ', ' ', 'o'}};
//        // Step 2: Execute method
//        boolean result = GameUtils.diagonalCheckOne(diagArray);
//        // Step 3: validation
//        Assert.assertEquals(expected, result);
//    }
//
//    @Test public void diagonalCheckOneNotWinnerTest() {
//        // Step 1: set up arguments
//        boolean expected = false;
//        Character[][] diagArray =  {{'o', ' ', ' '},
//                {' ', 'x', ' '},
//                {' ', ' ', 'o'}};
//        // Step 2: Execute method
//        boolean result = GameUtils.diagonalCheckOne(diagArray);
//        // Step 3: validation
//        Assert.assertEquals(expected, result);
//    }
//
//    @Test
//    public void diagonalCheckTwoTest() {
//        // Step 1: set up arguments
//        boolean expected = true;
//        Character[][] diagArray =  {{' ', ' ', 'x'},
//                {' ', 'x', ' '},
//                {'x', ' ', ' '}};
//        // Step 2: execute method
//        boolean result = GameUtils.diagonalCheckTwo(diagArray);
//
//        // validation
//        Assert.assertEquals(expected, result);
//    }
//
//    @Test
//    public void diagonalCheckTwoNotWinnerTest() {
//        // Step 1: set up arguments
//        boolean expected = false;
//        Character[][] diagArray =  {{' ', ' ', 'x'},
//                {' ', 'o', ' '},
//                {'x', ' ', ' '}};
//        // Step 2: execute method
//        boolean result = GameUtils.diagonalCheckTwo(diagArray);
//
//        // validation
//        Assert.assertEquals(expected, result);
//    }
}
