package it.roti.TicTacGame.service;

import it.roti.TicTacGame.model.Game;
import it.roti.TicTacGame.model.STATE;
import it.roti.TicTacGame.response.GameResponse;
import org.junit.Assert;
import org.junit.Test;

public class GameServiceTest {

    GameService gameService;

    @Test
    public void stateUpdateTest() {
        STATE expected = STATE.PLAY;
        //  step 1 - setup
        gameService = new GameService();
        GameResponse gameResponse = gameService.createGame();
        // step-2 make the calls
        gameService.joinGame("player1", gameResponse.getGameId());
        gameService.joinGame("player2", gameResponse.getGameId());
        // step 3 - validate
        Game game = gameService.findGame(gameResponse.getGameId());
        Assert.assertEquals(expected, game.getState());
    }
}
