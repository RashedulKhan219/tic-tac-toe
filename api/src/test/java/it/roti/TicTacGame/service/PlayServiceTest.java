package it.roti.TicTacGame.service;

import it.roti.TicTacGame.model.PlayRequest;
import it.roti.TicTacGame.model.Player;
import it.roti.TicTacGame.response.GameResponse;
import org.junit.Assert;
import org.junit.Test;

import java.util.UUID;

public class PlayServiceTest {
    @Test
    public void makeMoveTest() throws Exception {
        // Step 0: test  goal: to check it gives the right symbol after making a move
        // step 1: set up the arguments
        Character expected = 'X';
        GameService gameService = new GameService();
        GameResponse gameResponse = gameService.createGame();
        String uuid = gameResponse.getGameId();

        // step: execute the method
        PlayService playService = new PlayService(gameService);
        Player player1 = gameService.joinGame("player1", uuid);
        gameService.joinGame("player2", uuid);
        int row = 0;
        int col = 0;
        PlayRequest playRequest = new PlayRequest(row, col, player1.getPlayerId());
        GameResponse result = playService.makeMove(playRequest, uuid);

        // step 3: validate the method
        Player[][] gameBoardTest = result.getBoard();
        Player value = gameBoardTest[row][col];
        Assert.assertTrue(value.getSymbol().equals(expected));

    }
}
