package it.roti.TicTacGame.service;

import it.roti.TicTacGame.model.Game;
import it.roti.TicTacGame.model.Player;
import it.roti.TicTacGame.model.STATE;
import it.roti.TicTacGame.response.GameResponse;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class GameService {

    Map<String, Game> games = new HashMap<>();

    public GameResponse createGame() {
        Game game = new Game();
        games.put(game.getGameId(), game);
        return new GameResponse(game);
    }

    public Game findGame(String gameId) {
        return games.get(gameId);
    }


    public Player joinGame(String name, String gameId) {//Big O, n = gameId
        Game game = games.get(gameId);  // 1 step
        if (game == null) return null; // 1 step
        Player newPlayer; // 1 step
        if (game.getPlayer1() == null) { //
            newPlayer = new Player(name, 'X');
            game.setPlayer1(newPlayer);
        } else if (game.getPlayer2() == null) {
            newPlayer = new Player(name, 'O');
            game.setPlayer2(newPlayer);
            startPlaying(game);
        } else {
            System.err.println("cannot join game");
            return null;
        }
        return newPlayer;
    }

    private void startPlaying(Game game) {
        game.setState(STATE.PLAY);
        game.setTurn(game.getPlayer1());
    }

    public List<GameResponse> getGames() {//Big O
        List<GameResponse> result = new ArrayList<>();//
        for (Game game : games.values()) {// n = games.values.size
            result.add(new GameResponse(game)); // o(n) = n //
        }
        return result;
    }

    public GameResponse playRematch(String gameId) {
        //only rematch after game finish playing
        Game game = games.get(gameId);
        if (game.getState() == STATE.CLOSED) {// O(n) = n
            game.setState(STATE.PLAY);
            game.setWinner(null);
            game.setFull(false);
            game.setBoard(new Player[3][3]);
            return new GameResponse(game);
        }
        return null;
    }
}
