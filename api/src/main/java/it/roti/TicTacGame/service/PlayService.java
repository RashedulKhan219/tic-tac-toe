package it.roti.TicTacGame.service;

import it.roti.TicTacGame.model.Game;
import it.roti.TicTacGame.model.PlayRequest;
import it.roti.TicTacGame.model.Player;
import it.roti.TicTacGame.model.STATE;
import it.roti.TicTacGame.response.GameResponse;
import it.roti.TicTacGame.utils.GameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class PlayService {

    GameService gameService;

    @Autowired
    public PlayService(GameService gameService) {
        this.gameService = gameService;
    }

    public GameResponse makeMove(PlayRequest playRequest, String gameId) throws Exception {
        // Validation -> game exists ? playerID part of the game ? Spot open ? state playing ? which player's turn?
        Game game = gameService.findGame(gameId);
        int row = playRequest.getRow();
        int col = playRequest.getCol();

        if (!validateMakeMoveRequest(row, col, playRequest, game))
            return null;

        //  Request is VALID
        Player player = getPlayerFromGame(playRequest.getPlayerId(), game);
        makeMove(row, col, player, game);
        updatePlayerTurn(game);

        Player winner = GameUtils.checkWinner(game, row, col);
        if (winner != null) updateGameBoardWin(game, winner);

        if (GameUtils.isFull(game)) updateIsFull(game);

        return new GameResponse(game);
    }

    public void updateIsFull(Game game){
        game.setFull(true);
        game.setState(STATE.CLOSED);
    }

    // helper method
    public void updateGameBoardWin(Game game, Player winner) {
        System.out.println("updating winning state");
        game.setWinner(winner);
        game.setState(STATE.CLOSED);
    }


    public void makeMove(int row, int col, Player player, Game game) {
        game.board[row][col] = player;
    }

    //true means validation passed
    // false means validation failed
    public static boolean validateMakeMoveRequest(int row, int col, PlayRequest playRequest, Game game) {
        //  is there a game ?
        if (game == null) return false;

        //  Is Spot open ?
        Player[][] board = game.getBoard();
        if (board[row][col] != null) return false;

        //  Is the game in play state ?
        STATE state = game.getState();
        if (state != STATE.PLAY) return false;

        //  Is it your(playRequest) turn ?
        UUID playerTurn = game.getTurn().getPlayerId();
        if(!playerTurn.equals(playRequest.getPlayerId())) return false;

        return true;
    }

    public static Player getPlayerFromGame(UUID playerID, Game game) {
        UUID player1 = game.getPlayer1().getPlayerId();
        UUID player2 = game.getPlayer2().getPlayerId();
        if (player1.equals(playerID)) return game.getPlayer1();
        if (player2.equals(playerID)) return game.getPlayer2();
        return null;
    }
    public void updatePlayerTurn(Game game){
        Player turn = game.getTurn();
        if(turn.equals(game.getPlayer1())){
            game.setTurn(game.getPlayer2());

        }else{
            game.setTurn(game.getPlayer1());
        }
    }
}


