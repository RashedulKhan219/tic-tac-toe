package it.roti.TicTacGame;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TicTacGameApplication {

    public static void main(String[] args) {
        SpringApplication.run(TicTacGameApplication.class, args);


    }

}


