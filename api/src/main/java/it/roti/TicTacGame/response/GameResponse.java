package it.roti.TicTacGame.response;

import it.roti.TicTacGame.model.Game;
import it.roti.TicTacGame.model.Player;
import it.roti.TicTacGame.model.STATE;
import lombok.Data;


@Data
public class GameResponse {
    Player[][] board;
    STATE state;
    boolean isFull;
    Player player1, player2, winner, turn;
    String gameId;

    public GameResponse(Game game) {
        if (game == null) return;
        this.board = game.getBoard();
        this.state = game.getState();
        this.isFull = game.isFull();
        this.winner = game.getWinner();
        this.player1 = game.getPlayer1();
        this.player2 = game.getPlayer2();
        this.gameId = game.getGameId();
        this.turn = game.getTurn();
    }


}
