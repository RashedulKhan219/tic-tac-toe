package it.roti.TicTacGame.model;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.RandomStringUtils;

@Setter
@Getter
public class Game {
    public Player[][] board;
    STATE state;
    String gameId;
    Player player1;
    Player player2;
    Player turn;
    boolean isFull;
    Player winner;

    public Game(){

        this.gameId = RandomStringUtils.randomNumeric(4);
        this.board = new Player[3][3];
        this.state = STATE.OPEN;
    }






}
