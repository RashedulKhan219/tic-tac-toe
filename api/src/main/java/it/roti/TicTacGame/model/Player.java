package it.roti.TicTacGame.model;

import lombok.Data;

import java.util.UUID;

@Data
public class Player {
    String name;
    UUID playerId = UUID.randomUUID();
    Character symbol;

    public Player(String name, Character symbol) {
        this.name = name;
        this.symbol = symbol;

    }

}
