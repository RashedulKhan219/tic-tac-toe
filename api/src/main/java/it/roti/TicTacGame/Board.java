package it.roti.TicTacGame;

import it.roti.TicTacGame.model.STATE;

public class Board {
    STATE state = STATE.OPEN;
    public Character[][] gameBoard = new Character[3][3];


    @Override
    public String toString() {
        String printBoard = "";
        for (int row = 0; row < gameBoard.length; row++) {
            System.out.print("|");
            for (int col = 0; col < gameBoard[row].length; col++) {
                Character value = gameBoard[row][col];
                String valueString = (value == '\u0000' ? " " : value) + "|";
                System.out.print(valueString);
            }
            System.out.println();
        }
        return printBoard;
    }

    public boolean isFull() {
        for (int i = 0; i < gameBoard.length; i++) {
            gameBoard[0][0] = ' ';
            for (int j = 0; j < gameBoard[i].length; j++) {
                Character value = gameBoard[i][j];
                if (value == null) {
                    return false;
                }
            }
        }
        return true;
    }

}







