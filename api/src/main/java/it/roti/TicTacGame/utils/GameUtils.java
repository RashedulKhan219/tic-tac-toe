package it.roti.TicTacGame.utils;

import it.roti.TicTacGame.model.Game;
import it.roti.TicTacGame.model.Player;

public class GameUtils {

    public static Player checkWinner(Game game, int row, int col) {
        Player[][] boardArray = game.getBoard();
        boolean hWin = horizontalCheck(boardArray[row]);
        boolean vWin = verticalCheck(boardArray, col);
        boolean dWin1 = diagonalCheckOne(boardArray);
        boolean dWin2 = diagonalCheckTwo(boardArray);
        if (hWin || vWin || dWin1 || dWin2) {
            return boardArray[row][col];
        }
        return null;
    }

    public static boolean horizontalCheck(Player[] rowArray) {
        if (rowArray.length == 0) return false;
        Player firstSymbol = rowArray[0];
        for (Player c : rowArray) {
            if (c == null) return false;
            if (!c.equals(firstSymbol)) return false;
        }
        return true;
    }

    public static boolean verticalCheck(Player[][] colArray, int col) {
        if (colArray.length == 0) return false;
        Player[] flatten = new Player[colArray.length];
        for (int row = 0; row < colArray.length; row++) {
            flatten[row] = colArray[row][col];
        }
        return horizontalCheck(flatten);
    }

    public static boolean diagonalCheckOne(Player[][] diaArray) {
        if (diaArray.length == 0) return false;
        Player[] flatten = new Player[diaArray.length];
        for (int row = 0; row < diaArray.length; row++) {
            flatten[row] = diaArray[row][row];
        }

        return horizontalCheck(flatten);
    }

    public static boolean diagonalCheckTwo(Player[][] diaArray) {

        if (diaArray.length == 0) return false;
        Player[] flatten = new Player[diaArray.length];
        for (int row = 0; row < diaArray.length; row++) {
            for (int col = diaArray.length - 1; col >= 0; col--) {
                flatten[col] = diaArray[row][col];
                row++;
            }
        }
        return horizontalCheck(flatten);
    }

    public static boolean isFull(Game game) {
        for (int row = 0; row < game.getBoard().length; row++) {
            for (int col = 0; col < game.getBoard()[row].length; col++) {
                Player result = game.getBoard()[row][col];
                if (result == null) return false;
            }
        }
        return true;
    }

}


