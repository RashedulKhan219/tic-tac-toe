package it.roti.TicTacGame.api;

import it.roti.TicTacGame.model.PlayRequest;
import it.roti.TicTacGame.response.GameResponse;
import it.roti.TicTacGame.service.PlayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
@RequestMapping("/play")
public class PlayApi {
    @Autowired
    PlayService playService;

    @PostMapping("/{gameId}")
    public ResponseEntity<GameResponse> playGame(@RequestBody PlayRequest request, @PathVariable String gameId) throws Exception {
        GameResponse response = playService.makeMove(request, gameId);
        if (response == null) return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

}
