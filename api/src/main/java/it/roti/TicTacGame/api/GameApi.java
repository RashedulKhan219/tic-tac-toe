package it.roti.TicTacGame.api;

import it.roti.TicTacGame.model.Game;
import it.roti.TicTacGame.model.JoinRequest;
import it.roti.TicTacGame.model.Player;
import it.roti.TicTacGame.response.GameResponse;
import it.roti.TicTacGame.service.GameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/game")
public class GameApi {
    @Autowired
    GameService gameService;

    @GetMapping
    List<GameResponse> getGames() {
        return gameService.getGames();
    }

    @GetMapping("/{gameId}")
    ResponseEntity<GameResponse> getGame(@PathVariable String gameId){
        Game game = gameService.findGame(gameId);
        if (game == null) return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        GameResponse response = new GameResponse(gameService.findGame(gameId));
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PostMapping
    GameResponse createGame() {
        return gameService.createGame();
    }// Big(o) Time: O(n) = 1 / Space: 0(n) = 1

    @PutMapping("/{gameId}")
    Player joinGame(@RequestBody JoinRequest request, @PathVariable String gameId) {
        return gameService.joinGame(request.getName(), gameId);
    }

    @PutMapping("/rematch/{gameId}")
    public GameResponse playRematch(@PathVariable String gameId) {
        return gameService.playRematch(gameId);
    }


}

