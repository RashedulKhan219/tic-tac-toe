package GameProject.ChooseYourPath.service;

import GameProject.ChooseYourPath.model.Users;
import GameProject.ChooseYourPath.response.UserResponse;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
//creating a user
public class UserService {
    //creating a hashmap ,to our user data
    Map<UUID, Users> userData = new HashMap<>();
    //creating a method to create user and store inside hashmap
    public UserResponse createUser(Users user){
        if(!userData.isEmpty()){
        for(Users data:userData.values()){
            if(data.getEmailAddress().equals(user.getEmailAddress())){
                return null;
            }
        }
        }

        userData.put(user.getUserID(),user);
        return new UserResponse(user);
    }

    // creating a method to get users and their data and save it into a array list
    public List<UserResponse> getUsers(){
        List<UserResponse> userResponse = new ArrayList<>();
        for(Users user: userData.values())
            userResponse.add(new UserResponse(user));
        return userResponse;
    }

    public UserResponse userLogin(String email, String password){
        for(Users data:userData.values()){
            if(data.getEmailAddress().equals(email) && data.getPassword().equals(password)){

                return new UserResponse(data);
            }

        }

        return null;

    }

    public Users getUser(UUID userId){ ;
        return userData.get(userId);
    }


}
