package GameProject.ChooseYourPath.service;

import GameProject.ChooseYourPath.model.*;
import GameProject.ChooseYourPath.response.GameResponse;
import GameProject.ChooseYourPath.response.UserResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class GameService {
    @Autowired
    UserService service;
    Map<UUID, Game> games = new HashMap<>();

    public GameResponse createGame(UUID userId) {
        Goblin goblin = new Goblin();
        Game game = new Game(findUser(userId),goblin);
        game.setState(STATE.OPEN);
        games.put(game.getGameId(), game);
        //im going to add my main code decision choices the character can make
        return new GameResponse(game);
    }

    public GameResponse addQuest(UUID gameId) {
        Game game = games.get(gameId);
         game.setVillageQuest("Village Elder: Kill the Goblin and save our women and children!");
         game.setTowerQuest("Tower Guard: Obtain the Goblin head to enter the city");
         game.setState(STATE.INPROGRESS);
    return new GameResponse(game);
    }

    public Game getGame(UUID gameId) {
        return games.get(gameId);
    }

    public Users findUser(UUID userId){
        return service.userData.get(userId);
    }
    // change the hp give goblin atk and hp
    public Game fight(UUID gameId){
        Game game = games.get(gameId);

            int goblinHp = game.getGoblin().getGoblinHp();
            int playerHp = game.getPlayerHp();
            if (goblinHp !=0) {
                goblinHp = goblinHp - game.getPlayerDmg();
                playerHp = playerHp - game.getGoblin().getGoblinDmg();

            }
            game.getGoblin().setGoblinHp(goblinHp);
            game.setPlayerHp(playerHp);
            game.setState(STATE.COMPLETED);
        return game;
    }

    public GameResponse playGame(UUID gameId){
        Game fight = fight(gameId);
        return new GameResponse(fight);
    }

    public List<GameResponse> getGames() {
        List<GameResponse> gameResponse = new ArrayList<>();
        for(Game games: games.values())
            gameResponse.add(new GameResponse(games));
        return gameResponse;
    }
    // create state class which will allow me to go to few areas and choices
    // quest log
    
}

////    public Player createPlayer(UUID uuid,String name){
////        Player newPlayer = new Player(service.userData.get(uuid), name);
////        return newPlayer;
////    }
//
//    public GameResponse playGame(int choice){
//       switch (choice){
//           case 1: towerGate();
//           break;
//
//           case 2: village();
//           break;
//
//           case 3: wilderness();
//           break;
//
//           default:
//               return null;
//
//       }
//
//
//    }
//
//    private void wilderness(int choice) {
//        String story = "asdasdasdasdsadasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdadsdas";
//        switch (choice){
//            case 1: attack();
//                break;
//
//            case 2: towerGate();
//                break;
//
//            case 3: quit();
//                break;
//
//            default:
//                String message = "no choice was made";
//
//    }
//
//    private void village() {
//
//        }
//    }
//
//    private void attack() {
//    }
//
//    private void quit() {
//    }
//
//    private String towerGate() {
//        //Text saying
//    }
//}
