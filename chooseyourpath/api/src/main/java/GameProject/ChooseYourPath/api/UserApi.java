package GameProject.ChooseYourPath.api;

import GameProject.ChooseYourPath.model.Users;
import GameProject.ChooseYourPath.response.UserResponse;
import GameProject.ChooseYourPath.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping("/users")
public class UserApi {

    @Autowired
    UserService userService;

    //creating a signUp method to post
    @PostMapping
    public UserResponse signUp(@RequestBody Users newUser){
        return userService.createUser(newUser);
    }

    @GetMapping
    public List<UserResponse> getUsers(){
        return userService.getUsers();
    }

    @PostMapping("/{email}/{password}")
    public UserResponse userLogin(@PathVariable String email,@PathVariable String password){
        return userService.userLogin(email,password);
    }

    @GetMapping("/{userId}")
    public Users getUser(@PathVariable UUID userId){
        return userService.getUser(userId);
    }


}
