package GameProject.ChooseYourPath.api;

import GameProject.ChooseYourPath.model.Game;
import GameProject.ChooseYourPath.response.GameResponse;
import GameProject.ChooseYourPath.response.UserResponse;
import GameProject.ChooseYourPath.service.GameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping("/game")
public class GameApi {
    @Autowired
    GameService gameService;

    @PostMapping("/{userId}")
    public GameResponse createGame(@PathVariable UUID userId){
        return gameService.createGame(userId);
    }

    @PostMapping("/{play}/{gameId}")
    GameResponse playGame(@PathVariable UUID gameId){
        return  gameService.playGame(gameId);
    }

    @GetMapping
    public List<GameResponse> getGames(){
        return gameService.getGames();
    }

    @PutMapping("/{gameId}")
    public GameResponse addQuest(@PathVariable UUID gameId){
        return gameService.addQuest(gameId);
    }

    @GetMapping("/{gameId}")
    public Game getGame(@PathVariable UUID gameId){
        return gameService.getGame(gameId);
    }
}