package GameProject.ChooseYourPath.response;

import GameProject.ChooseYourPath.model.Game;
import GameProject.ChooseYourPath.model.Goblin;
import GameProject.ChooseYourPath.model.Quests;
import GameProject.ChooseYourPath.model.STATE;
import lombok.Data;

import java.util.UUID;

@Data
public class GameResponse {
   String userName;
   int playerHp;
   int playerDmg;
   int goblinHp;
   int goblinDmg;
    UUID gameId;
    String villageQuest;
    String towerQuest;
    STATE state;

    public GameResponse(Game game){
        this.userName = game.getUser().getUserName();
        this.playerHp = game.getPlayerHp();
        this.playerDmg = game.getPlayerDmg();
        this.goblinHp = game.getGoblin().getGoblinHp();
        this.goblinDmg = game.getGoblin().getGoblinDmg();
        gameId = game.getGameId();
        this.villageQuest = game.getVillageQuest();
        this.towerQuest = game.getTowerQuest();
        state = game.getState();

    }
}
