package GameProject.ChooseYourPath.response;

import GameProject.ChooseYourPath.model.Users;
import lombok.Data;

import java.util.UUID;
@Data
public class UserResponse {
    String userName;
    String firstName;
    String lastName;
    //char gender;
    String emailAddress;
    UUID userId;

    public UserResponse(Users user){
        userName = user.getUserName();
        firstName = user.getFirstName();
        lastName = user.getLastName();
        //gender = user.getGender();
        emailAddress = user.getEmailAddress();
        userId = user.getUserID();
    }

}
