package GameProject.ChooseYourPath.model;

import lombok.Data;

@Data
public class Quests {
    String villageQuest;
    String towerQuest;
}
