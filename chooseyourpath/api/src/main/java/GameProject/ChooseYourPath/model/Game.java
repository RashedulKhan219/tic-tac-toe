package GameProject.ChooseYourPath.model;

import lombok.Data;

import java.util.UUID;
@Data
public class Game {
    UUID gameId;
    Users user;
    int playerHp;
    int playerDmg;
    Goblin goblin;
    STATE state;
    String villageQuest;
    String towerQuest;

    public Game(Users user,Goblin goblin){
        this.user = user;
        this.gameId = UUID.randomUUID();
        playerHp=100;
        playerDmg=25;
        this.goblin = goblin;


    }
}
