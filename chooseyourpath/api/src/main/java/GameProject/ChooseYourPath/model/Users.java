package GameProject.ChooseYourPath.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.UUID;
@Data
@AllArgsConstructor
public class Users {
    String userName;
    String firstName;
    String lastName;
    String dateOfBirth;
    //char gender;
    String emailAddress;
    String password;
    UUID userID;

    public Users(){
        userID = UUID.randomUUID();


    }

}
