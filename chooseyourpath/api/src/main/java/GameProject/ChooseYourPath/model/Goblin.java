package GameProject.ChooseYourPath.model;

import lombok.Data;

import java.util.UUID;

@Data
public class Goblin {
    UUID goblinId = UUID.randomUUID();
    String goblinName = "WomenHunter";
    int goblinHp = 100;
    int goblinDmg=20;

}
