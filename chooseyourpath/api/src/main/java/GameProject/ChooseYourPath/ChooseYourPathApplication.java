package GameProject.ChooseYourPath;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ChooseYourPathApplication {

	public static void main(String[] args) {
		SpringApplication.run(ChooseYourPathApplication.class, args);
	}

}
