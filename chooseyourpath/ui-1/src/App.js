import React from 'react';
import './App.css';

import SignUp from './components/SignUp';
import { Switch, Route } from 'react-router-dom';
import Login from './components/Login';
import Wilderness from './components/Wilderness';
import TowerGate from './components/TowerGate';
import TowerGate2 from './components/TowerGate2';
import Village from './components/Village';
import Village2 from './components/Village2';
import Quit from './components/Quit';
import GamePage from './components/GamePage';
import Home from './components/Home';


function App() {
  return (
    <div className="App">
      
      <Switch>
        <Route path='/' exact component={Home} />
        <Route path='/signup' exact component={SignUp} />
        <Route path='/login' exact component={Login } />
        <Route path='/wilderness/:gameId' exact component={Wilderness} />
        <Route path='/towergate/:gameId' exact component={TowerGate} />
        <Route path='/towergate2' exact component={TowerGate2} />
        <Route path='/village/:gameId' exact component={Village} />
        <Route path='/village2' exact component={Village2} />
        <Route path='/quit' exact component={Quit} />
        <Route path='/gamepage/:userId' exact component={GamePage} />

        

      </Switch>

    </div>
  );
}

export default App;
