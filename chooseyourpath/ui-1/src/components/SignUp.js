import React, { useState } from "react";
import { Form, Button } from "react-bootstrap";
import UserApi from "./api/UserApi";
import Header from './Header';

const SignUp = ({ history }) => {
  const [user, setUser] = useState([]);
  const [userName, setUserName] = useState("");
  const [firstName, setFirstName] = useState("");
  const [lastName, setlastName] = useState("");
  const [emailAddress, setEmail] = useState("");

  //const [gender,setGender] = useState('')
  const [dateOfBirth, setDateOfBirth] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState("");

  const handleUser = (e) => {
    e.preventDefault();

    console.log(userName);

    UserApi.createUser(
      userName,
      firstName,
      lastName,
      emailAddress,
      dateOfBirth,
      password
    ).then((response) => {
      setUser(response);
      if (response) {
        history.push("/gamepage/" + response.userId);
      }
      setError("email already exists");
    });
  };

  return (
    <div>
      <Header/>
    <Form className="container" onSubmit={handleUser}>
      <Form.Label>User Name</Form.Label>
      <Form.Control
        onChange={(e) => setUserName(e.target.value)}
        type="text"
        placeholder="Enter user name"
      />
      <Form.Label>First Name</Form.Label>
      <Form.Control
        onChange={(e) => setFirstName(e.target.value)}
        type="text"
        placeholder="Enter first name"
      />
      <Form.Label>Last Name</Form.Label>
      <Form.Control
        onChange={(e) => setlastName(e.target.value)}
        type="text"
        placeholder="Enter last name"
      />
      <Form.Label>Email Address</Form.Label>
      <Form.Control
        onChange={(e) => setEmail(e.target.value)}
        type="email"
        placeholder="Enter email"
      />
      <Form.Label>Date of Birth</Form.Label>
      <Form.Control
        onChange={(e) => setDateOfBirth(e.target.value)}
        type="date"
        placeholder="Enter date of birth"
      />
      {/* <Form.Label>Gender</Form.Label>
    <Form.Control onChange={(e)=>setGender(e.target.value)}type="text" placeholder="Enter gender" /> */}
      <Form.Label>Password</Form.Label>
      <Form.Control
        onChange={(e) => setPassword(e.target.value)}
        type="password"
        placeholder="Password"
      />
      <Button variant="primary" type="submit">
        Submit
      </Button>
      <p className="text-danger bg-dark">{error}</p>
    </Form>
    </div>
  );
};

export default SignUp;
