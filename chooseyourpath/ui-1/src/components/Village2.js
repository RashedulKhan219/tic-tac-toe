import React from 'react'
import { useHistory } from 'react-router-dom'
import {Button, Container} from 'react-bootstrap'


const Village2 = () => {
    const history =useHistory()
    const quitgame = () => {
        history.push("/quit")
    }
    const towergate = () => {
        history.push("/towergate2")
    }
    const wilderness = () => {
        history.push("/wilderness")
    }
    return (
        <div  className= "App-village text-warning">
            <Container>
            <h1>Welcome to the village</h1>
            <h1>Thank you for saving our women and childrens and slaying the goblin.</h1>
            <h1>Adventurer has obtained 1000 coins from the village elder</h1>

            <Button onClick={wilderness} variant="primary" type="submit">Wilderness</Button>
            <Button onClick={towergate} variant="primary" type="submit">Tower Gate</Button>
            <Button onClick={quitgame} variant="primary" type="submit">Quit Game</Button>
            </Container>  
        </div>
    )
}

export default Village2