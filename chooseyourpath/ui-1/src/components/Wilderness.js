import React from "react";
import { useHistory, useParams } from "react-router-dom";
import { Button, Container, Alert, Card } from "react-bootstrap";
import { useState } from "react";
import GameApi from "./api/GameApi";

const Wilderness = () => {
  const [attcked, setAttacked] = useState("");
  const history = useHistory();
  const { gameId } = useParams();
  const [quest, setQuest] = useState();
  const [game, setGame] = useState();
  const [play, setPlay] = useState();
  const [playerHp, setPlayerHp] = useState();
  const [playerDmg, setPlayerDmg] = useState();
  const [goblinHp, setGoblinHp] = useState();
  const [goblinDmg, setGoblinDmg] = useState();
  const towergate2 = () => {
    history.push("/towergate2");
  };
  const attack = () => {
    GameApi.playGame(gameId).then((play) => {
      setPlay(play);
      console.log(play);
      setPlayerHp(play.playerHp);
      setPlayerDmg(play.playerDmg);
      setGoblinHp(play.goblinHp);
      setGoblinDmg(play.goblinDmg);
    });
  };

  const quit = () => {
    history.push("/quit");
  };
  const village2 = () => {
    history.push("/village/" + gameId);
  };
  const displayQuest = () => {
    GameApi.getGame(gameId).then((game) => {
      setGame(game);
      setQuest(
        <div>
          Quest : {game.villageQuest} <br />
          STATE: {game.state}
        </div>
      );
    });
  };

  return (
    <div className="App-Wilderness text-danger">
      <Alert>
        {goblinHp === 0 && (
          <h1>Goblin was killed. Goblin head was obtained.</h1>
        )}
      </Alert>
      <h1> Player HP/DMG: {playerHp + " " + playerDmg} </h1>
      <h1> Goblin HP/DMG : {goblinHp + " " + goblinDmg}</h1>
      {/* <Container className=""> */}
      <Card
        className="container bg-info text-white "
        style={{ width: "24rem", fontSize: "1.5rem" }}
      >
        <p>
          username has entered the wilderness After searching a few hideouts
          username has found where they kept the prisoners while freeing the
          prisoners, a goblin has appeared
        </p>
        <h1>{attcked}</h1>
      </Card>

      <br />
      <br />
      <Button onClick={attack} variant="primary" type="submit">
        Attack
      </Button>
      <Button onClick={quit} variant="primary" type="submit">
        Quit
      </Button>
      <Button onClick={village2} variant="primary" type="submit">
        Village
      </Button>
      <Button
        className="text-right"
        onClick={towergate2}
        variant="primary"
        type="submit"
      >
        Tower Gate
      </Button>
      <Card className="left-container" style={{ width: "24rem" }}>
        <Card.Header className=" text-light text-left bg-danger">
          Quests
        </Card.Header>
        <Card.Body>
          <Card.Title>Village Quest</Card.Title>
          <Card.Text>{quest}</Card.Text>
          <Card.Title>Tower Quest</Card.Title>
          <Card.Text>
            {game && (
              <p>
                {game.towerQuest} <br />
                STATE: {game.state}
              </p>
            )}
          </Card.Text>
          <Button
            className="text-right"
            onClick={displayQuest}
            variant="primary"
            type="submit"
          >
            Quest
          </Button>
        </Card.Body>
      </Card>
      {/* </Container> */}
    </div>
  );
};

export default Wilderness;
