import React, {useEffect,useState} from "react";
import { Navbar, Nav } from "react-bootstrap";
import { Link, useHistory } from "react-router-dom";
import CONSTANTS from "./utils/CONSTANTS";


const Header = () => {
  const[user,setUser] = useState({})
  const history = useHistory()

  useEffect(() => {
   const user = localStorage.getItem(CONSTANTS.LOCAL_STORAGE.USER)
  if (user) {
    setUser(JSON.parse(user))
  }
  }, [])
  const handleLogOut= () =>{
    localStorage.setItem(CONSTANTS.LOCAL_STORAGE.USER,JSON.stringify(null)) 
    
  }
  return (
    <div>
    <Navbar variant="dark" bg="dark" expand="lg">
      <Link className="navbar-brand" to="/">
        Choose Your Legacy
        
      </Link>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="ml-auto">
        {!user && <Link className="nav-link text-primary" to="/signup">
            Signup
          </Link>}
          

         {!user && <Link className="nav-link text-info" to="/login">
            login
          </Link>}
          {user && <Link className="nav-link text-info">
            {user.userName}
          </Link>}
         {user && <Link onClick={handleLogOut} className="nav-link text-info" to="/">
            Logout
          </Link>}
   
   
        </Nav>
      </Navbar.Collapse>
    </Navbar>

    
    </div>
    
  );
};

export default Header;
