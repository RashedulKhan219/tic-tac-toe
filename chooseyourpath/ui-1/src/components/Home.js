import React from 'react'
import Header from './Header'
import {Nav,Navbar,NavDropdown} from 'react-bootstrap'
import {Link} from 'react-router-dom'
function Home(){
    //here you can write code

    return(//here you write html
        //{} is used to display code within html
        // row-creates a row which puts it to the left side,
        //row has 12 grid
        // container shrinks the size
        // col gives you columns which you can control
        // monospace-control text
        // font-weight-bolder controls the boldness of text
        <div>
          <Header/>
          <br/>
          
          <div className='container row bg-warning mb-5'>
          <Navbar  className='ml-auto mr-auto'>
  
  <Navbar.Collapse id="basic-navbar-nav"></Navbar.Collapse>
  <Nav>
         <Link className="nav-link text-primary" to="/signup">
            Home
          </Link>
          

        <Link className="nav-link text-info" to="/login">
            Story
          </Link>
           <Link className="nav-link text-info">
            About
          </Link>
         <Link  className="nav-link text-info" to="/">
            Forum
          </Link>
   
   
        </Nav>
 
</Navbar>
    </div> 
            
        </div>
    )
}
export default Home;