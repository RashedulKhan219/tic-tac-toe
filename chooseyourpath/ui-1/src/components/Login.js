import React, { useState } from "react";
import { Form, Button } from "react-bootstrap";
import UserApi from "./api/UserApi";
import { useHistory } from "react-router-dom";
import CONSTANTS from "./utils/CONSTANTS";
import Header from './Header';

const Login = () => {
  const history = useHistory();
  const [user, setUser] = useState([]);

  const [emailAddress, setEmail] = useState("");

  const [password, setPassword] = useState("");

  const [error, setError] = useState("");

  const handleUser = (e) => {
    e.preventDefault();

    UserApi.userLogin(emailAddress, password).then((user) => {
      setUser(localStorage.setItem(CONSTANTS.LOCAL_STORAGE.USER, JSON.stringify(user)));
      if (user) {
        history.push("/gamepage/" + user.userId);
      }

      setError("wrong email or password");
    });
  };

  return (
    <div className="App-login">
      <Header/>
      <Form className="container col-lg-4" onSubmit={handleUser}>
        <Form.Label>Email Address</Form.Label>
        <Form.Control
          onChange={(e) => setEmail(e.target.value)}
          type="email"
          placeholder="Enter email"
        />
        <Form.Label>Password</Form.Label>
        <Form.Control
          onChange={(e) => setPassword(e.target.value)}
          type="password"
          placeholder="Password"
        />
        <Button variant="primary" type="submit">
          Login
        </Button>
        <p className="text-danger bg-dark">{error}</p>
      </Form>
    </div>
  );
};

export default Login;
