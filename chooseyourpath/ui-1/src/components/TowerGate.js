import React, { useState, useEffect } from "react";
import { useHistory, useParams } from "react-router-dom";
import { Button, Container, Card } from "react-bootstrap";
import GameApi from "./api/GameApi";
import Village from "./Village";

const TowerGate = () => {
  const { gameId } = useParams();
  const history = useHistory();
  const [game, setGame] = useState([]);
  const [quest, setQuest] = useState([]);

  useEffect(() => {
    GameApi.getGame(gameId).then((game) => setGame(game));
  }, []);

  const displayQuest = () => {
    GameApi.getGame(gameId).then((game) => {
      setGame(game);
      setQuest(
        <div>
          Quest : {game.towerQuest} <br />
          STATE: {game.state}
        </div>
      );
    });
  };

  const quitgame = () => {
    history.push("/quit");
  };
  const village = () => {
    history.push("/village/" + gameId);
  };
  const wilderness = () => {
    history.push("/wilderness/" + gameId);
  };

  return (
    <div className="App-towergate text-success ">
      {game.state === "INPROGRESS" && (
        <Container>
        <Card className="bg-warning "style={{width:"19rem"}}> 
        
          <p>TowerGuard: welcome to the Tower Gate adventurer.
          I can't allow you to enter as you did not finish the quest
          please obtain the head of a goblin to enter the city</p>
          
          </Card>
  
          <Button onClick={displayQuest} variant="primary">
            Quest
          </Button>
          <Button onClick={village} variant="primary" type="submit">
            Village
          </Button>
          <Button onClick={wilderness} variant="primary" type="submit">
            Wilderness
          </Button>
          <Button onClick={quitgame} variant="primary" type="submit">
            Quit
          </Button>
          <Card className="bg-warning" style={{width:"19rem"}}>
          <Card.Header className=" text-light text-left bg-danger">
          Quests
        </Card.Header>
        <Card.Body>
          <Card.Title>Tower Quest</Card.Title>
          <Card.Text>{quest}</Card.Text>
          </Card.Body>
          </Card>
          </Container>)}
          <br/>
          
          
        
      )
     
        {game.state === "COMPLETED" && (
          <Container className="col-lg-4">
            <div className="pt-5">
              <Card className="bg-dark">
                <p className="mt-5">
                  TowerGuard: welcome to the Tower Gate adventurer. is that a
                  goblin head TowerGuard shouts at the other guards to open the
                  gate to city. Welcome to Hidden leaf City
                </p>

                <h3>
                  You have reached the end of the game. Thank you for playing
                  Choose Your Path
                </h3>
              </Card>
            </div>
          </Container>
        )}
      </div>
    
  );
};

export default TowerGate;
