import React, { useState, useEffect } from "react";
import { useHistory, useParams } from "react-router-dom";
import { Button, Container, Card } from "react-bootstrap";
import GameApi from "./api/GameApi";
const Village = ({ user }) => {
  const history = useHistory();
  const [quest, setQuest] = useState([]);
  const { gameId } = useParams();
  const [game, setGame] = useState([]);

  useEffect(() => {
    GameApi.getGame(gameId).then((game) => setGame(game));
  }, []);

  const displayQuest = () => {
    GameApi.getGame(gameId).then((game) => {
      setGame(game);
      setQuest(
        <div>
          Quest : {game.villageQuest} <br />
          STATE: {game.state}
        </div>
      );
    });
  };

  const quitgame = () => {
    history.push("/quit");
  };
  const towergate = () => {
    history.push("/towergate/" + gameId);
  };
  const wilderness = () => {
    history.push("/wilderness/" + gameId);
  };
  console.log(game)
  return (
    <div className="App-village text-light">
      {game.state === "INPROGRESS"   && (
        <Container>
          
          <Card className="App-village-text bg-success" style={{ width: "23.5rem" }}>
          <p>
            Welcome to the village Adventurer we been terroized by goblins ,
            please help us kill them as they have been taking our women and
            childrens you can find the goblins staying in the wilderness
            Adventurer quest obtained
          </p>
          </Card>
          <Button onClick={displayQuest} variant="primary">
            Quest
          </Button>
          <Button onClick={wilderness} variant="primary" type="submit">
            Wilderness
          </Button>
          <Button onClick={towergate} variant="primary" type="submit">
            Tower Gate
          </Button>
          <Button onClick={quitgame} variant="primary" type="submit">
            Quit Game 
          </Button>
          <Card className="bg-warning" style={{width:"19rem"}}>
          <Card.Header className=" text-light text-left bg-danger">
          Quests
        </Card.Header>
        <Card.Body>
          <Card.Title>Village Quest</Card.Title>
          <Card.Text>{quest}</Card.Text>
          </Card.Body>
          </Card>
          </Container>)}
          <br/>
          
          
        
      )

      {game.state === "COMPLETED" && (
        <Container>
          <Card className ="App-village-text bg-success" style={{ width: "23.5rem" }}>
          <p>Welcome to the village
            Thank you for saving our women and childrens and slaying the goblin.
          Adventurer has obtained 1000 coins from the village elder</p>
          </Card>
          <Button onClick={towergate} variant="primary" type="submit">
            Tower Gate
          </Button>
          <Button onClick={quitgame} variant="primary" type="submit">
            Quit Game
          </Button>
        </Container>
      )}
    </div>
  );
};

export default Village;
