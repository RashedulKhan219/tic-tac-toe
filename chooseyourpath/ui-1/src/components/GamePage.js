import React from "react";
import { useHistory, useParams } from "react-router-dom";
import { Button, Container, Card } from "react-bootstrap";
import GameApi from "./api/GameApi";
import { useState } from "react";
import { useEffect } from "react";
import UserApi from "./api/UserApi";
const GamePage = () => {
  const [game, setGame] = useState(null);
  const [user, setUser] = useState([]);
  const [state, setState] = useState();
  const { userId } = useParams();

  const history = useHistory();
  useEffect(() => {
    UserApi.getUser(userId).then((user) => setUser(user));
  }, []);
  const village = () => {
    GameApi.getVillageQuest(game.gameId);
    history.push("/village/" + game.gameId);
  };
  const towergate = () => {
    GameApi.getVillageQuest(game.gameId);
    history.push("/towergate/" + game.gameId);
  };

  const handleCreateGame = () => {
    GameApi.createGame(userId).then((game) => {
      setGame(game);
      setState(state);
    });
  };
  console.log(game);
  return (
    <div className="App-gamepage">
      <Container>
        <h1>Choose Your Path {user.userName}!</h1>
        {game !== null && (
          <Button onClick={village} variant="primary" className="mr-2">
            Village
          </Button>
        )}
        {game !== null && (
          <Button onClick={towergate} variant="primary" className="mr-2">
            Tower Gate
          </Button>
        )}
        
        {game === null && (
          <Button onClick={handleCreateGame} variant="primary">
            Create Game
          </Button>
        )}
        {game && (
          <div>
            <h1>UserName: {game.userName}</h1>
            <h1>HP: {game.playerHp}</h1>
            <h1>DMG:{game.playerDmg}</h1>
          </div>
        )}
      </Container>
    </div>
  );
};

export default GamePage;
