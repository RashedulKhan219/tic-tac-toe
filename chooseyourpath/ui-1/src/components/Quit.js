import React from "react";

const Quit = () => {
  return (
    <div>
      <h1>Dont be afraid adventurer, you can finish the game!</h1>
    </div>
  );
};

export default Quit;
