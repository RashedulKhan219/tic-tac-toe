const BASE_URL = "http://localhost:8080/";

export default {
    URL: {
        BASE: BASE_URL,
        SIGN_UP: BASE_URL + "users",
        GAME: BASE_URL + "game"
    },
    LOCAL_STORAGE: {
        LOCAL_USER: "USER"
    }
}
