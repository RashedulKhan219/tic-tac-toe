import axios from "axios";
import CONSTANTS from "../utils/CONSTANTS";

class GameApi {
  static url = CONSTANTS.URL.GAME;

  static async playGame(gameId) {
    const response = await axios.post(this.url + "/play/" + gameId);
    return response.data;
  }

  static async createGame(userId) {
    const response = await axios.post(this.url + "/" + userId);
    return response.data;
  }

  static async getVillageQuest(gameId) {
    const response = await axios.put(this.url + "/" + gameId);
    return response.data;
  }

  static async getGame(gameId) {
    const response = await axios.get(this.url + "/" + gameId);
    return response.data;
  }
}

export default GameApi;
