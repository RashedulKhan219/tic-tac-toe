import axios from "axios";
import CONSTANTS from "../utils/CONSTANTS";

class UserApi {
  static url = CONSTANTS.URL.SIGN_UP;

  static async createUser(
    userName,
    firstName,
    lastName,
    emailAddress,
    dateOfBirth,
    password
  ) {
    const payload = {
      userName: userName,
      firstName: firstName,
      lastName: lastName,

      // "gender":'gender',
      emailAddress: emailAddress,
      dateOfBirth: dateOfBirth,
      password: password,
    };

    const response = await axios.post(this.url, payload);
    return response.data;
  }
  static async userLogin(emailAddress, password) {
    const response = await axios.post(
      this.url + "/" + emailAddress + "/" + password
    );
    return response.data;
  }

  static async getUser(userId) {
    const response = await axios.get(this.url + "/" + userId);
    return response.data;
  }
}

export default UserApi;
