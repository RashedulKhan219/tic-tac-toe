import React, { useState, useEffect } from 'react'
import CONSTANTS from './utils/CONSTANTS'
import { Navbar, Nav, Button } from 'react-bootstrap'
import { Link } from 'react-router-dom'

const Header = () => {
  const [user, setUser] = useState()

  useEffect(() => {
    setInterval(() => {
      getUser()
    }, 1000)
},[])

  const getUser = () => {
    const user = localStorage.getItem(CONSTANTS.LOCAL_STORAGE.USER)
    if(user) {
        setUser(JSON.parse(user))
    }
  }

    return (
    <Navbar collapseOnSelect expand="md" sticky="top" bg="dark" variant="dark">
      <Link className="navbar-brand" to="/">TLC CAR FOR RENT</Link>
    <Navbar.Toggle aria-controls="responsive-navbar-nav" />
    <Navbar.Collapse id="responsive-navbar-nav">
    <Nav className="ml-auto">
      <Link className="nav-link m-auto text-info" to="/uberlyft">Uber/Lyft</Link>
      <Link className="nav-link m-auto text-warning" to="/yellowcab">Yellow Cab</Link>
      <Link className="nav-link m-auto text-success" to="/greencab">Green Cab</Link>
    </Nav>
    <Nav className="ml-auto">
    <Link className="m-auto inline text-light nav-link" to="/postad">
      <Button variant="primary">Post Ad</Button>
    </Link>
      {!user && <Link className="m-auto text-light nav-link" to="/login">Login</Link>}
      {!user && <Link className="m-auto inline text-light nav-link" to="/signup">Sign up</Link>}
      {user && <Link className="m-auto inline text-light nav-link" to={"/account/" + user.userId}>{user.fullName}</Link>}
    </Nav> 
    </Navbar.Collapse>
  </Navbar>
    )
}

export default Header
