import React, { useState, useEffect } from 'react'
import Pagination from '../components/Pagination';
import { Container, Form, Button, Card, Spinner, Image, Col, Row, InputGroup, FormGroup, FormLabel} from 'react-bootstrap'
import { Link, useParams } from 'react-router-dom'
import YellowCabApi from './api/YellowCabApi'
import CONSTANTS from './utils/CONSTANTS';
import ReactTimeAgo from 'react-time-ago'
import { Helmet } from 'react-helmet'
import noImg from './images/no_image.jpg'

const YellowCab = () => {
    const[yellowCabAds, setYellowCabAds] = useState([])
    const [loading, setLoading] = useState(false)
    const { yellowCabId } = useParams();
    const [search, setSearch] = useState('');

    useEffect(() =>{
        setLoading(true)
        YellowCabApi.getYellowCabAds().then((response) => {
            setYellowCabAds(response)
            setLoading(false)
        })
    }, [yellowCabId])

    const showPerPage = 25
    const [pagination, setPagination] = useState({
    start: 0,
    end: showPerPage,
    });

    const onPaginationChange = (start, end) => {
        setPagination({ start: start, end: end });
      };

    if(loading) return <Spinner variant="warning" animation="border" size="lg"/>

    return (
        <Container className="mt-4">
            <Helmet><title>TLC Car for Rent | List of Yellow Cabs for rent</title></Helmet>
            <Link className="nav-link p-0" to="/yellowcabpostad">
                <Button className="btn-block" variant="warning">Post Ad for Yellow Cab</Button>
            </Link>
            <h4 className="text-center mt-3">{yellowCabAds.length} Yellow cabs available for rent!</h4>
            <FormGroup>
                <FormLabel>Search</FormLabel>
                <InputGroup>
                    <InputGroup.Prepend>
                        <InputGroup.Text className="bg-dark">🚖</InputGroup.Text>
                    </InputGroup.Prepend>
                    <Form.Control className="bg-light text-dark" type='search' placeholder='search - example "honda accord"' onChange={(e) => setSearch(e.target.value)} />
                </InputGroup>
            </FormGroup>
            <div>
                {
                    yellowCabAds.filter(ads => {
                    if (search === '') {
                        return ads;
                    } else if (ads.adTitle.toLowerCase().includes(search.toLowerCase())){
                            return ads;
                    }
                    }).slice(pagination.start, pagination.end).map((ad, adId) => (
                        <Card className="p-2 mt-3 bg-light shadow" key={adId}>
                            <Row>
                                <Col xs={4} md={3}>
                                {ad.image ? <Link to={'/yellowcab/individual/' + ad.yellowCabId}>
                                    <Image className="shadow rounded" src={CONSTANTS.URL.UPLOAD + "/" + ad.yellowCabId} fluid /></Link> :
                                    <Link to={'/yellowcab/individual/' + ad.yellowCabId}>
                                    <Image className="shadow rounded" src={noImg} fluid /></Link>}
                                </Col>
                                <Col>
                                    <Link className="nav-link p-0" to={'/yellowcab/individual/' + ad.yellowCabId}>
                                    <Card.Title className="text-dark">⭐{ad.adTitle} - <span className="text-success"> ${ad.adPrice}</span></Card.Title>
                                    </Link>
                                    <div className="text-danger">📌 {ad.adLocation}</div>
                                    <div className="text-primary">Posted by {ad.user.fullName} <ReactTimeAgo className="text-dark" date={ad.date} locale="en-US"/></div>
                                </Col>
                            </Row>
                        </Card>
                    )      
                    )
                }
            </div>
            {!loading && <Pagination
          showPerPage={showPerPage}
          onPaginationChange={onPaginationChange}
          total={yellowCabAds.length}
        />}
        </Container>
    )
}

export default YellowCab
