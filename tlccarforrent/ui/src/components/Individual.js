import React, { useState, useEffect } from 'react'
import UberLyftApi from './api/UberLyftApi'
import { Container, Button, Card, Spinner, Figure, Modal, FormGroup, InputGroup, Form, FormLabel, Alert } from 'react-bootstrap'
import { useParams } from 'react-router-dom'
import CONSTANTS from './utils/CONSTANTS';
import YellowCabApi from './api/YellowCabApi';
import GreenCabApi from './api/GreenCabApi';
import UploadImageApi from './api/UploadImageApi';
import { Helmet } from 'react-helmet'
import noImg from './images/no_image.jpg'
import { SRLWrapper } from 'simple-react-lightbox'

const Individual = () => {
  const [ad, setAd] = useState()
  const [loading, setLoading] = useState(false)
  const { uberLyftId, yellowCabId, greenCabId } = useParams();
  const [adTitle, setAdTitle] = useState("");
  const [adPrice, setAdPrice] = useState("");
  const [adPhone, setAdPhone] = useState("");
  const [adLocation, setAdLocation] = useState("");
  const [adDescription, setAdDescription] = useState("");
  const [show, setShow] = useState(false);
  const [updateAd, setUpdateAd] = useState();
  const [user, setUser] = useState()
  const [img, setImg] = useState()
  const [show1, setShow1] = useState(false);
  const [file, setFile] = useState();

  useEffect(() => {
    setLoading(true)
    const users = localStorage.getItem(CONSTANTS.LOCAL_STORAGE.USER)
    if (users) {
      setUser(JSON.parse(users))
    }
    if (uberLyftId) {
    UberLyftApi.getUberLyftAd(uberLyftId).then((response) => {
      setAd(response)
      setImg(CONSTANTS.URL.UPLOAD + "/" + response.uberLyftId)
      setLoading(false)
    })}
    else if (yellowCabId) {
    YellowCabApi.getYellowCabAd(yellowCabId).then((response) => {
      setAd(response)
      setImg(CONSTANTS.URL.UPLOAD + "/" + response.yellowCabId)
      setLoading(false)
    })}
    else if (greenCabId) {
    GreenCabApi.getGreenCabAd(greenCabId).then((response) => {
      setAd(response)
      setImg(CONSTANTS.URL.UPLOAD + "/" + response.greenCabId)
      setLoading(false)
    })}
  }, [uberLyftId, yellowCabId, greenCabId])

  const handleClose = () => {
    setShow(false)
    setShow1(false)
  };

  const handleShowDelete = () => {
    setShow1(true);
  }

  const handleShowUpdate = () => {
    setAdDescription(ad.adDescription);
    setAdTitle(ad.adTitle);
    setAdPrice(ad.adPrice);
    setAdPhone(ad.adPhone);
    setAdLocation(ad.adLocation);
  
  setShow(true)
}

  const handleUpdate = (event) => {
    event.preventDefault();
    setLoading(true)
    if (uberLyftId) {
      if (file) {
        if (ad.image) {
    const formData = new FormData();
    formData.append('file', file);
    UploadImageApi.updateAdImage(uberLyftId, formData).then(img => {
      if (img) {
      setFile(img)
    UberLyftApi.updateUberLyftAd(uberLyftId, adTitle, adPrice, adPhone, adLocation, adDescription, ad.image).then(ad => {
      setUpdateAd(ad)
      setAd(ad)
      setShow(false)
      setLoading(false)
    })}
    })
  }else {
    const formData = new FormData();
      formData.append('file', file);
    UploadImageApi.createImgFile(ad.uberLyftId, formData).then(img => {
      if (img) {
        UberLyftApi.getUberLyftAd(uberLyftId).then()
        UberLyftApi.updateUberLyftAd(uberLyftId, adTitle, adPrice, adPhone, adLocation, adDescription, true).then(ad => {
          setUpdateAd(ad)
          setAd(ad)
          setShow(false);
          setLoading(false)
          })
      }
    })}
  }else {
    UberLyftApi.updateUberLyftAd(uberLyftId, adTitle, adPrice, adPhone, adLocation, adDescription, ad.image).then(ad => {
      setUpdateAd(ad)
      setAd(ad)
      setShow(false)
      setLoading(false)
    })
    }
  }
    
    else if (greenCabId) {
      if(file) {
      if (ad.image) {
    GreenCabApi.updateGreenCabAd(greenCabId, adTitle, adPrice, adPhone, adLocation, adDescription, ad.image).then(ad => {
      const formData = new FormData();
      formData.append('file', file);
      UploadImageApi.updateAdImage(greenCabId, formData).then(img => {
      if (img) {
      setFile(img)
      setUpdateAd(ad)
      setAd(ad)
      setShow(false);
      setLoading(false)
    }})
  })
  }else {
    const formData = new FormData();
      formData.append('file', file);
    UploadImageApi.createImgFile(ad.greenCabId, formData).then(img => {
      if (img) {
        GreenCabApi.getGreenCabAd(greenCabId).then()
        GreenCabApi.updateGreenCabAd(greenCabId, adTitle, adPrice, adPhone, adLocation, adDescription, true).then(ad => {
          setUpdateAd(ad)
          setAd(ad)
          setShow(false);
          setLoading(false)
          })
      }
    })
  }
    }else {
      GreenCabApi.updateGreenCabAd(greenCabId, adTitle, adPrice, adPhone, adLocation, adDescription, ad.image).then(ad => {
        setUpdateAd(ad)
        setAd(ad)
        setShow(false);
        setLoading(false)
    })
  }
}

    else if (yellowCabId) {
      if (file) {
        if (ad.image) {
      const formData = new FormData();
      formData.append('file', file);
      UploadImageApi.updateAdImage(yellowCabId, formData).then(img => {
        if (img) {
        setFile(img)
      YellowCabApi.updateYellowCabAd(yellowCabId, adTitle, adPrice, adPhone, adLocation, adDescription, ad.image).then(ad => {
        setUpdateAd(ad)
        setAd(ad)
        setShow(false);
        setLoading(false)
      })}})
    }else {
      const formData = new FormData();
        formData.append('file', file);
      UploadImageApi.createImgFile(ad.yellowCabId, formData).then(img => {
        if (img) {
          YellowCabApi.getYellowCabAd(yellowCabId).then()
          YellowCabApi.updateYellowCabAd(yellowCabId, adTitle, adPrice, adPhone, adLocation, adDescription, true).then(ad => {
            setUpdateAd(ad)
            setAd(ad)
            setShow(false);
            setLoading(false)
            })
        }
      })}
    }else {
      YellowCabApi.updateYellowCabAd(yellowCabId, adTitle, adPrice, adPhone, adLocation, adDescription, ad.image).then(ad => {
        setUpdateAd(ad)
        setAd(ad)
        setShow(false);
        setLoading(false)
      })
    }
    }

  }

  const handleDelete = (event) => {
    event.preventDefault();
    setLoading(true)
    if (uberLyftId) {
    UberLyftApi.removeUberLyftAd(uberLyftId).then(() => {
      setAd(null)
      setShow1(false)
      setLoading(false)
    })}
    else if (yellowCabId) {
      YellowCabApi.removeYellowCabAd(yellowCabId).then(() => {
        setAd(null)
        setShow1(false)
        setLoading(false)
      })}
    else if (greenCabId) {
        GreenCabApi.removeGreenCabAd(greenCabId).then(() => {
          setAd(null)
          setShow1(false)
          setLoading(false)
      })}

  }

  const handlePhoneNumber =(event)=>{
    var x = event.target.value;
 var cleaned = ('' + x).replace(/\D/g, '');
 var match = cleaned.match(/^(1|)?(\d{3})(\d{3})(\d{4})$/);
 if (match) {
   var intlCode = (match[1] ? '+1 ' : '');
   return setAdPhone([intlCode, '(', match[2], ') ', match[3], '-', match[4]].join(''))
 }
 return null;
}

  if (loading) return <Spinner variant="warning" animation="border" size="lg" />

  return (
    <div>
      {ad ?
        <Container className="mt-4">
          <Helmet><title>TLC Car for Rent | {ad.adTitle}</title></Helmet>
          {!loading && <div>
            <div className="text-center">
            <SRLWrapper>
              <Figure>
              <a href={img} />
              {ad.image ? <Figure.Image
              className="shadow rounded"
                  width={500}
                  alt="tlc car"
                  style={{cursor: 'zoom-in'}}
                  src={img} /> :
                  <Figure.Image
                  width={500}
                  alt="tlc car"
                  src={noImg} />}
              </Figure>
              </SRLWrapper>
              <div>
                <h5>Contact ({ad.user.fullName})</h5>
                <h5>{!ad.adPhone ? <div>📧 E-mail now <span className="text-danger">{ad.user.email}</span></div> : <div>☎️ Call now <span className="text-danger">{ad.adPhone}</span></div>}</h5>
                {updateAd && <Alert dismissible onClose={() => setUpdateAd(null)} variant="success">
                  Successfully updated!
                 </Alert>}
              </div>
            </div>
            <Card className="p-4 mt-3 shadow">
              <Card.Title className="card-title text-primary">⭐ {ad.adTitle} - <span className="text-success"> ${ad.adPrice}</span></Card.Title>
              <div className="text-primary">📌 {ad.adLocation} {!ad.adPhone ? <span> 📧 {ad.user.email}</span> :  <span> ☎️ {ad.adPhone}</span>}</div>
              <hr className="bg-light"></hr>
              <Card.Text className="mt-2 text-dark">{ad.adDescription}</Card.Text>
              <hr className="bg-light"></hr>
              {user && ad.user.userId === user.userId &&
                <Button onClick={handleShowUpdate} variant='dark' className='mb-2'>Update</Button>}
              {user && ad.user.userId === user.userId &&
                <Button variant='danger' onClick={handleShowDelete} >Delete</Button>}
            </Card>

          </div>}
        </Container> : <Container className="mt-5 text-center"> <Alert variant="danger" >
          <h4>This post is no longer available</h4>
        </Alert></Container>}
      <Modal
        className="bg-dark shadow"
        show={show}
        onHide={handleClose}
        backdrop="static"
        keyboard={false}
      >
        <Modal.Header closeButton>
          <Modal.Title>Update your post</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <FormGroup>
            <FormLabel>Title*</FormLabel>
            <InputGroup>
              <InputGroup.Prepend>
                <InputGroup.Text className="bg-secondary">🚘</InputGroup.Text>
              </InputGroup.Prepend>
              <Form.Control className="bg-dark text-light" type='text' onChange={(e) => setAdTitle(e.target.value)} required defaultValue={adTitle} />
            </InputGroup>
          </FormGroup>
          <FormGroup>
            <FormLabel>Price*</FormLabel>
            <InputGroup>
              <InputGroup.Prepend>
                <InputGroup.Text className="px-3 bg-secondary text-light">💲</InputGroup.Text>
              </InputGroup.Prepend>
              <Form.Control className="bg-dark text-light" type='number' onChange={(e) => setAdPrice(e.target.value)} required defaultValue={adPrice} />
            </InputGroup>
          </FormGroup>
          <FormGroup>
            <FormLabel>Phone*</FormLabel>
            <InputGroup>
              <InputGroup.Prepend>
                <InputGroup.Text className="bg-secondary text-light">📞</InputGroup.Text>
              </InputGroup.Prepend>
              <Form.Control className="bg-dark text-light" type='tel' onChange={handlePhoneNumber} required defaultValue={adPhone} />
            </InputGroup>
          </FormGroup>
          <FormGroup>
            <FormLabel>Location*</FormLabel>
            <InputGroup>
              <InputGroup.Prepend>
                <InputGroup.Text className="bg-secondary">📌</InputGroup.Text>
              </InputGroup.Prepend>
              <Form.Control className="bg-dark text-light" type='text' onChange={(e) => setAdLocation(e.target.value)} required defaultValue={adLocation} />
            </InputGroup>
          </FormGroup>
          <FormGroup>
            <FormLabel>Description*</FormLabel>
            <Form.Control className="bg-dark text-light" as="textarea" rows="5" onChange={(e) => setAdDescription(e.target.value)} required defaultValue={adDescription} />
          </FormGroup>
          <input className="mb-3"
                        type="file"
                        // required
                        multiple={false}
                        id="upload-button"
                        onChange={(e) => setFile(e.target.files[0])}
                    />
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <Button variant="primary" onClick={handleUpdate}>Update</Button>
        </Modal.Footer>
      </Modal>

      <Modal
        className="bg-dark shadow"
        show={show1}
        onHide={handleClose}
        backdrop="static"
        keyboard={false}                  
      >
        <Modal.Header closeButton>
          <Modal.Title className="text-danger">Warning...!</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          Are you sure you want to remove this post?
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <Button variant="danger" onClick={handleDelete}>Delete</Button>
        </Modal.Footer>
      </Modal>
    </div>
  )
}

export default Individual                                             
