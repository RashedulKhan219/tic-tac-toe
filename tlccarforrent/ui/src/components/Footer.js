import React from 'react'
import { Container, Row, Col } from 'react-bootstrap'

const Footer = () => {

    return (
        <div className="py-5">
            <Container className="py-5">
            <hr className="bg-dark"></hr>
                <Row>
                    <Col>
                    <h5>Contact us</h5>
                    <p className="mb-4">contact@tlccarforrent.com</p>
            
                    <footer>United States</footer>
                    <footer>New York City</footer>
                    <footer>&copy; Copyright 2020-{new Date().getFullYear()} tlccarforrent.com</footer>
                    </Col>
                    <Col className="text-center">
                    <form action="https://www.paypal.com/donate" method="post" target="_top">
                    <input type="hidden" name="hosted_button_id" value="QZ2WGULQPWV8Y" />
                    <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif" border="0" name="submit" title="PayPal - The safer, easier way to pay online!" alt="Donate with PayPal button" />
                    <img alt="" border="0" src="https://www.paypal.com/en_US/i/scr/pixel.gif" width="1" height="1" />
                    </form>
                    <h6 className="text-success">Leave a tip for the developer.</h6>
                    </Col>
                </Row>
            </Container> 
        </div>
    )
}

export default Footer

