import React, { useState, useEffect } from 'react'
import UserApi from "./api/UserApi";
import{ useHistory } from 'react-router-dom'
import { Form, Button, Container, FormGroup, FormLabel, Spinner } from 'react-bootstrap'
import { Link } from "react-router-dom"
import CONSTANTS from './utils/CONSTANTS';
import { Helmet } from 'react-helmet'

const Login = () => {
    const [user, setUser] = useState()
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const history = useHistory()
    const [loading, setLoading] = useState(false)
    const [error, setError] = useState("");

    useEffect(() => {
        const users = localStorage.getItem(CONSTANTS.LOCAL_STORAGE.USER)
    if(users) {
    setUser(JSON.parse(users))
    }
    },[])

    const handleUserLogin = (event) => {
        event.preventDefault();
        setLoading(true)
        UserApi.userLogin(email, password).then(response => {
            if(response){
                localStorage.setItem(CONSTANTS.LOCAL_STORAGE.USER, JSON.stringify(response))
                localStorage.setItem(CONSTANTS.LOCAL_STORAGE.LOGIN, JSON.stringify(true));
                localStorage.setItem(CONSTANTS.LOCAL_STORAGE.LOGOUT, JSON.stringify(false));
                history.push('/account/' + response.userId)
            }
            setError("Wrong email or password")
            setLoading(false)
        })

    }

    if(user) history.push('/account/' + user.userId)

    return (
        <Container className="col-sm-5 col-md-4 col-lg-3 mt-4">
            <Helmet><title>TLC Car for Rent | Login to your account</title></Helmet>
            <Form className="bg-dark p-4" onSubmit={handleUserLogin}>
                <h5 className="text-center">Login to your account</h5>
                <hr />
                <FormGroup>
                <FormLabel>Email</FormLabel>
                <Form.Control className="bg-dark text-light" type='email' onChange={(e) => setEmail(e.target.value)} required placeholder="Email*" />   
                </FormGroup>
                <FormGroup>
                <FormLabel>Password</FormLabel>
                <Form.Control className="bg-dark text-light" type='password' onChange={(e) => setPassword(e.target.value)} required placeholder="Password*" />
                </FormGroup>
                <FormGroup>
                <Button className="btn-block" variant="secondary" type="submit">Log in {loading && <Spinner variant="dark" animation="border" size="sm"/>}</Button>
                <p className="text-danger text-center">{error}</p>
                <p className="text-center">Need an account? <Link to="/signup">Register</Link></p>
                </FormGroup>
            </Form>
        </Container>
    )
}

export default Login
