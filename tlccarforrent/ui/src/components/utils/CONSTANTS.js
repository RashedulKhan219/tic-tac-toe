const BASE_URL = '/api/'
// const BASE_URL = 'http://localhost:8080/'

export default {
    URL: {
        BASE: BASE_URL,
        USER: BASE_URL + 'user',
        UBERLYFT: BASE_URL + 'uberlyft',
        YELLOWCAB: BASE_URL + 'yellowcab',
        GREENCAB: BASE_URL + 'greencab',
        UPLOAD: BASE_URL + 'uploadfile',
        TLCDRUGTEST: "https://data.cityofnewyork.us/resource/pe54-wf39.json?$$app_token=5mFu1QdweW5WxJxdwoUiYomyW"

    },
    LOCAL_STORAGE: {
        LOCAL_USER: "USER",
        "LOGIN":"LOGIN",
        "LOGOUT":"LOGOUT"
    }
}