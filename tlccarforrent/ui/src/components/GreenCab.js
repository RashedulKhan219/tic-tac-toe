import React, { useState, useEffect } from 'react'
import Pagination from '../components/Pagination';
import { Container, Form, Button, Card, Spinner, Image, Col, Row, InputGroup, FormGroup, FormLabel } from 'react-bootstrap'
import { Link, useParams } from 'react-router-dom'
import GreenCabApi from './api/GreenCabApi'
import CONSTANTS from './utils/CONSTANTS';
import ReactTimeAgo from 'react-time-ago'
import { Helmet } from 'react-helmet'
import noImg from './images/no_image.jpg'


const GreenCab = () => {
    const [greenCabAds, setGreenCabAds] = useState([])
    const [loading, setLoading] = useState(false)
    const { greenCabId } = useParams();
    const [search, setSearch] = useState('');

    useEffect(() => {
        setLoading(true)
        GreenCabApi.getGreenCabAds().then((response) => {
            setGreenCabAds(response)
            setLoading(false)
        })
    }, [greenCabId])

    const showPerPage = 25
    const [pagination, setPagination] = useState({
        start: 0,
        end: showPerPage,
    });

    const onPaginationChange = (start, end) => {
        setPagination({ start: start, end: end });
    };

    if (loading) return <Spinner variant="warning" animation="border" size="lg" />

    return (
        <Container className="mt-4">
            <Helmet><title>TLC Car for Rent | List of Green Cabs for rent</title></Helmet>
            <Link className="nav-link p-0" to="/greencabpostad">
                <Button className="btn-block" variant="success">Post Ad for Green Cab</Button>
            </Link>
            <h4 className="text-center mt-3">{greenCabAds.length} Green cabs available for rent!</h4>
            <FormGroup>
                <FormLabel>Search</FormLabel>
                <InputGroup>
                    <InputGroup.Prepend>
                        <InputGroup.Text className="bg-dark">🚙</InputGroup.Text>
                    </InputGroup.Prepend>
                    <Form.Control className="bg-light text-dark" type='search' placeholder='search - example "honda accord"' onChange={(e) => setSearch(e.target.value)} />
                </InputGroup>
            </FormGroup>
            <div>
                {
                    greenCabAds.filter(ads => {
                    if (search === '') {
                        return ads;
                    } else if (ads.adTitle.toLowerCase().includes(search.toLowerCase())){
                        return ads;
                    }
                    }).slice(pagination.start, pagination.end).map((ad, adId) => (
                        <Card className="p-2 mt-3 bg-light shadow" key={adId}>
                            <Row>
                                <Col xs={4} md={3}>
                                {ad.image ? <Link to={'/greencab/individual/' + ad.greenCabId}>
                                    <Image className="shadow rounded" src={CONSTANTS.URL.UPLOAD + "/" + ad.greenCabId} fluid /></Link> :
                                    <Link to={'/greencab/individual/' + ad.greenCabId}>
                                    <Image className="shadow rounded" src={noImg} fluid /></Link>}
                                </Col>
                                <Col>
                                    <Link className="nav-link p-0" to={'/greencab/individual/' + ad.greenCabId}>
                                    <Card.Title className="text-success">⭐{ad.adTitle} - <span className="text-success"> ${ad.adPrice}</span></Card.Title>
                                    </Link>
                                    <div className="text-danger">📌 {ad.adLocation}</div>
                                    <div className="text-primary">Posted by {ad.user.fullName} <ReactTimeAgo className="text-dark" date={ad.date} locale="en-US"/></div>
                                </Col>
                            </Row>
                        </Card>
                    )
                    )
                }
            </div>
            {!loading && <Pagination
                showPerPage={showPerPage}
                onPaginationChange={onPaginationChange}
                total={greenCabAds.length}
            />}
        </Container>
    )
}

export default GreenCab
