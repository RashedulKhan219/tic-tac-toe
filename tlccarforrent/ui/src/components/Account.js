import React, { useEffect, useState } from 'react'
import { Row, Image, Form, Button, Container, FormGroup, FormLabel, Alert, InputGroup, Card, Col, Modal, Spinner } from 'react-bootstrap'
import { useParams } from 'react-router'
import { Link } from 'react-router-dom'
import GreenCabApi from './api/GreenCabApi'
import UberLyftApi from './api/UberLyftApi'
import UploadImageApi from './api/UploadImageApi'
import YellowCabApi from './api/YellowCabApi'
import CONSTANTS from './utils/CONSTANTS'
import ReactTimeAgo from 'react-time-ago'
import UserApi from './api/UserApi'
import { Helmet } from 'react-helmet'
import noImg from './images/no_image.jpg'

const Account = ({ history }) => {
  const [uberLyftAds, setUberLyftAds] = useState([]);
  const [yellowCabAds, setYellowCabAds] = useState([]);
  const [greenCabAds, setGreenCabAds] = useState([]);
  const [user, setUser] = useState({})
  const [adTitle, setAdTitle] = useState("");
  const [adPrice, setAdPrice] = useState("");
  const [adPhone, setAdPhone] = useState("");
  const [adLocation, setAdLocation] = useState("");
  const [adDescription, setAdDescription] = useState("");
  const [remove, setRemove] = useState()
  const [file, setFile] = useState();
  const [loading, setLoading] = useState(false);
  const { userId } = useParams()
  const [updateAd, setUpdateAd] = useState();
  const [uberLyftId, setUberLyftId] = useState();
  const [yellowCabId, setYellowCabId] = useState();
  const [greenCabId, setGreenCabId] = useState();
  const [show, setShow] = useState(false);
  const [show1, setShow1] = useState(false);
  const [logout, setLogout] = useState(false);

  useEffect(() => {
    const logout = localStorage.getItem(CONSTANTS.LOCAL_STORAGE.LOGOUT);
    if (logout) {
      setLogout(JSON.parse(logout))
    }
    UserApi.findUser(userId).then(user => setUser(user))
    UberLyftApi.getAllUserAds(userId).then(ads => setUberLyftAds(ads))
    YellowCabApi.getAllUserAds(userId).then(ads => setYellowCabAds(ads))
    GreenCabApi.getAllUserAds(userId).then(ads => setGreenCabAds(ads))
  }, [])


  const handleClose = () => {
    setShow(false)
    setShow1(false)
  };

  const handleShowUberLyft = (event) => {
    event.preventDefault()
    const id = document.getElementById(event.target.id)
    if (id.id) {
    UberLyftApi.getUberLyftAd(id.id).then(ad => {
      setUberLyftId(ad.uberLyftId)
      setAdDescription(ad.adDescription);
      setAdTitle(ad.adTitle);
      setAdPrice(ad.adPrice);
      setAdPhone(ad.adPhone);
      setAdLocation(ad.adLocation)
      setShow(true);
    })}
  }
  
    const handleShowYellowCab = (event) => {
      event.preventDefault()
      const id = document.getElementById(event.target.id)
      console.log(id)
      if (id.id) {
    YellowCabApi.getYellowCabAd(id.id).then(ad => {
      setYellowCabId(ad.yellowCabId)
      setAdDescription(ad.adDescription);
      setAdTitle(ad.adTitle);
      setAdPrice(ad.adPrice);
      setAdPhone(ad.adPhone);
      setAdLocation(ad.adLocation);
      setShow(true);
    })}
    }
  
    const handleShowGreenCab = (event) => {
      event.preventDefault()
      const id = document.getElementById(event.target.id)
      if (id.id) {
      GreenCabApi.getGreenCabAd(id.id).then(ad => {
      setGreenCabId(ad.greenCabId)
      setAdDescription(ad.adDescription);
      setAdTitle(ad.adTitle);
      setAdPrice(ad.adPrice);
      setAdPhone(ad.adPhone);
      setAdLocation(ad.adLocation)
      setShow(true);
    })}
  }

  const handleLogout = () => {
    localStorage.setItem(CONSTANTS.LOCAL_STORAGE.USER, JSON.stringify(null))
    localStorage.setItem(CONSTANTS.LOCAL_STORAGE.LOGOUT, JSON.stringify(true));
    localStorage.setItem(CONSTANTS.LOCAL_STORAGE.LOGIN, JSON.stringify(false));
    history.push("/")
  }
    
  const handleUpdate = (event) => {
    event.preventDefault()
    setLoading(true)
    if (uberLyftId) {
      if (file) {
        UberLyftApi.getUberLyftAd(uberLyftId).then(response => {
          if (response.image) {
            const formData = new FormData();
            formData.append('file', file);
            UploadImageApi.updateAdImage(uberLyftId, formData).then(img => {
            setFile(img)})
            UberLyftApi.updateUberLyftAd(uberLyftId, adTitle, adPrice, adPhone, adLocation, adDescription, response.image).then(response => {
            UberLyftApi.getAllUserAds(userId).then(ads => {
            setUberLyftAds(ads)
            setUpdateAd(response)
            setUberLyftId(null)
            setShow(false)
            setLoading(false)})})
          }else {
            const formData = new FormData();
            formData.append('file', file);
            UploadImageApi.createImgFile(uberLyftId, formData).then(img => {
              if (img) {
                UberLyftApi.getUberLyftAd(uberLyftId).then()
                UberLyftApi.updateUberLyftAd(uberLyftId, adTitle, adPrice, adPhone, adLocation, adDescription, true).then(ad => {
                  UberLyftApi.getAllUserAds(userId).then(ads => {
                  setUberLyftAds(ads)
                  setUpdateAd(ad)
                  setUberLyftId(null)
                  setShow(false);
                  setLoading(false)})})}})}})
        }else {
          UberLyftApi.getUberLyftAd(uberLyftId).then(response => {
          UberLyftApi.updateUberLyftAd(uberLyftId, adTitle, adPrice, adPhone, adLocation, adDescription, response.image).then(ad => {
            UberLyftApi.getAllUserAds(userId).then(ads => {
            setUberLyftAds(ads)
            setUpdateAd(ad)
            setUberLyftId(null)
            setShow(false)
            setLoading(false)})})})}
      }
      else if (yellowCabId) {
        if (file) {
          UberLyftApi.getUberLyftAd(yellowCabId).then(response => {
            if (response.image) {
              const formData = new FormData();
              formData.append('file', file);
              UploadImageApi.updateAdImage(yellowCabId, formData).then(img => {
              setFile(img)})
              YellowCabApi.updateYellowCabAd(yellowCabId, adTitle, adPrice, adPhone, adLocation, adDescription, response.image).then(response => {
              YellowCabApi.getAllUserAds(userId).then(ads => {
              setYellowCabAds(ads)
              setUpdateAd(response)
              setYellowCabId(null)
              setShow(false)
              setLoading(false)})})
            }else {
              const formData = new FormData();
              formData.append('file', file);
              UploadImageApi.createImgFile(yellowCabId, formData).then(img => {
                if (img) {
                  YellowCabApi.getYellowCabAd(yellowCabId).then()
                  YellowCabApi.updateYellowCabAd(yellowCabId, adTitle, adPrice, adPhone, adLocation, adDescription, true).then(ad => {
                    YellowCabApi.getAllUserAds(userId).then(ads => {
                    setYellowCabAds(ads)
                    setUpdateAd(ad)
                    setYellowCabId(null)
                    setShow(false);
                    setLoading(false)})})}})}})
          }else {
            YellowCabApi.getYellowCabAd(yellowCabId).then(response => {
            YellowCabApi.updateYellowCabAd(yellowCabId, adTitle, adPrice, adPhone, adLocation, adDescription, response.image).then(ad => {
              YellowCabApi.getAllUserAds(userId).then(ads => {
              setYellowCabAds(ads)
              setUpdateAd(ad)
              setYellowCabId(null)
              setShow(false)
              setLoading(false)})})})}
        }
        else if (greenCabId) {
          if (file) {
            GreenCabApi.getGreenCabAd(greenCabId).then(response => {
              if (response.image) {
                const formData = new FormData();
                formData.append('file', file);
                UploadImageApi.updateAdImage(greenCabId, formData).then(img => {
                setFile(img)})
                GreenCabApi.updateGreenCabAd(greenCabId, adTitle, adPrice, adPhone, adLocation, adDescription, response.image).then(response => {
                GreenCabApi.getAllUserAds(userId).then(ads => {
                setGreenCabAds(ads)
                setUpdateAd(response)
                setGreenCabId(null)
                setShow(false)
                setLoading(false)})})
              }else {
                const formData = new FormData();
                formData.append('file', file);
                UploadImageApi.createImgFile(greenCabId, formData).then(img => {
                  if (img) {
                    GreenCabApi.getGreenCabAd(greenCabId).then()
                    GreenCabApi.updateGreenCabAd(greenCabId, adTitle, adPrice, adPhone, adLocation, adDescription, true).then(ad => {
                      GreenCabApi.getAllUserAds(userId).then(ads => {
                      setGreenCabAds(ads)
                      setUpdateAd(ad)
                      setGreenCabId(null)
                      setShow(false);
                      setLoading(false)})})}})}})
            }else {
              GreenCabApi.getGreenCabAd(greenCabId).then(response => {
              GreenCabApi.updateGreenCabAd(greenCabId, adTitle, adPrice, adPhone, adLocation, adDescription, response.image).then(ad => {
                GreenCabApi.getAllUserAds(userId).then(ads => {
                setGreenCabAds(ads)
                setUpdateAd(ad)
                setGreenCabId(null)
                setShow(false)
                setLoading(false)})})})}
          }
    }

  const handleShowDeleteUberLyft = (event) => {
    const x = document.getElementById(event.target.id);
    setUberLyftId(x.id);
    setShow1(true);
  }
  const handleShowDeleteYellowCab = (event) => {
    const x = document.getElementById(event.target.id);
    setYellowCabId(x.id);
    setShow1(true);
  }
  const handleShowDeleteGreenCab = (event) => {
    const x = document.getElementById(event.target.id);
    setGreenCabId(x.id);
    setShow1(true);
  }

  const handleDelete = (event) => {
    event.preventDefault();
    if (uberLyftId) {
    UberLyftApi.removeUberLyftAd(uberLyftId).then(remove => {
      if (remove) {
      UberLyftApi.getAllUserAds(userId).then(ads => {
        setUberLyftAds(ads)
        setRemove(remove)
        setUberLyftId(null)
        setShow1(false)})
    }})}
    else if (yellowCabId) {
      YellowCabApi.removeYellowCabAd(yellowCabId).then(remove => {
        if (remove) {
        YellowCabApi.getAllUserAds(userId).then(ads => {
          setYellowCabAds(ads)
          setRemove(remove)
          setYellowCabId(null)
          setShow1(false)})
      }})}
      else if (greenCabId) {
        GreenCabApi.removeGreenCabAd(greenCabId).then(remove => {
          if (remove) {
          GreenCabApi.getAllUserAds(userId).then(ads => {
            setGreenCabAds(ads)
            setRemove(remove)
            setGreenCabId(null)
            setShow1(false)})
        }})}
  }

  if (logout) history.push("/login")

  return (
    
  <div>
    <Container className="mt-4">
    <Helmet><title>{`TLC Car for Rent | ${user.fullName}`}</title></Helmet>
      <Row>
      <Col md={4}>
      <Card className="" bg="dark">
          <Card.Header as="h5">Account imformation!</Card.Header>
          <Card.Body className="pt-2">
            Name: {user.fullName}
            <p>Email: {user.email}</p>
            <Button className="btn-block" variant="danger" onClick={handleLogout}>Logout</Button>
          </Card.Body>
        </Card>
      </Col>

    <Col>
      <Card className=""  bg="dark">
        <Card.Header className="rounded" as="h5">You currently have ({uberLyftAds.length + yellowCabAds.length + greenCabAds.length}) posts</Card.Header>
        {remove && <Alert variant="danger" dismissible onClose={() => setRemove(null)}>
          {remove}
        </Alert>}
          {uberLyftAds.map((ad, adId) => (
            <Card.Body className="px-4 py-2">
              <Row className="py-2 bg-light rounded" key={adId}>
              <Col xs={1} md={3}>
              {!loading && <Link to={'/uberlyft/individual/' + ad.uberLyftId}>
              {ad.image ? <Image src={CONSTANTS.URL.UPLOAD + "/" + ad.uberLyftId} fluid /> :
               <Image src={noImg} fluid />}
              </Link>}
              </Col>
              <Col>
              <Link className="nav-link p-0" to={'/uberlyft/individual/' + ad.uberLyftId}>
                <Card.Title className="text-info">⭐{ad.adTitle} - <span className="text-success"> ${ad.adPrice}</span></Card.Title>
              </Link>
              <div className="mb-1"><ReactTimeAgo className="text-primary" date={ad.date} locale="en-US"/></div>
              {updateAd && updateAd.uberLyftId === ad.uberLyftId && <Alert dismissible onClose={() => setUpdateAd(null)} variant="success">
                Successfully updated!
                 </Alert>}
              <Button className='mr-2' onClick={handleShowUberLyft} id={ad.uberLyftId} variant='dark'>Update</Button>
              <Button variant='danger' onClick={handleShowDeleteUberLyft} id={ad.uberLyftId}>Delete</Button>
              </Col>
              </Row>
              </Card.Body>
          ))}
          {yellowCabAds.map((ad, adId) => (
            <Card.Body className="py-2">
              <Row className="py-2 bg-light rounded" key={adId}>
              <Col xs={1} md={3}>
              {!loading && <Link to={'/yellowcab/individual/' + ad.yellowCabId}>
              {ad.image ? <Image src={CONSTANTS.URL.UPLOAD + "/" + ad.yellowCabId} fluid /> :
               <Image src={noImg} fluid />}
              </Link>}
              </Col>
              <Col>
              <Link className="nav-link p-0" to={'/yellowcab/individual/' + ad.yellowCabId}>
                <Card.Title className="text-dark">⭐{ad.adTitle} - <span className="text-success"> ${ad.adPrice}</span></Card.Title>
              </Link>
              <div className="mb-1"><ReactTimeAgo className="text-primary" date={ad.date} locale="en-US"/></div>
              {updateAd && updateAd.yellowCabId === ad.yellowCabId && <Alert dismissible onClose={() => setUpdateAd(null)} variant="success">
                Successfully updated!
                 </Alert>}
              <Button className='mr-2' onClick={handleShowYellowCab} id={ad.yellowCabId} variant='dark'>Update</Button>
              <Button variant='danger' onClick={handleShowDeleteYellowCab} id={ad.yellowCabId}>Delete</Button>
              </Col>
              </Row>
              </Card.Body>
          ))}
          {greenCabAds.map((ad, adId) => (
            <Card.Body className="py-2">
              <Row className="py-2 bg-light rounded" key={adId}>
              <Col xs={1} md={3}>
              {!loading && <Link to={'/greencab/individual/' + ad.greenCabId}>
              {ad.image ? <Image src={CONSTANTS.URL.UPLOAD + "/" + ad.greenCabId} fluid /> :
               <Image src={noImg} fluid />}
              </Link>}
              </Col>
              <Col>
              <Link className="nav-link p-0" to={'/greencab/individual/' + ad.greenCabId}>
                <Card.Title className="text-success">⭐{ad.adTitle} - <span className="text-success"> ${ad.adPrice}</span></Card.Title>
              </Link>
              <div className="mb-1"><ReactTimeAgo className="text-primary" date={ad.date} locale="en-US"/></div>
              {updateAd && updateAd.greenCabId === ad.greenCabId && <Alert dismissible onClose={() => setUpdateAd(null)} variant="success">
                Successfully updated!
                 </Alert>}
              <Button className='mr-2' onClick={handleShowGreenCab} id={ad.greenCabId} variant='dark'>Update</Button>
              <Button variant='danger' onClick={handleShowDeleteGreenCab} id={ad.greenCabId}>Delete</Button>
              </Col>
              </Row>
              </Card.Body>
          ))}
        </Card>

      
      <Modal
        className="bg-dark"
        show={show}
        onHide={handleClose}
        backdrop="static"
        keyboard={false}
      >
        <Modal.Header closeButton>
          <Modal.Title>Update your post</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <FormGroup>
            <FormLabel>Title*</FormLabel>
            <InputGroup>
              <InputGroup.Prepend>
                <InputGroup.Text className="bg-secondary">🚘</InputGroup.Text>
              </InputGroup.Prepend>
              <Form.Control className="bg-dark text-light" type='text' onChange={(e) => setAdTitle(e.target.value)} required defaultValue={adTitle} />
            </InputGroup>
          </FormGroup>
          <FormGroup>
            <FormLabel>Price*</FormLabel>
            <InputGroup>
              <InputGroup.Prepend>
                <InputGroup.Text className="px-3 bg-secondary text-light">💲</InputGroup.Text>
              </InputGroup.Prepend>
              <Form.Control className="bg-dark text-light" type='number' onChange={(e) => setAdPrice(e.target.value)} required defaultValue={adPrice} />
            </InputGroup>
          </FormGroup>
          <FormGroup>
            <FormLabel>Phone*</FormLabel>
            <InputGroup>
              <InputGroup.Prepend>
                <InputGroup.Text className="bg-secondary text-light">📞</InputGroup.Text>
              </InputGroup.Prepend>
              <Form.Control className="bg-dark text-light" type='phone' onChange={(e) => setAdPhone(e.target.value)} required defaultValue={adPhone} />
            </InputGroup>
          </FormGroup>
          <FormGroup>
            <FormLabel>Location*</FormLabel>
            <InputGroup>
              <InputGroup.Prepend>
                <InputGroup.Text className="bg-secondary">📌</InputGroup.Text>
              </InputGroup.Prepend>
              <Form.Control className="bg-dark text-light" type='text' onChange={(e) => setAdLocation(e.target.value)} required defaultValue={adLocation} />
            </InputGroup>
          </FormGroup>
          <FormGroup>
            <FormLabel>Description*</FormLabel>
            <Form.Control className="bg-dark text-light" as="textarea" rows="5" onChange={(e) => setAdDescription(e.target.value)} required defaultValue={adDescription} />
          </FormGroup>
          <input className="mb-3"
                        type="file"
                        // required
                        multiple={false}
                        id="upload-button"
                        onChange={(e) => setFile(e.target.files[0])}
                    />

        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <Button variant="primary" onClick={handleUpdate}>Update
          {loading && <Spinner animation="border" size="sm" variant="light" /> }
          </Button>
        </Modal.Footer>
      </Modal>


      <Modal
        className="bg-dark"
        show={show1}
        onHide={handleClose}
        backdrop="static"
        keyboard={false}
      >
        <Modal.Header closeButton>
          <Modal.Title className="text-danger">Warning...!</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          Are you sure you want to remove this post?
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <Button variant="danger" onClick={handleDelete}>Delete</Button>
        </Modal.Footer>
      </Modal>
      </Col>
      </Row>
      </Container>
    </div>
  )
}

export default Account
