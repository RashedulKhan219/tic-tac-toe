import React, { useState, useEffect } from 'react'
import UberLyftApi from './api/UberLyftApi'
import Pagination from '../components/Pagination';
import { Container, Form, Button, Card, Spinner, Image, Col, Row, InputGroup, FormGroup, FormLabel } from 'react-bootstrap'
import { Link, useParams } from 'react-router-dom'
import CONSTANTS from './utils/CONSTANTS';
import ReactTimeAgo from 'react-time-ago'
import { Helmet } from 'react-helmet'
import noImg from './images/no_image.jpg'

const UberLyft = () => {
    const [uberLyftAds, setUberLyftAds] = useState([])
    const [loading, setLoading] = useState(false)
    const { uberLyftId } = useParams();
    const [search, setSearch] = useState('');

    useEffect(() => {
        setLoading(true)
        UberLyftApi.getUberLyftAds().then((response) => {
            setUberLyftAds(response)
            setLoading(false)
        }) 
    }, [uberLyftId])

    const showPerPage = 25
    const [pagination, setPagination] = useState({
        start: 0,
        end: showPerPage,
    });

    const onPaginationChange = (start, end) => {
        setPagination({ start: start, end: end });
    };

    if (loading) return <Spinner variant="warning" animation="border" size="lg" />

    return (
        <Container className="mt-4">
            <Helmet><title>TLC Car for Rent | List of Uber and Lyft cars for rent</title></Helmet>
            <Link className="nav-link p-0" to="/uberlyftpostad">
                <Button className="btn-block" variant="info">Post Ad for Uber and Lyft</Button>
            </Link>
            <h4 className="text-center mt-3">{uberLyftAds.length} Uber and Lyft available for rent!</h4>
            <FormGroup>
                <FormLabel>Search</FormLabel>
                <InputGroup>
                    <InputGroup.Prepend>
                        <InputGroup.Text className="bg-dark">🚘</InputGroup.Text>
                    </InputGroup.Prepend>
                    <Form.Control className="bg-light text-dark" type='search' placeholder='search - example "honda accord"' onChange={(e) => setSearch(e.target.value)} />
                </InputGroup>
            </FormGroup>
            <div>
                {
                    uberLyftAds.filter(ads => {
                    if (search === '') {
                        return ads;
                    } else if (ads.adTitle.toLowerCase().includes(search.toLowerCase())){
                            return ads;
                    }
                    }).slice(pagination.start, pagination.end).map((ad, adId) => (
                        <Card className="p-2 mt-3 bg-light shadow" key={adId}>
                            <Row>
                                <Col xs={4} md={3}>
                        {ad.image ? <Link to={'/uberlyft/individual/' + ad.uberLyftId}>
                                    <Image className="shadow rounded" src={CONSTANTS.URL.UPLOAD + "/" + ad.uberLyftId} fluid /></Link> :
                                    <Link to={'/uberlyft/individual/' + ad.uberLyftId}>
                                    <Image className="shadow rounded" src={noImg} fluid /></Link>}
                                </Col>
                                <Col>
                                    <Link className="nav-link p-0" to={'/uberlyft/individual/' + ad.uberLyftId}>
                                    <Card.Title className="text-info">⭐{ad.adTitle} - <span className="text-success"> ${ad.adPrice}</span></Card.Title>
                                    </Link>
                                    <div className="text-danger">📌 {ad.adLocation}</div>
                                    <div className="text-primary">Posted by {ad.user.fullName} <ReactTimeAgo className="text-dark" date={ad.date} locale="en-US"/></div>
                                </Col>
                            </Row>
                        </Card>
                    )
                    )
                }
            </div>
            {!loading && <Pagination
                showPerPage={showPerPage}
                onPaginationChange={onPaginationChange}
                total={uberLyftAds.length}
            />}
        </Container>
    )
}

export default UberLyft
