import React from 'react'
import { Container, Form, Button } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import { Helmet } from 'react-helmet'

const PostAd = () => {
    return (
        <Container className="col-md-6 col-lg-5">
            <Helmet><title>TLC Car for Rent | Choose a Platform to Post Your Ad</title></Helmet>
            <Form className="bg-dark mt-4 p-4 text-center">
                <h5 className="text-center">Choose a Platform to Post Your Ad</h5>
                <hr />
                <Button className="btn-block" variant="info">
                <Link className="nav-link text-light" to="/uberlyftpostad">Post Ad for Uber or Lyft</Link>   
                </Button>
                <Button className="btn-block" variant="warning">
                <Link className="nav-link text-dark" to="/yellowcabpostad">Post Ad for Yellow Cab</Link>
                </Button>
                <Button className="btn-block" variant="success">
                <Link className="nav-link text-light" to="/greencabpostad">Post Ad for Green Cab</Link>
                </Button>
            </Form>
        </Container>
    )
}

export default PostAd