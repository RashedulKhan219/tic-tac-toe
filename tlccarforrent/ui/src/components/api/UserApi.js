import CONSTANTS from '../utils/CONSTANTS'
import axios from 'axios'

class UserApi {

    static url = CONSTANTS.URL.USER

    static async createUser(fullName, email, password) {
        const payload = {fullName: fullName,
            email: email, password: password}
        const response = await axios.post(this.url, payload)
        return response.data
    }

    static async userLogin(email, password) {
        const response = await axios.post(this.url + "/" + email + "/" + password)
        return response.data
    }

    static async findUser(userId) {
        const response = await axios.get(this.url + "/" + userId)
        return response.data
    }

}

export default UserApi