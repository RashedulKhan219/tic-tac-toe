import CONSTANTS from '../utils/CONSTANTS'
import axios from 'axios'

class TlcApi {

    static url = CONSTANTS.URL.TLCDRUGTEST

    static async getTlcDrugTestLocation() {
        const response = await axios.get(this.url)
        return response.data
    }

}

export default TlcApi