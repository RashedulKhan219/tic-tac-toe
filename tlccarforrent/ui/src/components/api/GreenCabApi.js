import CONSTANTS from '../utils/CONSTANTS'
import axios from 'axios'

class GreenCabApi {

    static url = CONSTANTS.URL.GREENCAB

    static async createGreenCabAd(adTitle, adPrice, adPhone, adLocation, adDescription, image, fullName, userId, email) {
        const payload = {adTitle: adTitle,
            adPrice: adPrice,
            adPhone: adPhone,
            adLocation: adLocation,
            adDescription: adDescription,
            image: image,
            user: {
                fullName: fullName,
                userId: userId,
                email: email
              }
        }
        const response = await axios.post(this.url, payload)
        return response.data
    }

    static async getGreenCabAds() {
        const response = await axios.get(this.url)
        return response.data
    }

    static async getGreenCabAd(greenCabId) {
        const response = await axios.get(this.url + "/" + greenCabId)
        return response.data
    }

    static async getAllUserAds(userId) {
        const response = await axios.get(this.url + "/user/" + userId)
        return response.data
    }

    static async updateGreenCabAd(greenCabId, adTitle, adPrice, adPhone, adLocation, adDescription, image) {
        const payload = {adTitle: adTitle,
            adPrice: adPrice,
            adPhone: adPhone,
            adLocation: adLocation,
            adDescription: adDescription,
            image: image
        }
        const response = await axios.put(this.url + "/" + greenCabId, payload)
        return response.data
    }

    static async removeGreenCabAd(greenCabId) {
        const response = await axios.delete(this.url + "/" + greenCabId)
        return response.data
    }

}

export default GreenCabApi