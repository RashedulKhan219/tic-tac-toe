import CONSTANTS from '../utils/CONSTANTS'
import axios from 'axios'

class YellowCabApi {

    static url = CONSTANTS.URL.YELLOWCAB

    static async createYellowCabAd(adTitle, adPrice, adPhone, adLocation, adDescription, image, fullName, userId, email) {
        const payload = {adTitle: adTitle,
            adPrice: adPrice,
            adPhone: adPhone,
            adLocation: adLocation,
            adDescription: adDescription,
            image: image,
            user: {
                fullName: fullName,
                userId: userId,
                email: email
              }
        }
        const response = await axios.post(this.url, payload)
        return response.data
    }

    static async getYellowCabAds() {
        const response = await axios.get(this.url)
        return response.data
    }

    static async getYellowCabAd(yellowCabId) {
        const response = await axios.get(this.url + "/" + yellowCabId)
        return response.data
    }

    static async getAllUserAds(userId) {
        const response = await axios.get(this.url + "/user/" + userId)
        return response.data
    }

    static async updateYellowCabAd(yellowCabId, adTitle, adPrice, adPhone, adLocation, adDescription, image) {
        const payload = {adTitle: adTitle,
            adPrice: adPrice,
            adPhone: adPhone,
            adLocation: adLocation,
            adDescription: adDescription,
            image: image
        }
        const response = await axios.put(this.url + "/" + yellowCabId, payload)
        return response.data
    }

    static async removeYellowCabAd(yellowCabId) {
        const response = await axios.delete(this.url + "/" + yellowCabId)
        return response.data
    }

}

export default YellowCabApi