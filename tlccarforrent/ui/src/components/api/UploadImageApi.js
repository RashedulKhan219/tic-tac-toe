import axios from 'axios'
import CONSTANTS from '../utils/CONSTANTS'

class UploadImageApi{


    static url = CONSTANTS.URL.UPLOAD;

    static async createImgFile(imgMatchingAdId, formData){
        const response = await axios.post(this.url + "/" + imgMatchingAdId, formData);
        return response.data;
    }
    static async getImageById(imgFileId){
        const response = await axios.get(this.url + "/" + imgFileId);
        return response.data;
    }

    static async updateAdImage(imgMatchingAdId, formData) {
        const response = await axios.put(this.url + "/" + imgMatchingAdId, formData);
        return response.data;
    }

}
export default UploadImageApi