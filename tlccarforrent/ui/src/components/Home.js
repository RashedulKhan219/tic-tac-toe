import React, { useState, useEffect } from 'react'
import UberLyftApi from './api/UberLyftApi'
import { Container, Card, Spinner, Button, Col, Row, Image, Modal, ListGroup } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import YellowCabApi from './api/YellowCabApi'
import GreenCabApi from './api/GreenCabApi'
import CONSTANTS from './utils/CONSTANTS'
import ReactTimeAgo from 'react-time-ago'
import TlcApi from './api/TlcApi'
import { Helmet } from 'react-helmet'
import noImg from './images/no_image.jpg'

const Home = () => {
    const[uberLyftAds, setUberLyftAds] = useState([])
    const[yellowCabAds, setYellowCabAds] = useState([])
    const[greenCabAds, setGreenCabAds] = useState([])
    const [loading, setLoading] = useState(false)
    const [show, setShow] = useState(false);
    const [tlcDrug, setTlcDrug] = useState([]);
    
    useEffect(() =>{
        setLoading(true)
        UberLyftApi.getUberLyftAds().then((response) => {
            setUberLyftAds(response)
            setLoading(false)
        })
        YellowCabApi.getYellowCabAds().then((response) => {
            setYellowCabAds(response)
            setLoading(false)
        })
        GreenCabApi.getGreenCabAds().then((response) => {
            setGreenCabAds(response)
            setLoading(false)
        })
    }, [])

    const handleDrugTest = () => {
      setShow(true)
      TlcApi.getTlcDrugTestLocation().then(response => setTlcDrug(response))

    }

  if(loading) return <Spinner variant="warning" animation="border" size="lg"/>

    const uberLyftRandomNumber = Math.floor (Math.random() * uberLyftAds.length) + 1
    const yellowCabRandomNumber = Math.floor (Math.random() * yellowCabAds.length) + 1
    const greenCabRandomNubmber = Math.floor (Math.random() * greenCabAds.length) + 1

    return (
        <div className="mt-3">
          <Helmet><title>TLC Car for Rent | Uber | Lyft | Yellow Cab | Green Cab</title></Helmet>
            <Container className="px-0">
            <Row>
            <Col className="pl-0">
          {uberLyftAds.slice(uberLyftRandomNumber -1, uberLyftRandomNumber).map((ad, adId) => (
            <Card className="p-0 pt-1 text-center" key={adId} bg="white">
                <Link className="nav-link p-0" to={'/uberlyft/individual/' + ad.uberLyftId}>
                <Card.Title className="text-info">⭐{ad.adTitle} - <span className="text-success"> ${ad.adPrice}</span></Card.Title>
                </Link>
                {ad.image ? <Link to={'/uberlyft/individual/' + ad.uberLyftId}>
                  <Image src={CONSTANTS.URL.UPLOAD + "/" + ad.uberLyftId} fluid /></Link> :
                  <Link to={'/uberlyft/individual/' + ad.uberLyftId}>
                  <Image src={noImg} fluid /></Link>}
            </Card>
          ))}
          </Col>
        
        <Col>
          {yellowCabAds.slice(yellowCabRandomNumber - 1, yellowCabRandomNumber).map((ad, adId) => (
            <Card className="p-0 pt-1 text-center" key={adId} bg="warning">
                <Link className="nav-link p-0" to={'/yellowcab/individual/' + ad.yellowCabId}>
                <Card.Title className="text-dark">⭐{ad.adTitle} - <span className="text-success"> ${ad.adPrice}</span></Card.Title>
                </Link>
                {ad.image ? <Link to={'/yellowcab/individual/' + ad.yellowCabId}>
                  <Image src={CONSTANTS.URL.UPLOAD + "/" + ad.yellowCabId} fluid /></Link> :
                  <Link to={'/yellowcab/individual/' + ad.yellowCabId}>
                  <Image src={noImg} fluid /></Link>}
            </Card>
          ))}
        </Col>

        <Col className="pr-0">
          {greenCabAds.slice(greenCabRandomNubmber - 1, greenCabRandomNubmber).map((ad, adId) => (
            <Card className="p-0 pt-1 text-center" key={adId} bg="success">
                <Link className="nav-link p-0" to={'/greencab/individual/' + ad.greenCabId}>
                <Card.Title className="text-light">⭐{ad.adTitle} - <span className="text-light"> ${ad.adPrice}</span></Card.Title>
                </Link>
                {ad.image ? <Link to={'/greencab/individual/' + ad.greenCabId}>
                  <Image src={CONSTANTS.URL.UPLOAD + "/" + ad.greenCabId} fluid /></Link> :
                  <Link to={'/greencab/individual/' + ad.greenCabId}>
                  <Image src={noImg} fluid /></Link>}
            </Card>
          ))}
        </Col>
        </Row>
        </Container>

        <Container className="mt-4 px-0">
        <Row>
          <Col className="px-0">
 
        <Card className="p-2 mb-4"  bg="dark">
        <Card.Header className="rounded bg-info" as="h5">Recent Posts in Uber and Lyft</Card.Header>
          {uberLyftAds.slice(0, 5).map((ad, adId) => (
            <Card.Body className="pb-0 shadow">
              <Row className="py-3 bg-light rounded" key={adId}>
                <Col xs={4} md={3}>
                {ad.image ? <Link to={'/uberlyft/individual/' + ad.uberLyftId}>
                  <Image className="shadow rounded" src={CONSTANTS.URL.UPLOAD + "/" + ad.uberLyftId} fluid /></Link> :
                  <Link to={'/uberlyft/individual/' + ad.uberLyftId}>
                  <Image className="shadow rounded" src={noImg} fluid /></Link>}
                </Col>
                <Col>
                <Link className="nav-link p-0" to={'/uberlyft/individual/' + ad.uberLyftId}>
                <Card.Title className="text-info">⭐{ad.adTitle} - <span className="text-success"> ${ad.adPrice}</span></Card.Title>
                </Link>
                <div className="text-danger">📌 {ad.adLocation}</div>
                <div className="text-dark">Posted by {ad.user.fullName} <ReactTimeAgo className="text-primary" date={ad.date} locale="en-US"/></div>
                </Col>
                </Row>
            </Card.Body> 
          ))}
          <Link className="nav-link p-0" to="/uberlyft">
                <Button className="btn-block mt-3" variant="secondary">View more uber and lyft posts</Button>
            </Link>
        </Card>

        <Card className="p-2 mb-4"  bg="dark">
        <Card.Header className="rounded bg-warning text-dark" as="h5">Recent Posts in Yellow Cab</Card.Header>
          {yellowCabAds.slice(0, 5).map((ad, adId) => (
            <Card.Body className="pb-0 shadow">
              <Row className="py-3 bg-light rounded" key={adId}>
                <Col xs={4} md={3}>
                {ad.image ? <Link to={'/yellowcab/individual/' + ad.yellowCabId}>
                  <Image className="shadow rounded" src={CONSTANTS.URL.UPLOAD + "/" + ad.yellowCabId} fluid /></Link> :
                  <Link to={'/yellowcab/individual/' + ad.yellowCabId}>
                  <Image className="shadow rounded" src={noImg} fluid /></Link>}
                </Col>
                <Col>
                <Link className="nav-link p-0" to={'/yellowcab/individual/' + ad.yellowCabId}>
                <Card.Title className="text-dark">⭐{ad.adTitle} - <span className="text-success"> ${ad.adPrice}</span></Card.Title>
                </Link>
                <div className="text-danger">📌 {ad.adLocation}</div>
                <div className="text-dark">Posted by {ad.user.fullName} <ReactTimeAgo className="text-primary" date={ad.date} locale="en-US"/></div>
                </Col>
                </Row>
            </Card.Body> 
          ))}
          <Link className="nav-link p-0" to="/yellowcab">
                <Button className="btn-block mt-3" variant="secondary">View more yellow cab posts</Button>
            </Link>
        </Card>

        <Card className="p-2"  bg="dark">
        <Card.Header className="rounded bg-success" as="h5">Recent Posts in Green Cab</Card.Header>
          {greenCabAds.slice(0, 5).map((ad, adId) => (
            <Card.Body className="pb-0 shadow">
              <Row className="py-3 bg-light rounded" key={adId}>
                <Col xs={4} md={3}>
                {ad.image ? <Link to={'/greencab/individual/' + ad.greenCabId}>
                  <Image className="shadow rounded" src={CONSTANTS.URL.UPLOAD + "/" + ad.greenCabId} fluid /></Link> :
                  <Link to={'/greencab/individual/' + ad.greenCabId}>
                  <Image className="shadow rounded" src={noImg} fluid /></Link>}
                </Col>
                <Col>
                <Link className="nav-link p-0" to={'/greencab/individual/' + ad.greenCabId}>
                <Card.Title className="text-success">⭐{ad.adTitle} - <span className="text-success"> ${ad.adPrice}</span></Card.Title>
                </Link>
                <div className="text-danger">📌 {ad.adLocation}</div>
                <div className="text-dark">Posted by {ad.user.fullName} <ReactTimeAgo className="text-primary" date={ad.date} locale="en-US"/></div>
                </Col>
                </Row>
            </Card.Body> 
          ))}
          <Link className="nav-link p-0" to="/greencab">
                <Button className="btn-block mt-3" variant="secondary">View more green cab posts</Button>
            </Link>
        </Card>
            
        
        </Col>
        <Col className="pr-0" xs={2}>
        <Card.Header className="rounded bg-secondary text-center" as="h6">Useful Links</Card.Header>
        <Link className="text-light"><h6 className="mt-2 mb-5" onClick={handleDrugTest}>TLC Approved LabCorp Patient Services Drug Test Locations</h6></Link>
        <form action="https://www.paypal.com/donate" method="post" target="_top">
            <input type="hidden" name="hosted_button_id" value="QZ2WGULQPWV8Y" />
            <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif" border="0" name="submit" title="PayPal - The safer, easier way to pay online!" alt="Donate with PayPal button" />
            <img alt="" border="0" src="https://www.paypal.com/en_US/i/scr/pixel.gif" width="1" height="1" />
        </form>

        </Col>
        </Row>
        </Container>
        <Modal
        size="xl"
        show={show}
        onHide={() => setShow(false)}
        dialogClassName="modal-90w"
        aria-labelledby="example-custom-modal-styling-title"
      >
        <Modal.Header closeButton>
          <Modal.Title id="example-custom-modal-styling-title">
          TLC Approved LabCorp Patient Services Drug Test Locations. You Must Call for an Appointment - <span className="text-primary">1(800) 923 2624</span>
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <ListGroup>
            {
              tlcDrug.map((data, id)=>(
                <ListGroup.Item key={id}>
                {data.address}, <span className="text-primary">{data.city},</span> {data.state} {data.postcode} - {data.landmark} - 1(800) 923 2624
                </ListGroup.Item>
            ))}
          </ListGroup>
        </Modal.Body>
      </Modal>
        </div>
    )
}

export default Home

