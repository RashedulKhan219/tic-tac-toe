import React, { useState, useEffect } from 'react'
import{ useHistory } from 'react-router-dom'
import YellowCabApi from './api/YellowCabApi'
import CONSTANTS from './utils/CONSTANTS'
import { Form, Button, Container, FormGroup, FormLabel, Spinner, InputGroup } from 'react-bootstrap'
import Login from './Login'
import UploadImageApi from './api/UploadImageApi'
import { Helmet } from 'react-helmet'


const YellowCabPostAd = () => {
    
    const [adTitle, setAdTitle] = useState("");
    const [adPrice, setAdPrice] = useState("");
    const [adPhone, setAdPhone] = useState("");
    const [adLocation, setAdLocation] = useState("");
    const [adDescription, setAdDescription] = useState("");
    const history = useHistory()
    const [loading, setLoading] = useState(false)
    const [user, setUser] = useState([])
    const [imgFile, setImgFile] = useState();

    useEffect(() => {
        const user = localStorage.getItem(CONSTANTS.LOCAL_STORAGE.USER)
        if(user) {
            setUser(JSON.parse(user))
        }
    },[])

    const handlePhoneNumber =(event)=>{
        var x = event.target.value;
     var cleaned = ('' + x).replace(/\D/g, '');
     var match = cleaned.match(/^(1|)?(\d{3})(\d{3})(\d{4})$/);
     if (match) {
       var intlCode = (match[1] ? '+1 ' : '');
       return setAdPhone([intlCode, '(', match[2], ') ', match[3], '-', match[4]].join(''))
     }
     return null;
    }

    const handleYellowCabPostAd = (event) => {
        event.preventDefault();
        setLoading(true)
        if (imgFile) {
        YellowCabApi.createYellowCabAd(adTitle, adPrice, adPhone, adLocation, adDescription, true, user.fullName, user.userId, user.email).then(ad => {
                    const formData = new FormData();
                    formData.append('file', imgFile);
                        UploadImageApi.createImgFile(ad.yellowCabId, formData).then(response => {
                            if(response){
                            history.push('/yellowcab/individual/' + ad.yellowCabId);
                            setLoading(false)}
                            })
                        })
                    }else {
                        YellowCabApi.createYellowCabAd(adTitle, adPrice, adPhone, adLocation, adDescription, false, user.fullName, user.userId, user.email).then(response => {
                        if(response){
                            history.push('/yellowcab/individual/' + response.yellowCabId);
                        setLoading(false)
                    }
                })}
        
    }

    return (
        <div>
            <Helmet><title>TLC Car for Rent | Create your Ad for Yellow Cab</title></Helmet>
            {!user && <Login />}
            {!user && <p className="mt-3 text-center text-warning">Please login to continue</p>}
        <Container className="col-md-6 col-lg-5">
        {user && <Form className="bg-warning mt-4 p-4" onSubmit={handleYellowCabPostAd}>
                <h5 className="text-center text-dark">Create your Ad for Yellow Cab</h5>
                <hr />
                <FormGroup>
                <FormLabel className="text-dark">Title*</FormLabel>
                <InputGroup>
                <InputGroup.Prepend>
                <InputGroup.Text className="bg-secondary">🚖</InputGroup.Text>
                </InputGroup.Prepend>
                <Form.Control className="bg-dark text-light" type='text' onChange={(e) => setAdTitle(e.target.value)} required placeholder="Enter your car details" />
                </InputGroup>
                </FormGroup>
                <FormGroup>
                <FormLabel className="text-dark">Price*</FormLabel>
                <InputGroup>
                <InputGroup.Prepend>
                <InputGroup.Text className="px-3 bg-secondary text-light">💲</InputGroup.Text>
                </InputGroup.Prepend>
                <Form.Control className="bg-dark text-light" type='number' onChange={(e) => setAdPrice(e.target.value)} required placeholder="Enter a price" />
                </InputGroup>
                </FormGroup>
                <FormGroup>
                <FormLabel className="text-dark">Phone*</FormLabel>
                <InputGroup>
                <InputGroup.Prepend>
                <InputGroup.Text className="bg-secondary text-light">📞</InputGroup.Text>
                </InputGroup.Prepend>
                <Form.Control className="bg-dark text-light" type='tel' onChange={handlePhoneNumber} minlength="10" required placeholder="Enter your phone number" />
                </InputGroup>
                </FormGroup>
                <FormGroup>
                <FormLabel className="text-dark">Location*</FormLabel>
                <InputGroup>
                <InputGroup.Prepend>
                <InputGroup.Text className="bg-secondary">📌</InputGroup.Text>
                </InputGroup.Prepend>
                <Form.Control className="bg-dark text-light" type='text' onChange={(e) => setAdLocation(e.target.value)} required placeholder="Enter your location" />
                </InputGroup>
                </FormGroup>
                <FormGroup>
                <FormLabel className="text-dark">Description*</FormLabel>
                <Form.Control className="bg-dark text-light" as="textarea" rows="5" onChange={(e) => setAdDescription(e.target.value)} required placeholder="Enter your description" />
                </FormGroup>
                <input className="mb-3" type="file" multiple={false}
                    id="upload-button"
                    onChange={(e) => setImgFile(e.target.files[0])}/>
                <FormGroup>
                <Button className="btn-block" variant="secondary" type="submit">Submit Ad {loading && <Spinner variant="warning" animation="border" size="sm"/>}</Button>
                </FormGroup>
            </Form>}
        </Container>
        </div>
    )
}

export default YellowCabPostAd