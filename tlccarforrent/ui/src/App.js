import React from 'react';
import './App.css';
import Home from './components/Home';
import { Switch, Route } from 'react-router-dom'
import Header from './components/Header';
import Signup from './components/Signup';
import Login from './components/Login';
import Account from './components/Account';
import UberLyft from './components/UberLyft';
import YellowCab from './components/YellowCab';
import GreenCab from './components/GreenCab';
import PostAd from './components/PostAd';
import UberLyftPostAd from './components/UberLyftPostAd';
import YellowCabPostAd from './components/YellowCabPostAd';
import GreenCabPostAd from './components/GreenCabPostAd';
import Footer from './components/Footer';
import Individual from './components/Individual';



function App() {




  return (
    <div className="App">
      <Header />

      <Switch>
        <Route path='/' exact component={Home} />
        <Route path='/uberlyft' exact component={UberLyft} />
        <Route path='/uberlyft/:uberLyftId' exact component={UberLyft} />
        <Route path='/uberlyft/individual/:uberLyftId' exact component={Individual} />
        <Route path='/yellowcab' exact component={YellowCab} />
        <Route path='/yellowcab/:yellowCabId' exact component={YellowCab} />
        <Route path='/yellowcab/individual/:yellowCabId' exact component={Individual} />
        <Route path='/greencab' exact component={GreenCab} />
        <Route path='/greencab/:greenCabId' exact component={GreenCab} />
        <Route path='/greencab/individual/:greenCabId' exact component={Individual} />
        <Route path='/postad' exact component={PostAd} />
        <Route path='/signup' exact component={Signup} />
        <Route path='/login' exact component={Login } />
        <Route path='/account' exact component={Account}/>
        <Route path='/account/:userId' exact component={Account}/>
        <Route path='/uberlyftpostad' exact component={UberLyftPostAd} />
        <Route path='/yellowcabpostad' exact component={YellowCabPostAd} />
        <Route path='/greencabpostad' exact component={GreenCabPostAd} />
      </Switch>

      <Footer />
    </div>
  );
}

export default App;
