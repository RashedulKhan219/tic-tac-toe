package tlc.api.response;

import lombok.Data;
import tlc.api.model.LoginUser;
import tlc.api.model.UberLyft;

import java.time.LocalDateTime;
import java.util.UUID;

@Data
public class UberLyftResponse {
    UUID uberLyftId;
    String adTitle;
    String adPrice;
    String adPhone;
    String adLocation;
    String adDescription;
    LoginUser user;
    LocalDateTime date;
    boolean isImage;

    public UberLyftResponse(UberLyft uberLyft) {
        uberLyftId = uberLyft.getUberLyftId();
        adTitle = uberLyft.getAdTitle();
        adPrice = uberLyft.getAdPrice();
        adPhone = uberLyft.getAdPhone();
        adLocation = uberLyft.getAdLocation();
        adDescription = uberLyft.getAdDescription();
        user = uberLyft.getUser();
        date = uberLyft.getDate();
        isImage = uberLyft.isImage();
    }
}
