package tlc.api.response;

import lombok.Data;
import tlc.api.model.GreenCab;
import tlc.api.model.LoginUser;

import java.time.LocalDateTime;
import java.util.UUID;

@Data
public class GreenCabResponse {
    UUID greenCabId;
    String adTitle;
    String adPrice;
    String adPhone;
    String adLocation;
    String adDescription;
    LoginUser user;
    LocalDateTime date;
    boolean isImage;

    public GreenCabResponse(GreenCab greenCab) {
        greenCabId = greenCab.getGreenCabId();
        adTitle = greenCab.getAdTitle();
        adPrice = greenCab.getAdPrice();
        adPhone = greenCab.getAdPhone();
        adLocation = greenCab.getAdLocation();
        adDescription = greenCab.getAdDescription();
        user = greenCab.getUser();
        date = greenCab.getDate();
        isImage = greenCab.isImage();
    }
}
