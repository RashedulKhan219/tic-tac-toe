package tlc.api.response;

import lombok.Data;
import tlc.api.model.User;

import java.util.UUID;

@Data
public class UserResponse {
    UUID userId;
    String fullName;
    String email;

    public UserResponse(User user) {
        userId = user.getUserId();
        fullName = user.getFullName();
        email = user.getEmail();
    }
}
