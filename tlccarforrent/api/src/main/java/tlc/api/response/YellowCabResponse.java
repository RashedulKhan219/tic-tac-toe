package tlc.api.response;

import lombok.Data;
import tlc.api.model.LoginUser;
import tlc.api.model.YellowCab;

import java.time.LocalDateTime;
import java.util.UUID;

@Data
public class YellowCabResponse {
    UUID yellowCabId;
    String adTitle;
    String adPrice;
    String adPhone;
    String adLocation;
    String adDescription;
    LoginUser user;
    LocalDateTime date;
    boolean isImage;

    public YellowCabResponse(YellowCab yellowCab) {
        yellowCabId = yellowCab.getYellowCabId();
        adTitle = yellowCab.getAdTitle();
        adPrice = yellowCab.getAdPrice();
        adPhone = yellowCab.getAdPhone();
        adLocation = yellowCab.getAdLocation();
        adDescription = yellowCab.getAdDescription();
        user = yellowCab.getUser();
        date = yellowCab.getDate();
        isImage = yellowCab.isImage();
    }
}
