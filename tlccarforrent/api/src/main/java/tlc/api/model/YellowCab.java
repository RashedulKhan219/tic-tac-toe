package tlc.api.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.UUID;

@Data
@AllArgsConstructor
public class YellowCab {
    UUID yellowCabId;
    String adTitle;
    String adPrice;
    String adPhone;
    String adLocation;
    String adDescription;
    LoginUser user;
    LocalDateTime date;
    boolean isImage;

    public YellowCab() {
        yellowCabId = UUID.randomUUID();
        date = LocalDateTime.now();
    }

}
