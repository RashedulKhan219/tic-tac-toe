package tlc.api.model;

import lombok.Data;

import java.util.UUID;

@Data
public class UploadFile {

    UUID imgFileId;
    String fileName;
    String fileType;
    byte[] fileData;

    public UploadFile() {
        imgFileId = UUID.randomUUID();
    }
}
