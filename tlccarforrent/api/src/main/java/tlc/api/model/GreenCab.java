package tlc.api.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.UUID;

@Data
@AllArgsConstructor
public class GreenCab {
    UUID greenCabId;
    String adTitle;
    String adPrice;
    String adPhone;
    String adLocation;
    String adDescription;
    LoginUser user;
    LocalDateTime date;
    boolean isImage;

    public GreenCab() {
        greenCabId = UUID.randomUUID();
        date = LocalDateTime.now();
    }

}
