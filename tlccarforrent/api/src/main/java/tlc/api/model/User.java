package tlc.api.model;

import lombok.Data;

import java.util.UUID;

@Data
public class User {

    UUID userId;
    String fullName;
    String email;
    String password;

    public User() {
        userId = UUID.randomUUID();
    }
}
