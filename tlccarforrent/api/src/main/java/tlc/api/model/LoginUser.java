package tlc.api.model;

import lombok.Data;

import java.util.UUID;

@Data
public class LoginUser {
    UUID userId;
    String fullName;
    String email;

    public LoginUser() {

    }
}
