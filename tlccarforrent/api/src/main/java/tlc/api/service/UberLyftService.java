package tlc.api.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tlc.api.model.UberLyft;
import tlc.api.response.UberLyftResponse;

import java.util.*;

@Service
public class UberLyftService {

    @Autowired
    UploadFileService uploadFileService;

    Map<UUID, UberLyft> uberLyftHashMap = new HashMap<>();

    public UberLyftResponse createUberLyftAd(UberLyft uberLyft) {
        uberLyftHashMap.put(uberLyft.getUberLyftId(), uberLyft);
        return new UberLyftResponse(uberLyft);
    }

    public UberLyft getUberLyftAd(UUID uberLyftId) {
        return uberLyftHashMap.get(uberLyftId);
    }

    public List<UberLyftResponse> getUberLyftAds() {
        List<UberLyftResponse> uberLyftResponseList = new ArrayList<>();
        for (UberLyft uberLyft : uberLyftHashMap.values()) {
            uberLyftResponseList.add(new UberLyftResponse(uberLyft));
        }
        //sorted
        uberLyftResponseList.sort(Comparator.comparing(UberLyftResponse::getDate).reversed());
        return uberLyftResponseList;
    }

    public List<UberLyftResponse> getAllAdByUserId(UUID userId) {
        List<UberLyftResponse> allUberLyftAds = new ArrayList<>();
        for (UberLyft uberLyft : uberLyftHashMap.values()) {
            if (uberLyft.getUser().getUserId().equals(userId)) {
                allUberLyftAds.add(new UberLyftResponse(uberLyft));
            }
        }
        return allUberLyftAds;

    }

    public UberLyftResponse updateUberLyftAd(UUID uberLyftId, UberLyft uberLyftRequest) {
        UberLyft uberLyft = uberLyftHashMap.get(uberLyftId);
        uberLyft.setAdTitle(uberLyftRequest.getAdTitle());
        uberLyft.setAdPrice(uberLyftRequest.getAdPrice());
        uberLyft.setAdLocation(uberLyftRequest.getAdLocation());
        uberLyft.setAdPhone(uberLyftRequest.getAdPhone());
        uberLyft.setAdDescription(uberLyftRequest.getAdDescription());
        uberLyft.setImage(uberLyftRequest.isImage());
        uberLyftHashMap.put(uberLyftId, uberLyft);
        return new UberLyftResponse(uberLyft);
    }

    public String removeUberLyftAd(UUID uberLyftId) {
        uberLyftHashMap.remove(uberLyftId);
        uploadFileService.imgFileHashMap.remove(uberLyftId);
        return "Successfully Removed";
    }
}
