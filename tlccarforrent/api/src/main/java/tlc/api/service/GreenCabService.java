package tlc.api.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tlc.api.model.GreenCab;
import tlc.api.response.GreenCabResponse;

import java.util.*;

@Service
public class GreenCabService {

    @Autowired
    UploadFileService uploadFileService;

    Map<UUID, GreenCab> greenCabHashMap = new HashMap<>();

    public GreenCabResponse createGreenCabAd(GreenCab greenCab) {
        greenCabHashMap.put(greenCab.getGreenCabId(), greenCab);
        return new GreenCabResponse(greenCab);
    }

    public GreenCab getGreenCabAd(UUID greenCabId) {
        return greenCabHashMap.get(greenCabId);
    }

    public List<GreenCabResponse> getGreenCabAds() {
        List<GreenCabResponse> greenCabResponseList = new ArrayList<>();
        for (GreenCab greenCab : greenCabHashMap.values()) {
            greenCabResponseList.add(new GreenCabResponse(greenCab));
        }
        //sorted
        greenCabResponseList.sort(Comparator.comparing(GreenCabResponse::getDate).reversed());
        return greenCabResponseList;
    }

    public List<GreenCabResponse> getAllAdByUserId(UUID userId) {
        List<GreenCabResponse> allGreenCabAds = new ArrayList<>();
        for (GreenCab greenCab : greenCabHashMap.values()) {
            if (greenCab.getUser().getUserId().equals(userId)) {
                allGreenCabAds.add(new GreenCabResponse(greenCab));
            }
        }
        return allGreenCabAds;

    }

    public GreenCabResponse updateGreenCabAd(UUID greenCabId, GreenCab greenCabRequest) {
        GreenCab greenCab = greenCabHashMap.get(greenCabId);
        greenCab.setAdTitle(greenCabRequest.getAdTitle());
        greenCab.setAdPrice(greenCabRequest.getAdPrice());
        greenCab.setAdLocation(greenCabRequest.getAdLocation());
        greenCab.setAdPhone(greenCabRequest.getAdPhone());
        greenCab.setAdDescription(greenCabRequest.getAdDescription());
        greenCab.setImage(greenCabRequest.isImage());
        greenCabHashMap.put(greenCabId, greenCab);
        return new GreenCabResponse(greenCab);
    }

    public String removeGreenCabAd(UUID greenCabId) {
        greenCabHashMap.remove(greenCabId);
        uploadFileService.imgFileHashMap.remove(greenCabId);
        return "Successfully Removed";
    }
}
