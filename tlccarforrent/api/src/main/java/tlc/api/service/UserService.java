package tlc.api.service;

import org.springframework.stereotype.Service;
import tlc.api.model.User;
import tlc.api.response.UserResponse;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Service
public class UserService {

    Map<UUID, User> userMap = new HashMap<>();

    public UserResponse createUser(User user) {
        for (User value : userMap.values()) {
            if (value.getEmail().equals(user.getEmail())) {
                return null;
            }
        }
        userMap.put(user.getUserId(), user);
        return new UserResponse(user);
    }

    public UserResponse findUser(UUID userId) {
        return new UserResponse(userMap.get(userId));
    }

    public UserResponse loginValidation(String email, String password) {
        for (User value : userMap.values()) {
            if (value.getEmail().equals(email) && value.getPassword().equals(password)) {
                return new UserResponse(value);
            }
        }
        return null;
    }

}