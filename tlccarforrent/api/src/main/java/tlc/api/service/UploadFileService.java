package tlc.api.service;

import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import tlc.api.model.UploadFile;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Service
public class UploadFileService {


    Map<UUID, UploadFile> imgFileHashMap = new HashMap<>();

    public UploadFile createImgFile(MultipartFile img, UUID imgMatchingAdId) throws IOException {
        UploadFile file = new UploadFile();
        file.setFileName(img.getOriginalFilename());
        file.setFileType(img.getContentType());
        file.setFileData(img.getBytes());
        file.setImgFileId(imgMatchingAdId);
        imgFileHashMap.put(imgMatchingAdId, file);
        return file;
    }

    public ResponseEntity<Resource> getImgFileById(UUID imgMatchingAdId) {
        UploadFile file = imgFileHashMap.get(imgMatchingAdId);
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename =\"" + file.getFileName() + "\"")
                .body(new ByteArrayResource(file.getFileData()));
    }

    public UploadFile updateImgFile(UUID imgMatchingAdId, MultipartFile file) throws IOException {

        UploadFile img = imgFileHashMap.get(imgMatchingAdId);
        img.setFileData(file.getBytes());
        img.setFileType(file.getContentType());
        img.setFileName(file.getOriginalFilename());
        return imgFileHashMap.put(imgMatchingAdId, img);
    }

}
