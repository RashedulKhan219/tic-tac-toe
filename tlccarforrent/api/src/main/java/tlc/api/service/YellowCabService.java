package tlc.api.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tlc.api.model.YellowCab;
import tlc.api.response.YellowCabResponse;

import java.util.*;

@Service
public class YellowCabService {

    @Autowired
    UploadFileService uploadFileService;

    Map<UUID, YellowCab> yellowCabHashMap = new HashMap<>();

    public YellowCabResponse createYellowCabAd(YellowCab yellowCab) {
        yellowCabHashMap.put(yellowCab.getYellowCabId(), yellowCab);
        return new YellowCabResponse(yellowCab);
    }

    public YellowCab getYellowCabAd(UUID yellowCabId) {
        return yellowCabHashMap.get(yellowCabId);
    }

    public List<YellowCabResponse> getYellowCabAds() {
        List<YellowCabResponse> yellowCabResponseList = new ArrayList<>();
        for (YellowCab yellowCab : yellowCabHashMap.values()) {
            yellowCabResponseList.add(new YellowCabResponse(yellowCab));
        }
        //sorted
        yellowCabResponseList.sort(Comparator.comparing(YellowCabResponse::getDate).reversed());
        return yellowCabResponseList;
    }

    public List<YellowCabResponse> getAllAdByUserId(UUID userId) {
        List<YellowCabResponse> allYellowCabAds = new ArrayList<>();
        for (YellowCab yellowCab : yellowCabHashMap.values()) {
            if (yellowCab.getUser().getUserId().equals(userId)) {
                allYellowCabAds.add(new YellowCabResponse(yellowCab));
            }
        }
        return allYellowCabAds;

    }

    public YellowCabResponse updateYellowCabAd(UUID yellowCabId, YellowCab yellowCabRequest) {
        YellowCab yellowCab = yellowCabHashMap.get(yellowCabId);
        yellowCab.setAdTitle(yellowCabRequest.getAdTitle());
        yellowCab.setAdPrice(yellowCabRequest.getAdPrice());
        yellowCab.setAdLocation(yellowCabRequest.getAdLocation());
        yellowCab.setAdPhone(yellowCabRequest.getAdPhone());
        yellowCab.setAdDescription(yellowCabRequest.getAdDescription());
        yellowCab.setImage(yellowCabRequest.isImage());
        yellowCabHashMap.put(yellowCabId, yellowCab);
        return new YellowCabResponse(yellowCab);
    }

    public String removeYellowCabAd(UUID yellowCabId) {
        yellowCabHashMap.remove(yellowCabId);
        uploadFileService.imgFileHashMap.remove(yellowCabId);
        return "Successfully Removed";
    }
}
