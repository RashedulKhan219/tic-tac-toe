package tlc.api.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import tlc.api.model.User;
import tlc.api.response.UserResponse;
import tlc.api.service.UserService;

import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping("/api/user")

public class UserApi {
    @Autowired
    UserService userService;

    @PostMapping()
    public UserResponse createUser(@RequestBody User user) {
        return userService.createUser(user);
    }

    @PostMapping("/{email}/{password}")
    public UserResponse loginValidation(@PathVariable String email, @PathVariable String password) {
        return userService.loginValidation(email, password);
    }

    @GetMapping("/{userId}")
    public UserResponse findUser(@PathVariable UUID userId) {
        return userService.findUser(userId);
    }

}