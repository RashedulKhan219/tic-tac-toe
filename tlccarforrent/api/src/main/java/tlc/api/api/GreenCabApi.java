package tlc.api.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import tlc.api.model.GreenCab;
import tlc.api.response.GreenCabResponse;
import tlc.api.service.GreenCabService;

import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping("/api/greencab")
public class GreenCabApi {
    @Autowired
    GreenCabService greenCabService;

    @PostMapping
    public GreenCabResponse createGreenCabAd(@RequestBody GreenCab greenCab) {
        return greenCabService.createGreenCabAd(greenCab);
    }

    @GetMapping
    public List<GreenCabResponse> getGreenCabAds() {
        return greenCabService.getGreenCabAds();
    }

    @GetMapping("/{greenCabId}")
    public GreenCab getGreenCabAd(@PathVariable UUID greenCabId) {
        return greenCabService.getGreenCabAd(greenCabId);
    }

    @GetMapping("/user/{userId}")
    public List<GreenCabResponse> getAllAdByUserId(@PathVariable UUID userId) {
        return greenCabService.getAllAdByUserId(userId);
    }

    @PutMapping("/{greenCabId}")
    public GreenCabResponse updateGreenCabAd(@PathVariable UUID greenCabId, @RequestBody GreenCab greenCabRequest) {
        return greenCabService.updateGreenCabAd(greenCabId, greenCabRequest);
    }

    @DeleteMapping("/{greenCabId}")
    public String removeGreenCabAd(@PathVariable UUID greenCabId) {
        return greenCabService.removeGreenCabAd(greenCabId);
    }
}
