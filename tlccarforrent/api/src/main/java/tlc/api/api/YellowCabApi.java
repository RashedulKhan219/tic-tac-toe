package tlc.api.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import tlc.api.model.YellowCab;
import tlc.api.response.YellowCabResponse;
import tlc.api.service.YellowCabService;

import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping("/api/yellowcab")
public class YellowCabApi {
    @Autowired
    YellowCabService yellowCabService;

    @PostMapping
    public YellowCabResponse createYellowCabAd(@RequestBody YellowCab yellowCab) {
        return yellowCabService.createYellowCabAd(yellowCab);
    }

    @GetMapping
    public List<YellowCabResponse> getYellowCabAds() {
        return yellowCabService.getYellowCabAds();
    }

    @GetMapping("/{yellowCabId}")
    public YellowCab getYellowCabAd(@PathVariable UUID yellowCabId) {
        return yellowCabService.getYellowCabAd(yellowCabId);
    }

    @GetMapping("/user/{userId}")
    public List<YellowCabResponse> getAllAdByUserId(@PathVariable UUID userId) {
        return yellowCabService.getAllAdByUserId(userId);
    }

    @PutMapping("/{yellowCabId}")
    public YellowCabResponse updateYellowCabAd(@PathVariable UUID yellowCabId, @RequestBody YellowCab yellowCabRequest) {
        return yellowCabService.updateYellowCabAd(yellowCabId, yellowCabRequest);
    }

    @DeleteMapping("/{yellowCabId}")
    public String removeYellowCabAd(@PathVariable UUID yellowCabId) {
        return yellowCabService.removeYellowCabAd(yellowCabId);
    }
}
