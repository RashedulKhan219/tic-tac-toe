package tlc.api.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import tlc.api.model.UploadFile;
import tlc.api.service.UploadFileService;

import java.io.IOException;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping("/api/uploadfile")
public class UploadFileApi {

    @Autowired
    UploadFileService uploadFileService;

    @GetMapping("/{imgFileId}")
    @ResponseBody
    public ResponseEntity<Resource> getImgFileById(@PathVariable() UUID imgFileId) {
        return uploadFileService.getImgFileById(imgFileId);
    }

    @PutMapping("/{imgMatchingAdId}")
    public UploadFile updateImgFile(@PathVariable UUID imgMatchingAdId, @RequestParam("file") MultipartFile file) throws IOException {
        return uploadFileService.updateImgFile(imgMatchingAdId, file);
    }

    @PostMapping("/{imgMatchingAdId}")
    public UploadFile createImgFile(@RequestParam("file") MultipartFile img, @PathVariable UUID imgMatchingAdId) throws IOException {
        return uploadFileService.createImgFile(img, imgMatchingAdId);
    }


}
