package tlc.api.api;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import tlc.api.model.UberLyft;
import tlc.api.response.UberLyftResponse;
import tlc.api.service.UberLyftService;

import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping("/api/uberlyft")
public class UberLyftApi {
    @Autowired
    UberLyftService uberLyftService;

    @PostMapping
    public UberLyftResponse createUberLyftAd(@RequestBody UberLyft uberLyft) {
        return uberLyftService.createUberLyftAd(uberLyft);
    }

    @GetMapping
    public List<UberLyftResponse> getUberLyftAds() {
        return uberLyftService.getUberLyftAds();
    }

    @GetMapping("/{uberLyftId}")
    public UberLyft getUberLyftAd(@PathVariable UUID uberLyftId) {
        return uberLyftService.getUberLyftAd(uberLyftId);
    }

    @GetMapping("/user/{userId}")
    public List<UberLyftResponse> getAllAdByUserId(@PathVariable UUID userId) {
        return uberLyftService.getAllAdByUserId(userId);
    }

    @PutMapping("/{uberLyftId}")
    public UberLyftResponse updateUberLyftAd(@PathVariable UUID uberLyftId, @RequestBody UberLyft uberLyftRequest) {
        return uberLyftService.updateUberLyftAd(uberLyftId, uberLyftRequest);
    }

    @DeleteMapping("/{uberLyftId}")
    public String removeUberLyftAd(@PathVariable UUID uberLyftId) {
        return uberLyftService.removeUberLyftAd(uberLyftId);
    }
}
