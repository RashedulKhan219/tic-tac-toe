git pull

echo --- Packaging JAR ---
mvn clean package -Dmaven.test.skip=true

echo --- Building image ---
docker build --no-cache -t student/sadek-tlccar .

echo --- Stoppping old app ---
docker kill student-sadek-tlccar
docker rm student-sadek-tlccar

#echo --- Starting container ---
#docker run -p 6060:8080 -d --name student-sadek-tlccar student/sadek-tlccar

echo --- Done ---
