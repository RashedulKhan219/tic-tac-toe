import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Switch,Route } from 'react-router-dom';
import Footer from './component/home/Footer';
import OfficeLogin from './component/office/OfficeLogin';
import OfficeSighUp from './component/office/OfficeSignUp';
import OfficeHome from './component/office/OfficeHome';

import OfficeProfile from './component/office/OfficeProfile';
import HomePage from './component/home/HomePage';
import PatientResponse from './component/patient/PatientResponse';
import UpdatePatient from './component/patient/UpdatePatient';
import Welcome from './component/patient/Welcome';
import Checkin from './component/patient/Checkin';
import PatientList from './component/patient/PatientList';
import ForgetPassword from './component/office/ForgetPassword';
import VerifyToken from './component/office/VerifyToken';
import ResetPassword from './component/office/ResetPassword';








function App() {
  return (
    <div className="App">

      <header className="App-header">

        <Switch>
          <Route path="/" exact component={HomePage} />
          <Route path="/checkin" exact component={Checkin} />
          <Route path="/patientlist" exact component={PatientList} />
          <Route path="/patientresponse/:patientId" exact component={PatientResponse} />
          <Route path="/welcome/:patientId" exact component={Welcome} />
          <Route path="/officelogin" exact component={OfficeLogin} />
          <Route path="/officesignup" exact component={OfficeSighUp} />
          <Route path="/officehome/:officeId" exact component={OfficeHome} />
          <Route path="/updatepatient/:patientId/:officeId" exact component={UpdatePatient} />
          <Route path="/forgetpassword" exact component={ForgetPassword} />
          <Route path="/officeprofile/:officeId" exact component={OfficeProfile}/>
          <Route path="/verifytoken/:tokenId/:officeId" exact component={VerifyToken}/>
          <Route path="/resetpassword/:officeId" exact component={ResetPassword}/>
          
        </Switch>
        
      </header>

      <Footer/>
    </div>
  );
}

export default App;
