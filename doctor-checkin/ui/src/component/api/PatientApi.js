import axios from 'axios'
import CONSTANT from '../utils/CONSTANT'


class PatientApi {

static url = CONSTANT.URL.PATIENT;

static createPatient(firstName, lastName, dateOfBirth,  time){
    const payLoad = {firstName: firstName, lastName: lastName, dateOfBirth: dateOfBirth, time: time}
    return axios.post(this.url, payLoad).then(response => response.data);
}

static getAllPatient(){
    return axios.get(this.url).then(response => response.data);
}

static getPatientById(patientId){
    return axios.get(this.url + "/" + patientId).then(response => response.data);
}

static getPatientByDateOfBirth(dateOfBirth){
    return axios.get(this.url + "/date/" + dateOfBirth).then(response => response.data);
}

static updatePatient(patientId, firstName, lastName, dateOfBirth){
    const payLoad = {firstName: firstName, lastName: lastName, dateOfBirth: dateOfBirth};
    return axios.put(this.url + "/" + patientId, payLoad).then(response => response.data);
}

static deletePatientById(patientId){
   return axios.delete(this.url + "/" + patientId).then(response => response.data);
}


}

export default PatientApi