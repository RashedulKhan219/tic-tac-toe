import axios from 'axios';
import CONSTANT from '../utils/CONSTANT';

class EmailApi {

    static url = CONSTANT.URL.EMAIL;

    static verifyEmailAndCreateToken(email){
        return axios.post(this.url + "/" + email).then(response => response.data);
    }

    static verifyToken(tokenId){
        return axios.get(this.url + "/" + tokenId).then(response => response.data);
    }
}

export default EmailApi