import axios from 'axios';
import CONSTANT from '../utils/CONSTANT';

class UploadImage{

    static url = CONSTANT.URL.UPLOAD;

    static createFile(file, officeId){
        return axios.post(this.url + "/" + officeId, file).then(response => response.data);
    }

    static getFileByFileId(fileId){
        return axios.get(this.url + "/" + fileId).then( response => response.data);
    }

    static getFileByOfficeId(officeId){
        return axios.get(this.url + "/office/" + officeId).then(response => response.data);
    }

}

export default UploadImage