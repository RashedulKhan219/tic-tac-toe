import axios from 'axios'
import CONSTANT from '../utils/CONSTANT'


class OfficeApi {

    static url = CONSTANT.URL.OFFICE;


    static getOfficeProfile(officeId) {
        return axios.get(this.url + "/" + officeId).then(response => response.data);
    }

    static loginAtOffice(email,password) {
        return axios.get(this.url + "/" + email + "/" + password).then(response => response.data);
    }

    static createOfficeProfile(email, password, confirmPass) {
        const payload = { email: email, password: password, confirmPass: confirmPass };
        return axios.post(this.url,payload).then(response => response.data);
    }

    static updateProfile(officeId, email){
        const payLoad = {email: email};
        return axios.put(this.url + "/" + officeId, payLoad).then(response => response.data);
    }

    static resetPassword(password, officeId){
        return axios.put(this.url + "/" + password + "/" + officeId).then(response => response.data);
    }

}

export default OfficeApi