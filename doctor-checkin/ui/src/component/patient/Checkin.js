import React,{ useState } from 'react'
import Navigation from '../home/Navigation'
import { Button,Icon } from 'semantic-ui-react';
import { useHistory } from 'react-router-dom';
import moment from "moment";
import { Form,Row,Col,Spinner } from 'react-bootstrap'
import PatientApi from '../api/PatientApi';




const Checkin = () => {
    const history = useHistory();
    const [count,setCount] = useState(0);
    const [firstName,setFirstName] = useState();
    const [lastName,setLastName] = useState();
    const [dateOfBirth,setDateOfBirth] = useState(null);
    const [time,setTime] = useState(new Date().toLocaleTimeString())
    const [patient,setpatient] = useState();
    const [loading,setLoading] = useState(false);




    const handleCreatepatient = () => {
        PatientApi.createPatient(firstName,lastName,dateOfBirth,time).then(patient => {
            setpatient(patient)
            console.log(patient)
            setLoading(false);
            if (patient) {
                history.push("/welcome/" + patient.patientId);
            }
        })

    }

    const handleSubmitButton = (event) => {
        event.preventDefault();
        setLoading(true)
        setTimeout(handleCreatepatient,1000)
    }

    const handleChange = (e) => {
        setDateOfBirth(moment(e.target.value).format("MM-DD-YYYY"))

    }


    const handleNext = (e) => {
        e.preventDefault();
        setCount(count + 1);

    }



    return <div>
            <Navigation />
            <div className="container mt-5 ">

                <Form onSubmit={handleNext} className="App-check-in-2 col-lg-6 ">
                    {count === 0 ?
                        <div>
                            <Row className="text-left">
                                <Col>
                                <Form.Label>FirstName</Form.Label>
                                    <Form.Control defaultValue={firstName} required placeholder="First name" 
                                    onChange={(e) => setFirstName(e.target.value)} />
                                </Col>
                                <Col>
                                <Form.Label>LastName</Form.Label>
                                    <Form.Control defaultValue={lastName} required placeholder="Last name" 
                                    onChange={(e) => setLastName(e.target.value)} />
                                </Col>
                            </Row> </div> : null}

                    {count === 1 ?
                        <div>

                            <Row className="text-left">
                                <Col>
                                <Form.Label>Date of birth</Form.Label>
                                    <Form.Control type="date" defaultValue={dateOfBirth}  required placeholder="mm-dd-yyyy" 
                                    onChange={handleChange} />
                                </Col>
                                <Col>
                                <Form.Label>Arrival Time</Form.Label>
                                    <Form.Control as="select" defaultValue={time} required 
                                    onChange={(e) => setTime(e.target.value)} >
                                        <option defaultValue={time} required >Select Time</option>
                                        <option defaultValue={time} value={time}>{time}</option>

                                    </Form.Control>
                                </Col>
                            </Row>
                        </div> : null}

                    {count <= 1 ? <Row className="pt-5">
                        <Col>


                            <Button disabled={count <= 0} color='teal' onClick={() => setCount(count - 1)}
                                className="mr-3" content='Previous' icon='left arrow' labelPosition='left' />
                            <Button content='Next' color='facebook' type="submit" icon='right arrow' labelPosition='right' />
                        </Col>

                    </Row> : null}


                </Form>


                {count === 2 && !patient ? <Form onSubmit={handleSubmitButton} className="mb-3">

                    <h3>Now click on submit to complete your CHECK-IN.....</h3>

                    <Button disabled={count <= 0} color='teal' onClick={() => setCount(count - 1)}
                        className="mr-3" content='Previous' icon='left arrow' labelPosition='left' />

                    <Button type="submit" color='instagram' className="mt-3">
                        <Icon name='arrow right' className="mr-2" />
                        Submit

                        {loading && <Spinner animation='border' size='sm' variant='white' />}
                    </Button>
                </Form> : null}

                <p className="text-muted">if you re-fresh you may loose your information</p>


            </div>


        </div>

}

export default Checkin