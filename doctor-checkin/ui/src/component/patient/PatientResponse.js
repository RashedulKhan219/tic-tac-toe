import React,{ useEffect,useState } from 'react'
import { useParams,useHistory } from 'react-router-dom'
import Navigation from '../home/Navigation';
import { Alert} from 'react-bootstrap';
import { Button } from 'semantic-ui-react';
import PatientApi from '../api/PatientApi';


const PatientResponse = () => {
    const { patientId } = useParams();
    const [patient,setpatient] = useState();
    const history = useHistory();

    useEffect(() => {
        PatientApi.getPatientById(patientId).then(patient => setpatient(patient))
    },[patientId])

    const handleClose = () => {
        history.push("/")
    }

    return <div>
            <Navigation response ={true} />
            <div className="container text-light">

                {patient ? <Alert className="col-lg-4 ml-auto mr-auto shadow mt-5 p-4" dismissible onClose={handleClose}>
                    <p>Name: {patient.firstName + " " + patient.lastName}</p>
                    <p>DOB: {patient.dateOfBirth}</p>
                    <p>Arrival-time: {patient.time}</p>
                    <Button color='grey' size='small' onClick={handleClose} >Close</Button>
                    
                </Alert> : null}

            </div>

        </div>
    
}

export default PatientResponse