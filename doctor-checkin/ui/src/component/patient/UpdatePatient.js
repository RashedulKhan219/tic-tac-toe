import React,{ useState,useEffect } from 'react'
import Navigation from '../home/Navigation'
import { Form,Button,Spinner, Row, Col } from 'react-bootstrap'

import { useParams,useHistory } from 'react-router-dom';
import CONSTANT from '../utils/CONSTANT';
import PatientApi from '../api/PatientApi';

const UpdatePatient = () => {

    const { patientId, officeId } = useParams();
    const [firstName,setFirstName] = useState();
    const [lastName,setLastName] = useState();
    const [dateOfBirth,setDateOfBirth] = useState();
    const [patient,setPatient] = useState();
    const history = useHistory();
    const [loading,setLoading] = useState(false);
    const [logout,setLogout] = useState(false);
    const [updatedpatient, setUpdatedpatient] = useState();

    useEffect(()=>{
        PatientApi.getPatientById(patientId).then(patient =>{
            setPatient(patient)
            setFirstName(patient.firstName)
            setLastName(patient.lastName)
            setDateOfBirth(patient.dateOfBirth)
        } )

        const saveLogout = localStorage.getItem(CONSTANT.LOCAL_STORAGE.LOGOUT);
        if(saveLogout){
            setLogout(JSON.parse(saveLogout));
        }
    },[patientId])


const handleUpdatepatient=()=>{
    PatientApi.updatePatient(patientId, firstName, lastName, dateOfBirth).then(updatedpatient =>{
        setUpdatedpatient(updatedpatient)
        setLoading(false);
        if(updatedpatient){
            history.push("/officehome/"  + officeId)
        }
    } )

}

const handleSaveButton =(event)=>{
event.preventDefault();
setLoading(true);
setTimeout(handleUpdatepatient, 1000);
}
    
    if (logout) {
        history.push('/officelogin')
    }

    return <div>
            <Navigation />
            <div className='container'>

          {patient &&     <Row>
                    <Col xs={4} className=" ml-auto mr-auto p-5 mt-5 text-left" >
                        <Form onSubmit={handleSaveButton}>
                            <h1 className="text-center">Update/patient</h1>
                            <Form.Label>FirstName:</Form.Label>
                            <Form.Control type="text" defaultValue={firstName}  className="text-capitalize mb-2" required
                                onChange={(e) => setFirstName(e.target.value)} />
                            <Form.Label>LastName:</Form.Label>
                            <Form.Control type="text" defaultValue={lastName} className="text-capitalize mb-2" required
                                onChange={(e) => setLastName(e.target.value)} />
                            <Form.Label>DOB (mm-dd-yyyy): </Form.Label>
                            <Form.Control type="text"  defaultValue={dateOfBirth}  className="mb-3" maxLength={10} required
                                onChange={(e) => setDateOfBirth(e.target.value)} />
                        
                            <Button type='submit'  className="col-lg" variant="info">Save all changes
                      {loading && <Spinner animation='border' size='sm' variant='white' />}
                            </Button>

                        </Form>
                    </Col>

                </Row>}
            </div>
        </div>
    
}

export default UpdatePatient