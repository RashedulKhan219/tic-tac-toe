import React from 'react'
import { useParams,useHistory } from 'react-router-dom'
import { Alert } from 'react-bootstrap';
import { Button,Icon } from 'semantic-ui-react';
import Navigation from '../home/Navigation';

const Welcome = () => {
    const { patientId } = useParams();
    const history = useHistory();

    const handleButton = () => {
        history.push("/")
    }

    return <div>
            <Navigation />
            {patientId ? <div className="mt-5">
                <div className="col-lg-6 ml-auto mr-auto">
                    <Alert variant='success' >

                        <h2>
                            Thank you for check in
                    </h2>
                    </Alert>
                    <p>Patient-ID: {patientId}</p>
                    <Button color='blue' onClick={handleButton}>
                        <Icon name='backward' />
                Go to home page</Button>
                </div>

            </div> : null}
        </div>
    
}

export default Welcome