import React,{ useEffect,useState } from 'react'
import Navigation from '../home/Navigation'
import { Table, Row } from 'react-bootstrap';
import { Input } from 'semantic-ui-react'
import TablePagination from '@material-ui/core/TablePagination';
import PatientApi from '../api/PatientApi';

const PatientList =()=>{

    const [patients,setPatients] = useState([]);
    const [search,setSearch] = useState('');
 
  

    useEffect(() => {
        PatientApi.getAllPatient().then(patients => setPatients(patients))
    },[])

    const [page,setPage] = React.useState(0);
    const [rowsPerPage,setRowsPerPage] = React.useState(10);

    const handleChangePage = (event,newPage) => {
        setPage(newPage);
    }

    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(parseInt(event.target.value,10));
        setPage(3);
    }
    

    return <div>
            <Navigation />
            <div className="container text-light pb-5">
                {patients.length === 0 ? <h3 className="mt-5">
                    There is no available Patient
                    </h3>: <h3 className="mt-5">Patient-List</h3>}
                    
                {patients.length !== 0 ? <Row className="mt-5">
              
                    <div className='mb-3  text-light text-left'>
                        <Input size='mini'  iconPosition='left' placeholder='Search.....' type='text' icon='search' onChange={(e) =>  setSearch(e.target.value)} />
                    </div>
                    <Table striped bordered hover variant="dark">
                        <thead>
                            <tr>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Time of arrival</th>
                                <th>Date of Birth</th>
                            </tr>
                        </thead>
                        <tbody>

                            {patients.filter(patient => {
                               if (search === '') {
                                    return patient;
                                } else if (patient.firstName.toLowerCase().includes(search.toLowerCase())
                                    || patient.lastName.toLowerCase().includes(search.toLowerCase())
                                    || patient.dateOfBirth.includes(search)
                                ) {
                                    return patient;
                                } 
                            }).slice(page * rowsPerPage,page * rowsPerPage + rowsPerPage).map((patients,index) => {
                             return   <tr key={index}>
                                    {/* <td> {patients.patientId}</td> */}
                                    <td > {patients.firstName}</td>
                                    <td > {patients.lastName}</td>
                                    <td> {patients.time}</td>
                                    <td> {patients.dateOfBirth}</td>
                                </tr>
                            })}

                        </tbody>
                    </Table>

                </Row> : null}

                {patients.length !== 0 &&  <Row> <TablePagination
                    className="bg-dark text-light"
                    component="table"
                    count={100}
                    page={page}
                    onChangePage={handleChangePage}
                    rowsPerPage={rowsPerPage}
                    onChangeRowsPerPage={handleChangeRowsPerPage}
                />
                </Row>}
              
            </div>

        </div>
    
}

export default PatientList