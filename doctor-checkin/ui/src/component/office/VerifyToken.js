import React,{ useState, useEffect } from 'react';
import Navigation from '../home/Navigation';
import { Container,InputGroup,Button,FormControl,Form,Row,Col,Spinner, Alert } from 'react-bootstrap';
import { useParams, useHistory } from 'react-router-dom';
import OfficeApi from '../api/OfficeApi';
import EmailApi from '../api/EmailApi';
import CONSTANT from '../utils/CONSTANT';


const VerifyToken = () => {

    const {tokenId, officeId} = useParams();
    const [loading,setLoading] = useState(false);
    const [office, setOffice] = useState();
    const [token, setToken] = useState();
    const history = useHistory();
    const [errorMessage,setErrorMessage] = useState();
    const [isPasswordChanged, setPasswordChanged] = useState(false);



    useEffect(()=>{
        const localpassChanged = localStorage.getItem(CONSTANT.LOCAL_STORAGE.PASSCHANGED);
        if(localpassChanged){
            setPasswordChanged(JSON.parse(localpassChanged))
        }
        OfficeApi.getOfficeProfile(officeId).then( office => setOffice(office))
    },[tokenId, officeId])

    const handleVerifyButton =(event)=>{
        event.preventDefault();
        setErrorMessage(null);
        setLoading(true)
        EmailApi.verifyToken(token).then( verified =>{
            if(verified){
                setLoading(false)
               history.push("/resetpassword/" + officeId);
            } else if(!verified){
                setLoading(false)
              return  setErrorMessage("Invalid token-ID")
            }
        })
       
    }

    const handleChange =(e)=>{
        const tokenValue = e.target.value;
         setToken(tokenValue)
         setErrorMessage(null);
    }


    if(isPasswordChanged) return history.push("/officelogin")

    return <div>
        <Navigation />
        <Container >

            {office && office.tokenId === tokenId && <p className="text-success mt-5" >A verification code has been sent to your email.</p>}
            <Row className="mt-5">
                <Col sm={5} className="ml-auto mr-auto">
                    <p>Enter verification code</p>
                    <Form onSubmit={handleVerifyButton}> 
                        <InputGroup className="mb-3">
                            <FormControl
                                placeholder="Enter verification code"
                                aria-label="Recipient's username"
                                aria-describedby="basic-addon2"
                                onChange={handleChange}
                            />
                            <InputGroup.Append>
                                <Button type="submit" variant="outline-info">
                                Verify
                                {loading && <Spinner animation='border' size='sm' variant='light' />}
                                </Button>
                            </InputGroup.Append>
                        </InputGroup>
                        {errorMessage && <Alert className="text-danger" dismissible onClose={() => setErrorMessage(null)}>
                            {errorMessage}
                            </Alert>}
                    </Form>
                </Col>
            </Row>

        </Container>

    </div>
}

export default VerifyToken