import React,{ useState,useEffect } from 'react';
import { Container,Row,Col,Form,FormControl,Button,Alert } from 'react-bootstrap';
import { useParams,useHistory } from 'react-router-dom';
import Navigation from '../home/Navigation';
import OfficeApi from '../api/OfficeApi';
import CONSTANT from '../utils/CONSTANT';


const ResetPassword = () => {

    const { officeId } = useParams();
    const [password,setPassword] = useState();
    const [confirmPassword,setConfirmPassword] = useState();
    const [error,setError] = useState();
    const [alert,setAlert] = useState();
    const [loading,setLoading] = useState();
    const history = useHistory();
    const [isPasswordChanged,setPasswordChanged] = useState(false);
    const [currentType, setCurrentType] = useState('password');


    useEffect(() => {
        const localpassChanged = localStorage.getItem(CONSTANT.LOCAL_STORAGE.PASSCHANGED);
        if (localpassChanged) {
            setPasswordChanged(JSON.parse(localpassChanged))
        }

    },[])

    const handleResetPassword = (event) => {
        event.preventDefault();
        if (password === confirmPassword) {
            OfficeApi.resetPassword(password,officeId).then(office => {
                if (office) {
                    localStorage.setItem(CONSTANT.LOCAL_STORAGE.PASSCHANGED,JSON.stringify(true));
                    return history.push("/officelogin");
                }
            })
        }
        else if (password !== confirmPassword) {
            setError("password didn't match...!")
            setLoading(false);
            return setAlert(false);
        }

    }

    const handleShowPassword = () => {
        if(currentType === 'password'){
            setCurrentType('text');
        } else {
            setCurrentType("password");
        }
    }

    if (isPasswordChanged) return history.push("/officelogin")

    return <div>
        <Navigation />

        <Container>
            <h2 className="mt-5">Reset Password</h2>
            <Row className="mt-5">
                <Col sm={5} className="ml-auto mr-auto text-left">

                    <Form onSubmit={handleResetPassword}>
                        <Form.Label>Enter new password</Form.Label>
                        <FormControl
                            type={currentType && currentType}
                            className="mb-3"
                            required
                            placeholder="Enter new password"
                            aria-label="Recipient's username"
                            aria-describedby="basic-addon2"
                            maxLength={15}
                            minLength={5}
                            onChange={(e) => setPassword(e.target.value)}
                        />
                        <Form.Label>Confirm-password</Form.Label>
                        <FormControl
                            type={currentType && currentType}
                            className="mb-4"
                            required
                            placeholder="confirm password"
                            aria-label="Recipient's username"
                            aria-describedby="basic-addon2"
                            maxLength={15}
                            minLength={5}
                            onChange={(e) => setConfirmPassword(e.target.value)}
                        />
                        <Form.Group controlId="formBasicCheckbox">
                            <Form.Check type="checkbox" label="Show password" onClick={handleShowPassword} />
                        </Form.Group>

                        <Button type="submit" variant="success">
                            Submit
                        </Button>

                        {error && <Alert className="text-danger" dismissible onClose={() => setError(false)} >
                            {error}
                        </Alert>}
                    </Form>
                </Col>
            </Row>

        </Container>
    </div>
}

export default ResetPassword