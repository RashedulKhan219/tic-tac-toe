import React,{ useState, useEffect } from 'react'
import { Form,Button,Alert } from 'react-bootstrap'
import Navigation from '../home/Navigation';
import { useHistory,Link } from 'react-router-dom';
import OfficeApi from '../api/OfficeApi';
import CONSTANT from '../utils/CONSTANT';


const OfficeLogin = () => {
 
    const [email,setEmail] = useState();
    const [password,setPassword] = useState();
    const history = useHistory();
    const [alert,setAlert] = useState(false);
    const [isLoggedIn, setIsLoggedIn] = useState();
    const [isPasswordChanged, setPasswordChanged] = useState(false);
    const [currentType, setCurrentType] = useState('password');


    useEffect(()=>{
        const isLoggedInLocal = localStorage.getItem(CONSTANT.LOCAL_STORAGE.LOGIN);
        const localpassChanged = localStorage.getItem(CONSTANT.LOCAL_STORAGE.PASSCHANGED);
        if(isLoggedIn && localpassChanged){
            setIsLoggedIn(JSON.parse(isLoggedInLocal))
            setPasswordChanged(JSON.parse(localpassChanged))
        }
    },[isLoggedIn])


    const handleLogin = (event) => {
        event.preventDefault();
        OfficeApi.loginAtOffice(email, password).then(office => {
            localStorage.setItem(CONSTANT.LOCAL_STORAGE.LOGOUT,JSON.stringify(false))
            localStorage.setItem(CONSTANT.LOCAL_STORAGE.LOGIN,JSON.stringify(true))
            localStorage.setItem(CONSTANT.LOCAL_STORAGE.OFFICE, JSON.stringify(office))
            if (office) {
                setAlert(false)
                history.push("/officehome/" + office.officeId);
            } else if (!office.officeId) {
              return  setAlert(true)
            }
        })

    }

    const handleShowPassword = () => {
        if(currentType === 'password'){
            setCurrentType('text');
        } else {
            setCurrentType("password");
        }
    }

    return <div >
            <Navigation />
            <div className="container ">


                <div className="row ">
                    <div className="col-lg-5 col-md-5 col-sm-5 ml-auto mr-auto p-5 rounded " >
                    <h2 className="p-3">Office's use only</h2>
                        <Form onSubmit={handleLogin} className="text-left">
                        <Form.Label>Email</Form.Label>
                            <Form.Control type="email" placeholder="Enter business email" required className="mb-3"
                                onChange={(e) => setEmail(e.target.value.toLowerCase())} />
                                    <Form.Label>Password</Form.Label>
                            <Form.Control type={currentType && currentType} placeholder="password" required maxLength={12}
                                onChange={(e) => setPassword(e.target.value)} className="mb-2" />
                                <div className="text-right mb-3">
                                <Link to="/forgetpassword" > Forget password?</Link>
                                
                                </div>
                                <Form.Group controlId="formBasicCheckbox">
                            <Form.Check type="checkbox" label="Show password" onClick={handleShowPassword} />
                        </Form.Group>
                            <Button type="submit" className="col-lg mb-4">
                                <i className="bi bi-arrow-right-large"></i>
                                <svg xmlns="http://www.w3.org/2000/svg" width="30" height="20" fill="currentColor" className="bi bi-arrow-right-short" viewBox="0 0 16 16">
                                    <path fill-rule="evenodd" d="M4 8a.5.5 0 0 1 .5-.5h5.793L8.146 5.354a.5.5 0 1 1 .708-.708l3 3a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708-.708L10.293 8.5H4.5A.5.5 0 0 1 4 8z" />
                                </svg>
                            Login
                            </Button>
                            {alert && <Alert variant="danger" onClose={() => setAlert(false)} dismissible className="text-left">
                                <p>The email or password you entered doesn't match with the records! Please enter valid email and password.</p>
                            </Alert>}
                            <p className="text-center">Don't have account? <Link className="text-warning" to="/officesignup">SignUp</Link></p>
                        </Form>
               
                       {isPasswordChanged && <Alert variant="success" dismissible onClose={() => setPasswordChanged(false)}>
                           Password has changed successfully.
                           </Alert>}
                    </div>
                </div>

            </div>

        </div>

}

export default OfficeLogin