import React,{ useEffect,useState } from 'react'
import { useParams,useHistory } from 'react-router-dom'
import OfficeApi from '../api/OfficeApi';
import Navigation from '../home/Navigation';
import { Table,Button,Modal,Alert,Spinner,Row, Col } from 'react-bootstrap';
import CONSTANT from '../utils/CONSTANT';
import UserApi from '../api/PatientApi';
import { Icon,Input } from 'semantic-ui-react';
import TablePagination from '@material-ui/core/TablePagination';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import ReactTimeAgo from 'react-time-ago'
import PatientApi from '../api/PatientApi';



const OfficeHome = () => {
    const { officeId } = useParams();
    const [office,setoffice] = useState();
    const [patients,setPatients] = useState([]);
    const history = useHistory();
    const [logout,setLogout] = useState(false);
    const [patiendId,setpatiendId] = useState();
    const [show,setShow] = useState(false);
    const [messege,setMessege] = useState();
    const [value,setTableValue] = useState();
    const [search,setSearch] = useState('');
    const [loading,setLoading] = useState(false);
    const [logoutClicked,setLogoutClicked] = useState(false);



    useEffect(() => {
        PatientApi.getAllPatient().then(patients => setPatients(patients))
        OfficeApi.getOfficeProfile(officeId).then(office => setoffice(office))
        const savleLogout = localStorage.getItem(CONSTANT.LOCAL_STORAGE.LOGOUT)
        if (savleLogout) {
            setLogout(JSON.parse(savleLogout))
        }
    },[officeId])


    const handleDelete = () => {
        if (patiendId) {
            PatientApi.deletePatientById(patiendId).then(office => {
                if(office){
                setShow(false);
                setLoading(false);
                UserApi.getAllPatient().then(patients => setPatients(patients))
                }
            })
        }

    }

    const handleYesDelete = (event) => {
        event.preventDefault();
        setLoading(true);
        setTimeout(handleDelete,1000);
    }

    const handleClose = () => {
        setShow(false)
    };
    const handleDeleteButton = (e) => {
        setpatiendId(e.target.value)
        setShow(true);
        setLogoutClicked(false);
    }


    const handleEditButton = (e) => {
        const user_Id = e.target.value;
        console.log(user_Id)
        if (user_Id) {
            history.push('/updatepatient/' + user_Id + "/" + officeId);
        }
    }

    const handleLogout = () => {
        localStorage.setItem(CONSTANT.LOCAL_STORAGE.LOGOUT,JSON.stringify(true));
        localStorage.setItem(CONSTANT.LOCAL_STORAGE.LOGIN,JSON.stringify(false));
        localStorage.setItem(CONSTANT.LOCAL_STORAGE.OFFICE,JSON.stringify(null))
        setoffice(null);
        return history.push("/officelogin")
    }

    const handleLogoutButton = () => {
        setShow(true);
        setLogoutClicked(true);
    }

    if (logout) {
        history.push('/officelogin')
    }


    const [page,setPage] = React.useState(0);
    const [rowsPerPage,setRowsPerPage] = React.useState(10);

    const handleChangePage = (event,newPage) => {
        setPage(newPage);
    }

    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(parseInt(event.target.value,10));
        setPage(3);
    }


    const handleOfficeProfile = () => {
        history.push("/officeprofile/" + officeId)
    }


    return <div>
        <Navigation />

        <div className="container">
            <div className='text-right mt-3'>

                <Button onClick={handleOfficeProfile} variant='secondary' className='mr-2'>
                    <Icon name='user md' />
                        profile
                    </Button>
                <Button onClick={handleLogoutButton}>
                    <Icon name='sign-out' />
                        Logout
                    </Button>
            </div>


            {office && <Row className='row mt-5'>
                <Col xs={12} md={6} xl={6} className='ml-auto mr-auto shadow p-3'>
                    <Icon name='user md' size='big' />
                    <h2 className='text-uppercase'>
                        Welcome to doctor's office
                    </h2>
                </Col>

            </Row>}

            <div className='mt-3'>
                {patients.length !== 0 ? <h3 >Patient-List</h3> : <Alert>
                    <p> Patients are not availble</p>
                </Alert>}
                {messege ? <Alert className='col-lg-4 ml-auto mr-auto mt-4' variant='danger'
                 onClose={() => setMessege(null)} dismissible>
                    {messege}
                </Alert> : null}
            </div>


            {patients.length !== 0 ? <Row className="mt-5">
                    
                <div className='text-left mb-3 text-light'>
                    <Input size='mini' 
                    iconPosition='left' 
                    placeholder="Search...." 
                    type='text' 
                    icon='search' 
                    onChange={(e) => setSearch(e.target.value)} />
                </div>
                <Table spellCheck striped bordered hover variant="dark">
                    <thead>
                        <tr >
                            <th>Patient-ID</th>
                            <th >First Name</th>
                            <th>Last Name</th>
                            <th>Time of arrival</th>
                            <th >Date of Birth</th>
                            <th>Checked in</th>
                            <th>Edit</th>
                            <th>Remove</th>
                        </tr>
                    </thead>
                    <tbody >

                        {patients.filter(user => {
                            if (search === '') {
                                return user;
                            } else if (user.firstName.toLocaleLowerCase().includes(search.toLocaleLowerCase())
                                || user.lastName.toLocaleLowerCase().includes(search.toLocaleLowerCase())
                                || user.dateOfBirth.includes(search)) {
                                return user;
                            }

                        }).slice(page * rowsPerPage,page * rowsPerPage + rowsPerPage).map((user,index) => {
                            return <tr key={index} onChange={(e) => setTableValue(user.dateOfBirth)}>
                                <td> {user.patientId}</td>
                                <td className="text-capitalize"> {user.firstName}</td>
                                <td className="text-capitalize"> {user.lastName}</td>
                                <td> {user.time}</td>
                                <td onChange={() => setTableValue('0')}> {user.dateOfBirth}</td>
                                <td><ReactTimeAgo date={user.localDateTime} locale="en-US" /></td>

                                <td>
                                    <Button value={user.patientId} name={user.patientId} 
                                    onClick={handleEditButton} variant='secondary'>
                                        <Icon name='edit' />
                                        Edit
                                    </Button>
                                </td>
                                <td>
                                    <Button value={user.patientId} name={user.patientId} 
                                    onClick={handleDeleteButton} variant='danger'>
                                        <DeleteForeverIcon />
                                        Delete
                                    </Button>
                                </td>

                            </tr>

                        })}

                    </tbody>
                </Table>

            </Row> : null}

            {patients.length !== 0 ? <Row className='pb-5'>
                <TablePagination
                    className="bg-dark text-light"
                    component="table"
                    count={100}
                    page={page}
                    onChangePage={handleChangePage}
                    rowsPerPage={rowsPerPage}
                    onChangeRowsPerPage={handleChangeRowsPerPage}
                />
            </Row> : null}

        </div>

        <Modal show={show} size={logoutClicked && "sm"} className='bg-dark' 
        onHide={handleClose} centered={logoutClicked}>
            <Modal.Header closeButton>
                {logoutClicked ? <Modal.Title>Log out</Modal.Title>
                    : <Modal.Title>Delete a patient</Modal.Title>}
            </Modal.Header>
            {logoutClicked ? <Modal.Body>Are you sure you want to log out ?</Modal.Body>
                : <Modal.Body>Are you sure, you want to delete this patient ?</Modal.Body>}
            <Modal.Footer>
                {logoutClicked ? <Button variant="primary" onClick={handleClose}>
                    Cancel
                    </Button>
                    : <Button variant="secondary" onClick={handleClose}>
                        No
                    </Button>}
                {logoutClicked ? <Button variant="danger" onClick={handleLogout}>
                    Log out
                    </Button>
                    : <Button variant="danger" onClick={handleYesDelete}>
                        yes, delete
                        {loading && <Spinner animation="border" size="sm" variant="light" />}
                    </Button>}
            </Modal.Footer>
        </Modal>


    </div>

}

export default OfficeHome