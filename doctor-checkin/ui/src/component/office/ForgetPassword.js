import React,{ useState } from 'react'
import Navigation from '../home/Navigation'
import { Form,Button,Alert,Spinner,Container,InputGroup,FormControl } from 'react-bootstrap'
import { Icon } from 'semantic-ui-react';
import EmailApi from '../api/EmailApi';
import { useHistory } from 'react-router-dom';
import CONSTANT from '../utils/CONSTANT';

const ForgetPassword = () => {

    const [email,setEmail] = useState();
    const [loading,setLoading] = useState(false);
    const [alert,setAlert] = useState(false);
    const history = useHistory();

    
    const handleVerifyEmail = (event) => {
        event.preventDefault();
        setLoading(true);
     
       EmailApi.verifyEmailAndCreateToken(email).then( token =>{
           if(token){
            localStorage.setItem(CONSTANT.LOCAL_STORAGE.PASSCHANGED, JSON.stringify(false));
            return history.push("/verifytoken/" + token.tokenId + "/" + token.officeId)
           } else if(!token){
               setAlert(true)
             return setLoading(false)
           }
       })
    }




    return <div>
        <Navigation />
        <Container>
            <div className="row mt-5">
                <div className="col-lg-5 ml-auto mr-auto">
                    <p>Verify email address</p>
                    <Form onSubmit={handleVerifyEmail}>

                        <InputGroup className="mb-3">
                            <FormControl
                                type="email"
                                required
                                placeholder="Enter email address"
                                aria-label="Recipient's username"
                                aria-describedby="basic-addon2"
                                onChange={(e) => setEmail(e.target.value.toLowerCase())}
                            />
                            <InputGroup.Append>
                                <Button type="submit" variant="outline-primary">
                                <Icon name='arrow right' />
                                Verify
                                {loading && <Spinner animation='border' size='sm' variant='light' />}
                                </Button>
                            </InputGroup.Append>
                        </InputGroup>
                        {alert && <Alert className="text-danger" dismissible onClose={() => setAlert(false)}>
                            Email doesn't exist in our record's.
                            </Alert>}

                    </Form>
                </div>

            </div>

        </Container>

    </div>

}

export default ForgetPassword