import React,{ useState } from 'react'
import { Form,Button,Alert,Spinner } from 'react-bootstrap'
import { Link,useHistory } from 'react-router-dom';
import Navigation from '../home/Navigation';
import OfficeApi from '../api/OfficeApi';
import CONSTANT from '../utils/CONSTANT';
import { Icon } from 'semantic-ui-react';


const OfficeSighUp = () => {
    const [email,setEmail] = useState();
    const [password,setPassword] = useState();
    const [confirmPassword,setConfirmPassword] = useState();
    const [alert,setAlert] = useState(false);
    const [loading,setLoading] = useState(false);
    const [error,setError] = useState('');
    const history = useHistory();
    const [currentType, setCurrentType] = useState('password');


    const handleCreateOfficeProfile = () => {

        if (password === confirmPassword) {
            OfficeApi.createOfficeProfile(email,password,confirmPassword).then(profile => {
                localStorage.setItem(CONSTANT.LOCAL_STORAGE.LOGIN,JSON.stringify(true))
                localStorage.setItem(CONSTANT.LOCAL_STORAGE.LOGOUT,JSON.stringify(false))
                localStorage.setItem(CONSTANT.LOCAL_STORAGE.OFFICE, JSON.stringify(profile))
                if (profile) {
                    setAlert(false);
                    setLoading(false);
                    history.push("/officehome/" + profile.officeId);
                }
                else if (!profile) {
                    setError(false);
                    setAlert(true);
                    return setLoading(false);
                } 

            })
        } 
        else if (password !== confirmPassword) {
            setError("password didn't match...!")
            setLoading(false);
            return setAlert(false);
        } 
        
       
    }

    const handleSubmitButton = (event) => {
        event.preventDefault();
        setLoading(true);
        setTimeout(handleCreateOfficeProfile,1000)
    }


    const handleShowPassword = () => {
        if(currentType === 'password'){
            setCurrentType('text');
        } else {
            setCurrentType("password");
        }
    }

    return  <div >
            <Navigation />
            <div className="row">
                <div className="col-lg-4 ml-auto mr-auto rounded mt-5">
                    <h2 className=" p-2">Registation (office's use only)</h2>

                    <Form onSubmit={handleSubmitButton} className="text-left">
                        <Form.Label>Email</Form.Label>
                        <Form.Control type="email" placeholder="Enter businsess email" required className="mb-3"
                            onChange={(e) => setEmail(e.target.value.toLowerCase())} />
                              <Form.Label>Password</Form.Label>
                        <Form.Control type={currentType && currentType} placeholder="Password" required className="mb-3" maxLength={12}
                            onChange={(e) => setPassword(e.target.value)} />
                              <Form.Label>Confirm password</Form.Label>
                        <Form.Control type={currentType && currentType} placeholder="Confirm-Password" required className="mb-4" maxLength={12}
                            onChange={(e) => setConfirmPassword(e.target.value)} />
           <Form.Group controlId="formBasicCheckbox">
                            <Form.Check type="checkbox" label="Show password" onClick={handleShowPassword} />
                        </Form.Group>
                        {alert && <Alert variant="danger" onClose={() => setAlert(false)} dismissible>
                            <p>Already have account. Can not create account.</p>
                        </Alert>}
                        {error && <p className='text-danger bg-dark p-2' >{error}</p>}

                        <Button type="submit" className="col-lg mb-5" style={{ backgroundColor: 'purple' }}>
                            <Icon name='arrow right' />
                         Submit

                    {loading && <Spinner animation="border" size='sm' variant='light' placeholder='loading' />}

                        </Button>
                        <p className="text-center">Already have an account? <Link className="text-warning" to="/officelogin"> Login </Link></p>
                    </Form>

                </div>

            </div>

        </div>
    
}
export default OfficeSighUp