import React,{ useEffect,useState } from 'react'
import Navigation from '../home/Navigation'
import { useParams } from 'react-router-dom'
import OfficeApi from '../api/OfficeApi';
import picture from '../image/find-a-doctor-abroad-1920x1080.jpeg'
import { Row,Col,Container,Image,Button,Modal,Form,Spinner,InputGroup,FormControl, Alert } from 'react-bootstrap';
import UploadImage from '../api/UploadImage';
import CONSTANT from '../utils/CONSTANT';
import { Icon } from 'semantic-ui-react';
import { SRLWrapper } from "simple-react-lightbox";


const OfficeProfile = () => {

    const { officeId } = useParams();
    const [office,setOffice] = useState();
    const [changeEmail,setChangeEmail] = useState(false);
    const [email,setEmail] = useState();
    const [existingEmail, setExistingEmail] = useState();
    const [loading,setLoading] = useState(false);
    const [show,setShow] = useState(false);
    const [file,setFile] = useState(false);
    const [newFile,setNewFile] = useState(false);
    const [isExistingEmail,setIsExistingEmail] = useState(false);



    useEffect(() => {
        OfficeApi.getOfficeProfile(officeId).then(office => {
            setOffice(office)
            setExistingEmail(office.email)
        })
        UploadImage.getFileByOfficeId(officeId).then(file => {
            setNewFile(file);
        })
    },[officeId])

    const handleUpdateProfile = () => {
        OfficeApi.updateProfile(officeId, email).then(office => {
            if(office){
            setOffice(office)
            setLoading(false);
            setIsExistingEmail(false);
             setEmail(office.email)       
           return setChangeEmail(false);
            }
        })
    }

    const handleUpdate = (event) => {
        event.preventDefault();
        
        if(existingEmail !== email){
        setLoading(true)
        setIsExistingEmail(false);
         return setTimeout(handleUpdateProfile,1000);
        } else {
        return setIsExistingEmail(true);
        }

    }

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);


    const handleUploadImage = (event) => {
        event.preventDefault();
        const formData = new FormData();
        formData.append("file",file);
        UploadImage.createFile(formData,officeId).then(uploadedFile => {
            if (uploadedFile) {
                UploadImage.getFileByOfficeId(uploadedFile.officeId).then(file => {
                    setNewFile(file);
                    setShow(false);
                })
            }
        })
    }

    const handleChangeEmailButton = () => {
        if (changeEmail === false) {
            setChangeEmail(true)
        } else {
            setChangeEmail(false);
        }
    }

    const handleEmail=(event)=>{
      setEmail(event.target.value);
      setIsExistingEmail(false);
    }

    return <div>
        <Navigation />


        {office ? <Container>

            <Row className="mt-5">
                <Col xs={6} md={4} className="ml-auto mr-auto">
                    <SRLWrapper>
                    {newFile ? <Image style={{cursor:'pointer'}} src={CONSTANT.URL.UPLOAD + "/office/" + officeId} thumbnail />
                        : <Image style={{cursor:'pointer'}} src={picture} thumbnail />}
                        </SRLWrapper>
                    <hr />
                    <Button onClick={handleShow}>Change photo</Button>

                </Col>
            </Row>

            <hr />
            <Row>
                <Col xs={6} className="ml-auto mr-auto">
                    <p>Email: {office.email} </p>
                    <Button variant='info' onClick={handleChangeEmailButton}>
                        <Icon name="edit outline" color="yellow" />
                        Change email
                    </Button>
                    {changeEmail && <Form onSubmit={handleUpdate}>
                        <InputGroup className="mb-3 mt-4">
                            <FormControl
                                type="email"
                                placeholder="Enter new email address"
                                aria-label="Recipient's username"
                                aria-describedby="basic-addon2"
                                required
                                onChange={handleEmail}
                            />
                            <InputGroup.Append>
                                <Button type="submit" variant="outline-warning">
                                    Submit
                                    {loading && <Spinner animation="border" size="sm" variant="primary" /> }
                                </Button>
                            </InputGroup.Append>
                        </InputGroup>
                        {isExistingEmail && <Alert className="text-danger" dismissible onClose={() => setIsExistingEmail(false)}>
                           This Email already exist...! Enter a new Email.
                            </Alert>}
                    </Form>}
                </Col>
            </Row>

        </Container> : null}


        <Modal show={show} className="bg-dark" onHide={handleClose}>
            <Modal.Header closeButton>
                <Modal.Title>Upload image</Modal.Title>
            </Modal.Header>
            <Modal.Body>Do you want to upload an image ?</Modal.Body>
            <Modal.Footer>
                <Form onSubmit={handleUploadImage}>
                    <input
                        type="file"
                        required
                        accept="image/*"
                        multiple={false}
                        id="upload-button"
                        onChange={(e) => setFile(e.target.files[0])}
                    />

                    <Button type="submit" color='instagram'>submit
                    {loading && <Spinner animation="border" size="sm" variant="white" />}
                    </Button>
                </Form>
            </Modal.Footer>

        </Modal>

    </div>

}

export default OfficeProfile