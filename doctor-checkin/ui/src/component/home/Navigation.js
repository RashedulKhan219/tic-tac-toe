import React,{ useState,useEffect } from 'react'
import { Navbar,Nav,Form,FormControl,Button, Alert } from 'react-bootstrap'
import { Link,useHistory } from 'react-router-dom'
import CONSTANT from '../utils/CONSTANT';
import PatientApi from '../api/PatientApi';

const Navigation = () => {

    const [patientId,setpatientId] = useState();
    const history = useHistory();

    const [message,setMessage] = useState();
    const [office,setOffice] = useState();


    useEffect(() => {
        const localOffice = localStorage.getItem(CONSTANT.LOCAL_STORAGE.OFFICE)
        if (localOffice) {
            setOffice(JSON.parse(localOffice))
        }
    },[])
    const handleSearch = (event) => {
        event.preventDefault();
        event.target.reset();
        PatientApi.getPatientById(patientId).then(patient => {
            if (patient) {
                history.push("/patientresponse/" + patient.patientId)
            } else if (!patient) {
                setMessage("Invalid patient-ID")
            }

        })


    }



    return <div>
        <Navbar fluid="true" collapseonselect={"true"} expand="lg" variant="dark" className="p-5 text-light App-check-in">
            <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" fill="currentColor" className="bi bi-person-check text-light mr-2" viewBox="0 0 16 16">
                <path d="M6 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm2-3a2 2 0 1 1-4 0 2 2 0 0 1 4 0zm4 8c0 1-1 1-1 1H1s-1 0-1-1 1-4 6-4 6 3 6 4zm-1-.004c-.001-.246-.154-.986-.832-1.664C9.516 10.68 8.289 10 6 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664h10z" />
                <path fillRule="evenodd" d="M15.854 5.146a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708 0l-1.5-1.5a.5.5 0 0 1 .708-.708L12.5 7.793l2.646-2.647a.5.5 0 0 1 .708 0z" />
            </svg>
            <i className="bi bi-person-check"></i>
            <Link to="/" className="mr-auto text-light">
                <h2>CHECK-IN</h2>
            </Link>

            <div>
                <p className="text-right">Call us @ 1-(718)-874-0076</p>
                <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                <Navbar.Collapse id="responsive-navbar-nav" >
                    <Nav collapseonselect={"true"}>
                        <Link to="/" className="nav-link text-white rounded App-nav">Home</Link>
                        <Link to="/checkin" className="nav-link text-white rounded App-nav">Check-in</Link>
                        <Link to="/patientlist" className="nav-link text-white rounded App-nav">Patient-list</Link>
                        {!office && <Link to="/officesignup" className="nav-link text-white rounded App-nav">SignUp</Link>}
                        {!office && <Link to="/officelogin" className="nav-link text-white rounded App-nav mr-3">Login</Link>}
                        {office && <Link to={"/officehome/" + office.officeId} className="nav-link text-white rounded App-nav mr-3">Officehome</Link> }
                    </Nav>

                    <Form inline onSubmit={handleSearch}>

                        <FormControl required type="text" placeholder="Enter patient-ID" className="mr-sm-2"
                            onChange={(e) => setpatientId(e.target.value)} />
                        <Button type="submit" variant="outline-primary">Search</Button>
                        
                    </Form>
                  
                </Navbar.Collapse>
                
            </div>

        </Navbar>
        {message &&   <Alert className="text-warning text-right" dismissible onClose={() => setMessage(null)}>  { message}</Alert>}
      
    </div>

}

export default Navigation