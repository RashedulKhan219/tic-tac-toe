import React from 'react'
import { Col,Container,Row } from 'react-bootstrap'
import { Button,Icon } from 'semantic-ui-react'

const Footer = () => {


    return <footer className="bg-dark text-light p-5 absolute">

        <div className="main-footer">
            <Container>

                <Row className=" mt-4">

                    <Col xl={4} md={4} xs={6}>
                        <h4>Location</h4>
                        <ul className="list-unstyled">
                            <li>169-85 Street, Jamaica</li>
                            <li>New York, NY</li>
                        </ul>

                    </Col>


                    <Col xl={4} md={4} xs={6}>
                        <h4>Contact us</h4>
                        <ul className="list-unstyled">

                            <p>
                                <Icon name='mail' />
                                    doctorsoffice@gmail.com </p>
                            <p>
                                <Icon name='phone volume' size='large' />
                                    1-718 874-0076</p>
                        </ul>

                    </Col>
                    <Col xl={4} md={4} xs={6}>
                        <h4>Follow us on</h4>
                        <Button className="mr-4" href='https://www.facebook.com/' circular size='large' color='facebook' icon='facebook' />
                        <Button className="mr-4" href='https://www.youtube.com/' circular color='youtube' icon='youtube' />
                        <Button className="mr-4" href='https://www.instagram.com/' circular color='instagram' icon='instagram' />
                    </Col>


                </Row>

                <Row className="mt-4 mb-4">
                    <Col xl={4} md={4} xs={6} className=" ml-auto mr-auto">
                        <div className="footer-bottom">
                            <p className="text-xs-center"></p>
                    &copy;Copyright {new Date().getFullYear()} - All rights reserved by Doctor's Office

                    </div>
                    </Col>
                </Row>
            </Container>

        </div>


    </footer>

}

export default Footer