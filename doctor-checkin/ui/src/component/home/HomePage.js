import React,{ useState } from 'react'
import Navigation from './Navigation'
import { Button, Icon } from 'semantic-ui-react'
import { useHistory } from 'react-router-dom'
import { Form,FormControl,Alert,Spinner, Row, Col, InputGroup } from 'react-bootstrap'
import moment from "moment";
import PatientApi from '../api/PatientApi'





const HomePage = () => {
    const history = useHistory()
    const [dateOfBirth,setDateOfBirth] = useState();
    const [patients,setpatients] = useState([]);
    const [alert,setAlert] = useState(false);
    const [loading,setLoading] = useState(false);
    const [time,setTime] = useState(moment(new Date()).format("hh:mm:ss a"));
    const [error,setError] = useState(false);
   



    const handleFindUser = () => {
        PatientApi.getPatientByDateOfBirth(dateOfBirth).then(patients => {
            setpatients(patients)
            setAlert(false);
            setLoading(false);
        })
        if (patients.length === 0) {
            setLoading(false);
            setError(true);
        }
    }

    const handleFindButton = (event) => {
        event.preventDefault();
        setLoading(true);
        setTimeout(handleFindUser,1000);
        event.target.reset();
    }

    const handleCheckInButton = () => {
        history.push("/checkin")
    }

    const handleChange =(e)=>{
            setDateOfBirth(moment(e.target.value).format("MM-DD-YYYY"))
    }


    function tick() {
        setTime(moment(new Date()).format("hh:mm:ss a"));
    }

    setInterval(tick,1000);

    
    
  
    

    return <div >
            <Navigation />

            <div className="App-section-1">
                <div className="text-right">
                <Icon name='calendar alternate outline' />
                    {moment(new Date()).format("dddd, MMMM Do YYYY")}
                    <Icon name='clock outline' />
                    {time}
                </div>
                

                <h2>
                    <Icon name="stethoscope" size='big' color='orange' />
                    Welcome to Doctor's office
                </h2>
                <p>Please check in before you visit to a doctor</p>
                <div className="col-lg-4 col-md-4 col-sm-4 ml-auto mr-auto shadow  App-check-in">
                    <Button color='purple' onClick={handleCheckInButton}>
                        <Icon name="check circle" />
                        CHECK IN HERE
                    </Button>
                    
                </div>

        

            </div>


            <div className="App-section-2">

            <Row>
                <Col sm={4} className="ml-auto mr-auto">
                
                    <Form.Label >See if you already checked in enter date of birth</Form.Label>
                    <Form  onSubmit={handleFindButton}>
                        <InputGroup className="mb-3">
                            <FormControl type="date" required placeholder="Enter appointment-ID" className="rounded mr-sm-2"
                                onChange={handleChange} />
                            <InputGroup.Append>
                                <Button type="submit" className="rounded" color='facebook'>
                                    Find
                            {loading && <Spinner animation="border" size="sm" variant="white" />}
                                </Button>
                            </InputGroup.Append>
                        </InputGroup>

                        {error && patients.length === 0 && <Alert variant="danger" dismissible onClose={() => setError(false)}>
                            Sorry....! Patient doesn't exist in our record's.
                          </Alert>}
                    </Form>

                    {patients.length !== 0 && !alert ? <div >
                        {patients.map((patients,index) => (
                            <Alert key={index} className='mt-4 bg-dark text-light rounded p-3 text-left' dismissible onClose={() => setAlert(true)}>
                                <p>Name: {patients.firstName + " " + patients.lastName}</p>
                                <p>Arrival time: {patients.time}</p>
                                <p>DOB: {patients.dateOfBirth}</p>
                                <div className="text-right">
                                <Button  size='small' variant="secondary"  onClick={() => setAlert(true)}>Close</Button>
                                </div>
                               
                            </Alert>

                        ))}
                    </div> : null}
                </Col>
            </Row>


         

                <div className="col-lg-10 col-md-6 col-sm-3 ml-auto mr-auto pb-5">
                    <h1>Primary care that puts your needs first.</h1>
                    <p>The way we care for our patients is to treat the whole person.
                    That means we handle everything from medical, social, and even behavioral
                    health in a safe and clean doctors office environment. Our primary care model
                    connects patients to the right programs and resources for them. Making sure no
                    one falls through the cracks. Our doctors are well-versed in internal medicine,
                    geriatric medicine, and even family medicine. With Internists and Geriatricians
                     on staff to assist those specific needs.</p>
                </div>

                <div className="col-lg-10 col-md-6 col-sm-3 ml-auto mr-auto">
                    <h2>Patients are not outsiders to our business. They are our business.</h2>
                    <p>
                        Creating and maintaining a patient-centered practice is not a new concept. Over 90 years ago,
                        the creed of Cleveland Clinic founder Dr. William Lower provided exceptional insight, recognizing
                        that “a patient is the most important person in the institution,” and they “are not dependent on us,
                        we are dependent on them,” and ultimately, “it is our job to satisfy them.”
                        Top-tier practitioners understand that “patient centered” has contemporary marketing interpretations…
                        Having a deep understanding of the needs and wants of the target audience and providing timely appropriate answers to those needs;
                        Recognizing patients as informed consumers who actively participate in their health care; and
                        Understanding that a connected relationship is an integral part of patient experience and satisfaction.
                </p>
                </div>


               

            </div>
        

           

        </div>
    
}

export default HomePage