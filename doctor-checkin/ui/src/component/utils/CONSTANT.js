const BASE_URL = '/api';

export default {

    URL:{
        BASE: BASE_URL,
        PATIENT: BASE_URL + '/patient',
        OFFICE: BASE_URL + '/office',
        EMAIL: BASE_URL + '/email',
        UPLOAD: BASE_URL + '/uploadFile'
    },
    LOCAL_STORAGE: {
        OFFICE:'OFFICE',
        LOGIN:'LOGIN',
        LOGOUT:'LOGOUT',
        PASSCHANGED:'PASSCHAGED'
    }
}