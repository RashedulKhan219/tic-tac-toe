package it.roti.io.api.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.apache.commons.lang3.RandomStringUtils;

@Data
@AllArgsConstructor
public class EmailToken {
    private String tokenId;
    private String toEmail;
    private String body;
    private String subject;
    private String officeId;

    public EmailToken() {
        tokenId = RandomStringUtils.randomNumeric(6);
        body = "Please enter the verification code to update your account";
        subject = "Verification/activation code-";
    }
}
