package it.roti.io.api.api;


import it.roti.io.api.model.EmailToken;
import it.roti.io.api.response.EmailTokenResponse;
import it.roti.io.api.service.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping("/api/email")
public class EmailApi {

    @Autowired
    EmailService emailService;

    // verify token
    @GetMapping("/{emailTokenId}")
    public EmailTokenResponse verifyToken(@PathVariable String emailTokenId) {
        return emailService.verifyToken(emailTokenId);
    }

    // verifies user email and create an email token
    @PostMapping("/{email}")
    public EmailTokenResponse verifyEmailAndCreateToken(@PathVariable String email) {
        return emailService.verifyEmailAndCreateToken(email);
    }


}