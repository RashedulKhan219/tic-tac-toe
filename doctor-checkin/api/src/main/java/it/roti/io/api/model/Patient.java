package it.roti.io.api.model;

import lombok.Data;
import org.apache.commons.lang3.RandomStringUtils;

import java.time.LocalDateTime;

@Data
public class Patient {
   private String patientId;
   private String firstName;
   private String lastName;
   private String dateOfBirth;
   private String time;
   private LocalDateTime localDateTime;

    public Patient() {
        patientId = RandomStringUtils.randomNumeric(5);
        localDateTime = LocalDateTime.now();
    }
}
