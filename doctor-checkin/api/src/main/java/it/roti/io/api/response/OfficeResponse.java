package it.roti.io.api.response;

import it.roti.io.api.model.Office;
import lombok.Data;

@Data
public class OfficeResponse {
   private String officeId;
   private String email;
   private String password;
   private String confirmPass;
   private String tokenId;

    public OfficeResponse(Office office) {
        officeId = office.getOfficeId();
        email = office.getEmail();
        password = office.getPassword();
        confirmPass = office.getConfirmPass();
        tokenId = office.getTokenId();
    }
}
