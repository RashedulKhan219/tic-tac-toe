package it.roti.io.api.service;

import it.roti.io.api.model.UploadFile;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class UploadFileService {

    Map<String, UploadFile> fileUploadMap = new HashMap<>();

    public UploadFile createFile(MultipartFile file, String officeId) throws IOException {
        // if file exist then update file other wise create a new file
        if (fileUploadMap.containsKey(officeId)) {
            return updateFile(officeId, file);
        } else {
                UploadFile uploadFile = new UploadFile();
                uploadFile.setFileData(file.getBytes());
                uploadFile.setFileName(file.getOriginalFilename());
                uploadFile.setFileType(file.getContentType());
                uploadFile.setOfficeId(officeId);
                fileUploadMap.put(officeId, uploadFile);
            return uploadFile;
        }
    }

    public List<UploadFile> getAllFile() {
        return new ArrayList<>(fileUploadMap.values());
    }

    public ResponseEntity<Resource> getFileByFileId(String fileId) {
        if (fileUploadMap.containsKey(fileId)) {
            UploadFile uploadFile = fileUploadMap.get(fileId);
            return ResponseEntity.ok()
                    .contentType(MediaType.parseMediaType(uploadFile.getFileType()))
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + uploadFile.getFileName() + "\"")
                    .body((new ByteArrayResource(uploadFile.getFileData())));
        }
        return null;

    }

    public ResponseEntity<Resource> getFileByOfficeId(String officeId) {
        if (fileUploadMap.containsKey(officeId)) {
            UploadFile uploadFile = fileUploadMap.get(officeId);
            return ResponseEntity.ok()
                    .contentType(MediaType.parseMediaType(uploadFile.getFileType()))
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + uploadFile.getFileName() + "\"")
                    .body((new ByteArrayResource(uploadFile.getFileData())));
        }
        return null;
    }


    public UploadFile updateFile(String officeId, MultipartFile file) throws IOException {
            UploadFile existingFile = fileUploadMap.get(officeId);
                existingFile.setFileData(file.getBytes());
                existingFile.setFileName(file.getOriginalFilename());
                existingFile.setFileType(file.getContentType());
                return existingFile;
    }

}

