package it.roti.io.api.response;

import it.roti.io.api.model.Patient;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
public class PatientResponse {
   private String patientId;
   private String firstName;
   private String lastName;
   private String dateOfBirth;
   private String time;
   private LocalDateTime localDateTime;

    public PatientResponse(Patient patient) {
        patientId = patient.getPatientId();
        firstName = patient.getFirstName();
        lastName = patient.getLastName();
        dateOfBirth = patient.getDateOfBirth();
        time = patient.getTime();
        localDateTime = patient.getLocalDateTime();
    }
}
