package it.roti.io.api.service;


import it.roti.io.api.model.EmailToken;
import it.roti.io.api.model.Office;
import it.roti.io.api.response.EmailTokenResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class EmailService {


    private final JavaMailSender mailSender;
    private final OfficeService officeService;

    @Autowired
    public EmailService(JavaMailSender mailSender, OfficeService officeService) {
        this.mailSender = mailSender;
        this.officeService = officeService;
    }

    Map<String, EmailToken> emailTokenMap = new HashMap<>();

    // verify email then create a token
    public EmailTokenResponse verifyEmailAndCreateToken(String email) {
        try {
            for (Office office : officeService.officeMap.values()) {
                if (office.getEmail().equals(email)) {
                    // create emailToken and set officeId into token
                    EmailToken emailToken = createToken(email);
                    emailToken.setOfficeId(office.getOfficeId());
                    // set token id into office
                    office.setTokenId(emailToken.getTokenId());
                    return new EmailTokenResponse(emailToken);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    // to create a token
    public EmailToken createToken(String toEmail) {
        EmailToken emailToken = new EmailToken();
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setFrom("amitahamed1993@gmail.com");
        mailMessage.setTo(toEmail);
        mailMessage.setText(emailToken.getBody() + "-" + "\n" + emailToken.getTokenId());
        mailMessage.setSubject(emailToken.getSubject() + "from doctor's office");
        mailSender.send(mailMessage);
        emailTokenMap.put(emailToken.getTokenId(), emailToken);
        return emailToken;
    }

    // validate tokenId
    public EmailTokenResponse verifyToken(String tokenId) {
        if (emailTokenMap.containsKey(tokenId)) {
            EmailToken emailToken = emailTokenMap.get(tokenId);
            // removing email token after verify
            emailTokenMap.remove(tokenId);
            return new EmailTokenResponse(emailToken);
        }
        return null;
    }


}
