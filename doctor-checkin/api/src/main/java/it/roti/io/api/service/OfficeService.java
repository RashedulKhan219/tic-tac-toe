package it.roti.io.api.service;


import it.roti.io.api.model.Office;
import it.roti.io.api.response.OfficeResponse;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class OfficeService {


    Map<String, Office> officeMap = new HashMap<>();

    // create profile
    public OfficeResponse createOfficeProfile(Office office) {
        if (!officeMap.isEmpty() || validateEmail(office.getEmail())) {
            return null;
        } else {
            officeMap.put(office.getOfficeId(), office);
            return new OfficeResponse(office);
        }
    }

    // validate email address
    public  boolean validateEmail(String email) {
        for (Office office1 : officeMap.values()) {
            if (office1.getEmail().equals(email)) {
                return true;
            }
        }
        return false;
    }

    // get profile by Id
    public OfficeResponse getOfficeProfileById(String officeId) {
        if (officeMap.containsKey(officeId)) {
            Office office = officeMap.get(officeId);
            return new OfficeResponse(office);
        }
        return null;
    }

    // log in to profile
    public OfficeResponse loginAtOffice(String email, String password) {
        for (Office office : officeMap.values()) {
            if (office.getEmail().equals(email) &&
                    office.getPassword().equals(password)) {
                return new OfficeResponse(office);
            }
        }
        return null;
    }

    // update office profile
    public OfficeResponse updateOfficeProfile(String officeId, Office office) {
        if(officeMap.containsKey(officeId)){
            Office office1 = officeMap.get(officeId);
            office1.setEmail(office.getEmail());
            return new OfficeResponse(office1);
        }
        return null;
    }

    // reset password
    public OfficeResponse resetPassword(String password, String officeId) {
        if (officeMap.containsKey(officeId)) {
            Office office = officeMap.get(officeId);
            office.setPassword(password);
            return new OfficeResponse(office);
        }
        return null;
    }
}
