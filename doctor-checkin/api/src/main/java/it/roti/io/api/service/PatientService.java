package it.roti.io.api.service;


import it.roti.io.api.model.Patient;
import it.roti.io.api.response.PatientResponse;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class PatientService {

    Map<String, Patient> patientMap = new HashMap<>();

    public PatientResponse createPatient(Patient patient) {
        patientMap.put(patient.getPatientId(), patient);
        return new PatientResponse(patient);
    }

    public List<PatientResponse> getAllPatient() {
        List<PatientResponse> userResponseList = new ArrayList<>();
        for (Patient patient : patientMap.values()) {
            userResponseList.add(new PatientResponse(patient));
        }
        userResponseList.sort(Comparator.comparing(PatientResponse::getLocalDateTime));
        return userResponseList;
    }

    public PatientResponse getPatientById(String patientId) {
        if (patientMap.containsKey(patientId)) {
            Patient patient = patientMap.get(patientId);
            return new PatientResponse(patient);
        }
        return null;
    }

    public List<PatientResponse> getPatientByDateOfBirth(String dateOfBirth) {
        List<PatientResponse> userResponseList = new ArrayList<>();
        for (Patient patient : patientMap.values()) {
            if (patient.getDateOfBirth().equals(dateOfBirth)) {
                userResponseList.add(new PatientResponse(patient));
            }
        }
        userResponseList.sort(Comparator.comparing(PatientResponse::getLocalDateTime));
        return userResponseList;
    }

    public PatientResponse updatePatient(String patientId, Patient userRequest) {
        Patient patient = patientMap.get(patientId);
        patient.setFirstName(userRequest.getFirstName());
        patient.setLastName(userRequest.getLastName());
        patient.setDateOfBirth(userRequest.getDateOfBirth());
        return new PatientResponse(patient);
    }

    public PatientResponse deletePatient(String patientId) {
        if (patientMap.containsKey(patientId)) {
            Patient patient = patientMap.get(patientId);
            patientMap.remove(patientId);
            return new PatientResponse(patient);
        }
        return null;
    }


}
