package it.roti.io.api.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.apache.commons.lang3.RandomStringUtils;

@Data
@AllArgsConstructor
public class Office {
   private String officeId;
   private String email;
   private String password;
   private String confirmPass;
   private String tokenId;

    public Office() {
        officeId = RandomStringUtils.randomNumeric(6);
    }
}
