package it.roti.io.api.api;

import it.roti.io.api.model.Office;
import it.roti.io.api.response.OfficeResponse;
import it.roti.io.api.service.OfficeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping("/api/office")
public class OfficeApi {

    @Autowired
    OfficeService officeService;

    @GetMapping("/{officeId}")
    public OfficeResponse getOfficeProfileById(@PathVariable String officeId) {
        return officeService.getOfficeProfileById(officeId);
    }

    @GetMapping("/{email}/{password}")
    public OfficeResponse loginAtOffice(@PathVariable String email, @PathVariable String password) {
        return officeService.loginAtOffice(email, password);
    }

    @PostMapping
    public OfficeResponse createOfficeProfile(@RequestBody Office office) {
        return officeService.createOfficeProfile(office);
    }

    @PutMapping("/{officeId}")
    public OfficeResponse updateOfficeProfile(@PathVariable String officeId, @RequestBody Office office) {
        return officeService.updateOfficeProfile(officeId, office);
    }

    @PutMapping("/{password}/{officeId}")
    public OfficeResponse resetPassword(@PathVariable String password, @PathVariable String officeId){
        return officeService.resetPassword(password, officeId);
    }
}
