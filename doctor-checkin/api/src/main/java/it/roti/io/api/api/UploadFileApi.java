package it.roti.io.api.api;

import it.roti.io.api.model.UploadFile;
import it.roti.io.api.service.UploadFileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/api/uploadFile")
public class UploadFileApi {
    @Autowired
    private UploadFileService uploadFileService;

    @GetMapping
    public List<UploadFile> getAllFile() {
        return uploadFileService.getAllFile();
    }

    @GetMapping("/{fileId}")
    public ResponseEntity<Resource> getFileByFileId(@PathVariable String fileId) {
        return uploadFileService.getFileByFileId(fileId);
    }

    @GetMapping("/office/{officeId}")
    public ResponseEntity<Resource> getFileByOfficeId(@PathVariable String officeId) {
        return uploadFileService.getFileByOfficeId(officeId);
    }

    @PostMapping("/{officeId}")
    public UploadFile createFile(@RequestParam("file") MultipartFile file, @PathVariable String officeId) throws IOException {
        return uploadFileService.createFile(file, officeId);
    }

    @PutMapping("/{fileId}")
    public UploadFile updateFile(@PathVariable String fileId, @RequestParam("file") MultipartFile file) throws IOException {
        return uploadFileService.updateFile(fileId, file);
    }

}

