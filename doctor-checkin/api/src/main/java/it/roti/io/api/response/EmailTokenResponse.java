package it.roti.io.api.response;

import it.roti.io.api.model.EmailToken;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class EmailTokenResponse {
    private String tokenId;
    private String toEmail;
    private String body;
    private String subject;
    private String officeId;

    public EmailTokenResponse(EmailToken emailToken) {
        tokenId = emailToken.getTokenId();
        toEmail = emailToken.getToEmail();
        body = emailToken.getBody();
        subject = emailToken.getSubject();
        officeId = emailToken.getOfficeId();
    }
}
