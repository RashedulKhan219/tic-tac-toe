package it.roti.io.api.api;


import it.roti.io.api.model.Patient;
import it.roti.io.api.response.PatientResponse;
import it.roti.io.api.service.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/api/patient")
public class PatientApi {

    @Autowired
    PatientService patientService;

    @GetMapping
    public List<PatientResponse> getAllPatient() {
        return patientService.getAllPatient();
    }

    @GetMapping("/{patientId}")
    public PatientResponse getPatientById(@PathVariable String patientId) {
        return patientService.getPatientById(patientId);
    }

    @GetMapping("/date/{dateOfBirth}")
    public List<PatientResponse> getPatientByDateOfBirth(@PathVariable String dateOfBirth) {
        return patientService.getPatientByDateOfBirth(dateOfBirth);
    }


    @PostMapping
    public PatientResponse createPatient(@RequestBody Patient patient) {
        return patientService.createPatient(patient);
    }

    @PutMapping("/{patientId}")
    public PatientResponse updatePatient(@PathVariable String patientId, @RequestBody Patient patientRequest) {
        return patientService.updatePatient(patientId, patientRequest);
    }

    @DeleteMapping("/{patientId}")
    public PatientResponse deletePatient(@PathVariable String patientId) {
        return patientService.deletePatient(patientId);
    }


}
