import axios from 'axios';
import CONSTANTS from '../utils/CONSTANTS';

class PlayApi {

static playGame(gameId, row, col, playerId){
    
    const url = CONSTANTS.URL.PLAY + '/' + gameId;
    const payload = { row: row, col: col, playerId: playerId}
    
    return axios.post(url, payload).then(response => response.data);
}

static rematch(gameId) {
    const url = CONSTANTS.URL.GAME + '/rematch/' + gameId;
    return axios.put(url).then(response => response.data);
}
    
  
}

export default PlayApi
