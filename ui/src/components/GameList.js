import React from 'react';
import { ListGroup } from 'react-bootstrap';

function GameList({games}) {

    let variant = 'primary';

    function getVariant(variant) {
        if (variant === 'primary')
            return 'danger';
        if (variant === 'danger')
            return 'primary';
    }

    return <div>
        All Games - {games.length}
        <ListGroup>
            {games.map(game => {
                variant = getVariant(variant);
                return <ListGroup.Item key={game.gameId} variant={variant}>{game.gameId} - {game.state}</ListGroup.Item>
            })}
        </ListGroup>
    </div>
}


export default GameList;