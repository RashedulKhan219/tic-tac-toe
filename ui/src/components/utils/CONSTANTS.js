//const BASE_URL = 'https://tic.roti.io';
 const BASE_URL = 'http://localhost:8080';

export default {
    URL: {
        BASE: BASE_URL,
        GAME : BASE_URL + '/game',
        PLAY: BASE_URL + '/play'
    },
    LOCAL_STORAGE: {
        PLAYER: 'PLAYER'
    }
}
