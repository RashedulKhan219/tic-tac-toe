git pull

echo --- Packaging JAR ---
mvn clean package -Dmaven.test.skip=true

echo --- Building image ---
docker build --no-cache -t student/bishawjit-appointment .

echo --- Stoppping old app ---
docker kill student-bishawjit-appointment
docker rm student-bishawjit-appointment

#echo --- Starting container ---
#docker run -p 6060:8080 -d --name student-bishawjit-appointment student/bishawjit-appointment

echo --- Done ---
