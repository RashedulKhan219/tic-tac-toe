package it.orti.io.api.api;

import it.orti.io.api.model.Barber;
import it.orti.io.api.response.BarberResponse;
import it.orti.io.api.service.BarberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/api/barber")
public class BarberApi {

    @Autowired
    BarberService barberService;

    @GetMapping
    public List<BarberResponse> getAllBarber() {
        return barberService.getAllBarber();
    }

    @GetMapping("/{barberId}")
    public ResponseEntity<BarberResponse> getBarber(@PathVariable String barberId) {
        BarberResponse response = barberService.getBarber(barberId);
        if (response == null) return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/{email}/{password}")
    public ResponseEntity<BarberResponse> login(@PathVariable String email, @PathVariable String password) {
        BarberResponse response = barberService.login(email, password);
        if (response == null) return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/email/{email}")
    public ResponseEntity<BarberResponse> validateEmailToGetToken(@PathVariable String email) {
        BarberResponse response = barberService.validateEmailToGetToken(email);
        if (response == null) return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<BarberResponse> createBarberProfile(@RequestBody Barber barber) {
        BarberResponse response = barberService.createBarberProfile(barber);
        if (response == null) return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PutMapping("/{barberId}")
    public ResponseEntity<BarberResponse> updateBarber(@PathVariable String barberId, @RequestBody Barber barberRequest) {
        BarberResponse response = barberService.updateBarber(barberId, barberRequest);
        if (response == null) return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PutMapping("/{password}/{barberId}")
    public ResponseEntity<BarberResponse> resetPassword(@PathVariable String password, @PathVariable String barberId) {
        BarberResponse response = barberService.resetPassword(password, barberId);
        if (response == null) return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @DeleteMapping("/{barberId}")
    public ResponseEntity<BarberResponse> deleteBarberProfile(@PathVariable String barberId) {
        BarberResponse response = barberService.deleteBarberProfile(barberId);
        if (response == null) return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

}
