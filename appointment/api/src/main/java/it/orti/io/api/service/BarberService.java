package it.orti.io.api.service;

import it.orti.io.api.model.Barber;
import it.orti.io.api.model.STATE;
import it.orti.io.api.response.BarberResponse;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class BarberService {


    Map<String, Barber> barberMap = new HashMap<>();

    // create barber profile
    public BarberResponse createBarberProfile(Barber barber) {
        if (validateEmail(barber.getEmail())) {
            return null;
        } else if (barber.getRole().equals(STATE.ADMIN) && !barberMap.containsKey(barber.getCurrentRole())) {
            barber.setBarberId(barber.getCurrentRole());
            barberMap.put(barber.getCurrentRole(), barber);
            return new BarberResponse(barber);
        } else if (!barber.getRole().equals(STATE.ADMIN)) {
            barberMap.put(barber.getBarberId(), barber);
            return new BarberResponse(barber);
        }
        return null;
    }


    // get all barber
    public List<BarberResponse> getAllBarber() {
        List<BarberResponse> barberList = new ArrayList<>();
        for (Barber barber : barberMap.values()) {
            barberList.add(new BarberResponse(barber));
        }
        return barberList;
    }

    // get a single barber
    public BarberResponse getBarber(String barberId) {
        if (barberMap.containsKey(barberId)) {
            return new BarberResponse(barberMap.get(barberId));
        }
        return null;
    }

    // login to account
    public BarberResponse login(String email, String password) {
        for (Barber barber : barberMap.values()) {
            if (barber.getEmail().equals(email) &&
                    barber.getPassword().equals(password)) {
                return new BarberResponse(barber);
            }
        }
        return null;
    }

    // update barber by Id
    public BarberResponse updateBarber(String barberId, Barber barberRequest) {
        if (barberMap.containsKey(barberId)) {
            Barber barber = barberMap.get(barberId);
            barber.setFirstName(barberRequest.getFirstName());
            barber.setLastName(barberRequest.getLastName());
            barber.setEmail(barberRequest.getEmail());
            barber.setPhone(barberRequest.getPhone());
            return new BarberResponse(barber);
        }
        return null;
    }

    // reset password
    public BarberResponse resetPassword(String password, String barberId) {
        if (barberMap.containsKey(barberId)) {
            Barber barber = barberMap.get(barberId);
            barber.setPassword(password);
            return new BarberResponse(barber);
        }
        return null;
    }

    // delete barber profile by id
    public BarberResponse deleteBarberProfile(String barberId) {
        if (barberMap.containsKey(barberId)) {
            Barber barber = barberMap.get(barberId);
            barberMap.remove(barberId);
            return new BarberResponse(barber);
        }
        return null;
    }

    // check if email already exist
    public boolean validateEmail(String email) {
        for (Barber barber : barberMap.values()) {
            if (barber.getEmail().equals(email)) {
                return true;
            }
        }
        return false;
    }

    // validate email to get token
    public BarberResponse validateEmailToGetToken(String email) {
        for (Barber barber : barberMap.values()) {
            if (barber.getEmail().equals(email)) {
                return new BarberResponse(barber);
            }
        }
        return null;
    }

}
