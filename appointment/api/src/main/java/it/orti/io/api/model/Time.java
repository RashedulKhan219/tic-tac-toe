package it.orti.io.api.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.apache.commons.lang3.RandomStringUtils;

@Data
@AllArgsConstructor

public class Time {
    private String timeId;
    private String time;
    private String date;
    private String barberId;
    private String barberName;

    public Time(TimeRequest timeRequest) {
        time = timeRequest.getTime();
        date = timeRequest.getDate();
        barberId = timeRequest.getBarberId();
        barberName = timeRequest.getBarberName();
        timeId = RandomStringUtils.randomNumeric(5);
    }
}
