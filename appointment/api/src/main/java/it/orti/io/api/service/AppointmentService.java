package it.orti.io.api.service;

import it.orti.io.api.model.Appointment;
import it.orti.io.api.model.AppointmentRequest;
import it.orti.io.api.response.AppointmentResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class AppointmentService {


    UserService userService;

    @Autowired
    public AppointmentService(UserService userService) {
        this.userService = userService;
    }

    Map<String, Appointment> appointmentMap = new HashMap<>();

    // create appointment
    public AppointmentResponse createAppointment(String userId, AppointmentRequest appointmentRequest) {
        if (appointmentMap.containsKey(userId)) {
            return null;
        }
        Appointment appointment = new Appointment(userId, appointmentRequest);
        appointment.setUserId(userId);
        appointment.setTimeId(appointmentRequest.getTimeId());
        appointmentMap.put(appointment.getAppointmentId(), appointment);
        return new AppointmentResponse(appointment);
    }

    // get all appointment
    public List<AppointmentResponse> getAllAppointment() {
        List<AppointmentResponse> appointmentResponseList = new ArrayList<>();
        for (Appointment appointment : appointmentMap.values()) {
            appointmentResponseList.add(new AppointmentResponse(appointment));
        }
        appointmentResponseList.sort(Comparator.comparing(AppointmentResponse::getLocalDateTime));
        return appointmentResponseList;
    }

    // get a single appointment
    public AppointmentResponse getAppointmentById(String appointmentId) {
        if (appointmentMap.containsKey(appointmentId)) {
            Appointment appointment = appointmentMap.get(appointmentId);
            return new AppointmentResponse(appointment);
        }
        return null;
    }

    // get appointment by userId
    public AppointmentResponse getAppointmentByUserId(String userId) {
        for (Appointment appointment : appointmentMap.values()) {
            if (appointment.getUserId().equals(userId)) {
                return new AppointmentResponse(appointment);
            }
        }
        return null;
    }

    // get appointments by barberId
    public List<AppointmentResponse> getAppointmentsByBarberId(String barberId) {
        List<AppointmentResponse> appointmentResponseList = new ArrayList<>();
        for (Appointment appointment : appointmentMap.values()) {
            if (appointment.getBarberId().equals(barberId)) {
                appointmentResponseList.add(new AppointmentResponse(appointment));
            }
        }
        appointmentResponseList.sort(Comparator.comparing(AppointmentResponse::getLocalDateTime));
        return appointmentResponseList;
    }

    // get appointment by email
    public AppointmentResponse getAppointmentByEmail(String email) {

        for (Appointment appointment : appointmentMap.values()) {
            if (appointment.getEmail().equals(email)) {
                return new AppointmentResponse(appointment);
            }
        }
        return null;
    }

    // update appointment
    public AppointmentResponse updateAppointment(String appointmentId, AppointmentRequest appointmentRequest) {
        if (appointmentMap.containsKey(appointmentId)) {
            Appointment appointment = appointmentMap.get(appointmentId);
            appointment.setDate(appointmentRequest.getDate());
            appointment.setTime(appointmentRequest.getTime());
            appointment.setTimeId(appointmentRequest.getTimeId());
            appointment.setGender(appointmentRequest.getGender());
            appointment.setBarberName(appointmentRequest.getBarberName());
            appointment.setBarberId(appointmentRequest.getBarberId());
            return new AppointmentResponse(appointment);
        }
        return null;
    }

    // delete appointment
    public AppointmentResponse deleteAppointment(String appointmentId) {
        if (appointmentMap.containsKey(appointmentId)) {
            Appointment appointment = appointmentMap.get(appointmentId);
            List<Appointment> appointmentList = userService.userMap.get(appointment.getUserId()).getAppointmentList();
            appointmentList.add(appointment);
            appointmentMap.remove(appointmentId);
            return new AppointmentResponse(appointment);
        }
        return null;
    }


}
