package it.orti.io.api.model;

import lombok.Data;

@Data
public class TimeRequest {
    private String time;
    private String date;
    private String barberId;
    private String barberName;
}
