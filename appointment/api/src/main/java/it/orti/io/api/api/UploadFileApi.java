package it.orti.io.api.api;


import it.orti.io.api.model.UploadFile;
import it.orti.io.api.service.UploadFileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/api/uploadFile")
public class UploadFileApi {
    @Autowired
    private UploadFileService uploadFileService;

    @GetMapping
    public List<UploadFile> getAllFile() {
        return uploadFileService.getAllFile();
    }

    @GetMapping("/{fileId}")
    public ResponseEntity<Resource> getFileById(@PathVariable String fileId) {
        return uploadFileService.getFileById(fileId);
    }

    @GetMapping("/user/{userId}")
    public ResponseEntity<Resource> getFileByUserId(@PathVariable String userId) {
        return uploadFileService.getFileByUserId(userId);
    }

    @PostMapping("/{userId}")
    public UploadFile createFile(@RequestParam("file") MultipartFile file, @PathVariable String userId) {
        return uploadFileService.createFile(file, userId);
    }

    @PutMapping("/{fileId}")
    public UploadFile updateFile(@PathVariable String fileId, @RequestParam("file") MultipartFile file) {
        return uploadFileService.updateFile(fileId, file);
    }

}
