package it.orti.io.api.model;

import lombok.Data;
import org.apache.commons.lang3.RandomStringUtils;

@Data
public class Barber {
    private String barberId;
    private String firstName;
    private String lastName;
    private String phone;
    private String email;
    private String password;
    private String fileId;
    private String tokenId;
    private STATE role;
    private String currentRole;

    public Barber() {
        barberId = RandomStringUtils.randomNumeric(5);
        role = STATE.BARBER;
    }
}
