package it.orti.io.api.response;


import it.orti.io.api.model.Appointment;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
public class AppointmentResponse {
    private String appointmentId;
    private String date;
    private String time;
    private String timeId;
    private String gender;
    private String barberName;
    private String barberId;
    private String userId;
    private String email;
    private String services;
    private LocalDateTime localDateTime;

    public AppointmentResponse(Appointment appointment) {
        appointmentId = appointment.getAppointmentId();
        date = appointment.getDate();
        time = appointment.getTime();
        timeId = appointment.getTimeId();
        gender = appointment.getGender();
        barberName = appointment.getBarberName();
        barberId = appointment.getBarberId();
        userId = appointment.getUserId();
        email = appointment.getEmail();
        services = appointment.getServices();
        localDateTime = appointment.getLocalDateTime();
    }

}
