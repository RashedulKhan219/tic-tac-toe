package it.orti.io.api.api;

import it.orti.io.api.model.Appointment;
import it.orti.io.api.model.User;
import it.orti.io.api.response.UserResponse;
import it.orti.io.api.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/api/user")
public class UserApi {
    @Autowired
    UserService userService;

    @GetMapping()
    public List<UserResponse> getAllUserResponse() {
        return userService.getAllUserResponse();
    }


    @GetMapping("/{userId}")
    public ResponseEntity<UserResponse> getUserById(@PathVariable String userId) {
        UserResponse response = userService.getUserById(userId);
        if (response == null) return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }


    @GetMapping("/{email}/{password}")
    public ResponseEntity<UserResponse> getLoginUser(@PathVariable String email, @PathVariable String password) {
        UserResponse response = userService.loginUser(email, password);
        if (response == null) return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/history/{userId}")
    public List<Appointment> getPastAppointments(@PathVariable String userId) {
        return userService.getPastAppointments(userId);
    }

    @GetMapping("/email/{email}")
    public ResponseEntity<UserResponse> validateEmailToGetToken(@PathVariable String email) {
        UserResponse response = userService.validateEmailToGetToken(email);
        if (response == null) return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/appointmentList/{userId}")
    public List<Appointment> getAppointmentListByUserId(@PathVariable String userId) {
        return userService.getAppointmentListByUserId(userId);
    }

    @PostMapping
    public ResponseEntity<UserResponse> createUser(@RequestBody User user) {
        UserResponse response = userService.createUser(user);
        if (response == null) return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PostMapping("/{password}/{userId}")
    public ResponseEntity<UserResponse> resetPassword(@PathVariable String password, @PathVariable String userId) {
        UserResponse response = userService.resetPassword(password, userId);
        if (response == null) return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PutMapping("/{userId}")
    public ResponseEntity<UserResponse> updateCustomer(@PathVariable String userId, @RequestBody User user) {
        UserResponse response = userService.updateCustomer(userId, user);
        if (response == null) return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @DeleteMapping("/{userId}")
    public ResponseEntity<UserResponse> deleteCustomerById(@PathVariable String userId) {
        UserResponse response = userService.deleteUserById(userId);
        if (response == null) return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

}
