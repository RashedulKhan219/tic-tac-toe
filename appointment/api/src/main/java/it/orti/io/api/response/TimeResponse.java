package it.orti.io.api.response;

import it.orti.io.api.model.Time;
import lombok.Data;

@Data
public class TimeResponse {
    private String timeId;
    private String time;
    private String date;
    private String barberId;
    private String barberName;

    public TimeResponse(Time time) {
        this.time = time.getTime();
        timeId = time.getTimeId();
        barberId = time.getBarberId();
        barberName = time.getBarberName();
        date = time.getDate();
    }

}
