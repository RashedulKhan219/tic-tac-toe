package it.orti.io.api.response;

import it.orti.io.api.model.Barber;
import it.orti.io.api.model.STATE;
import lombok.Data;


@Data
public class BarberResponse {
    private String barberId;
    private String firstName;
    private String lastName;
    private String phone;
    private String email;
    private String fileId;
    private String tokenId;
    private STATE role;
    private String currentRole;

    public BarberResponse(Barber barber) {
        barberId = barber.getBarberId();
        firstName = barber.getFirstName();
        lastName = barber.getLastName();
        phone = barber.getPhone();
        email = barber.getEmail();
        fileId = barber.getFileId();
        tokenId = barber.getTokenId();
        role = barber.getRole();
        currentRole = barber.getCurrentRole();
    }
}
