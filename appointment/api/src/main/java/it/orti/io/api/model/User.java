package it.orti.io.api.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.apache.commons.lang3.RandomStringUtils;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
public class User {

    private String userId;
    private String firstName;
    private String lastName;
    private String phone;
    private String email;
    private String password;
    private String fileId;
    private String tokenId;
    private STATE role;
    private List<Appointment> appointmentList;

    public User() {
        appointmentList = new ArrayList<>();
        userId = RandomStringUtils.randomNumeric(4);
        role = STATE.USER;
    }
}
