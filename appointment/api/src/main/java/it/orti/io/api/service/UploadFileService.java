package it.orti.io.api.service;

import it.orti.io.api.model.UploadFile;
import it.orti.io.api.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class UploadFileService {

    @Autowired
    UserService userService;


    Map<String, UploadFile> fileUploadMap = new HashMap<>();

    public UploadFile createFile(MultipartFile file, String userId) {
        if (fileUploadMap.containsKey(userId)) {
            return updateFile(userId, file);
        } else {
            UploadFile uploadFile = new UploadFile();
            try {
                uploadFile.setFileData(file.getBytes());
                uploadFile.setFileName(file.getOriginalFilename());
                uploadFile.setFileType(file.getContentType());
                uploadFile.setUserId(userId);
                if (userService.userMap.containsKey(userId)) {
                    User user = userService.userMap.get(userId);
                    user.setFileId(userId);
                }
                fileUploadMap.put(userId, uploadFile);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return uploadFile;
        }
    }

    public List<UploadFile> getAllFile() {
        return new ArrayList<>(fileUploadMap.values());
    }

    public ResponseEntity<Resource> getFileById(String fileId) {
        if (fileUploadMap.containsKey(fileId)) {
            UploadFile uploadFile = fileUploadMap.get(fileId);
            return ResponseEntity.ok()
                    .contentType(MediaType.parseMediaType(uploadFile.getFileType()))
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + uploadFile.getFileName() + "\"")
                    .body((new ByteArrayResource(uploadFile.getFileData())));
        }
        return null;

    }

    public ResponseEntity<Resource> getFileByUserId(String fileId) {
        if (fileUploadMap.containsKey(fileId)) {
            UploadFile uploadFile = fileUploadMap.get(fileId);
            return ResponseEntity.ok()
                    .contentType(MediaType.parseMediaType(uploadFile.getFileType()))
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + uploadFile.getFileName() + "\"")
                    .body((new ByteArrayResource(uploadFile.getFileData())));
        }
        return null;
    }


    public UploadFile updateFile(String fileId, MultipartFile file) {
        if (fileUploadMap.containsKey(fileId)) {
            UploadFile existingFile = fileUploadMap.get(fileId);
            try {
                existingFile.setFileData(file.getBytes());
                existingFile.setFileName(file.getOriginalFilename());
                existingFile.setFileType(file.getContentType());
                return existingFile;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

}
