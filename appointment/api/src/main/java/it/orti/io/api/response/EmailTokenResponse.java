package it.orti.io.api.response;

import it.orti.io.api.model.EmailToken;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class EmailTokenResponse {
    private String emailTokenId;
    private String toEmail;
    private String body;
    private String subject;
    private String userId;

    public EmailTokenResponse(EmailToken emailToken) {
        emailTokenId = emailToken.getEmailTokenId();
        toEmail = emailToken.getToEmail();
        body = emailToken.getBody();
        subject = emailToken.getSubject();
        userId = emailToken.getUserId();
    }
}
