package it.orti.io.api.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
public class AppointmentRequest {
    private String date;
    private String time;
    private String timeId;
    private String gender;
    private String barberName;
    private String barberId;
    private String email;
    private String services;
    private LocalDateTime localDateTime;

    public AppointmentRequest() {
        localDateTime = LocalDateTime.now();
    }
}
