package it.orti.io.api.api;


import it.orti.io.api.model.Time;
import it.orti.io.api.model.TimeRequest;
import it.orti.io.api.response.TimeResponse;
import it.orti.io.api.service.TimeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@CrossOrigin
@RestController
@RequestMapping("/api/time")
public class TimeApi {

    @Autowired
    TimeService timeService;


    @GetMapping
    public List<TimeResponse> getAllTimes() {
        return timeService.getAllTimes();
    }

    @GetMapping("/{barberId}")
    public List<TimeResponse> getAllTimesById(@PathVariable String barberId) {
        return timeService.getAllTimesById(barberId);
    }

    @PostMapping
    public TimeResponse createTimeByBarber(@RequestBody TimeRequest timeRequest) {
        return timeService.createTimeByBarber(timeRequest);
    }

    @DeleteMapping("/{timeId}")
    public ResponseEntity<Time> deleteTimeByTimeId(@PathVariable String timeId) {
        Time response = timeService.deleteTimeByTimeId(timeId);
        if (response == null) return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }


}
