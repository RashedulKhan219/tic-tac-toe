package it.orti.io.api.model;

public enum STATE {
    ADMIN, USER, BARBER
}
