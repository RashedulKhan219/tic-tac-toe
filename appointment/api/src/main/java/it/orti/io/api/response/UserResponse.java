package it.orti.io.api.response;

import it.orti.io.api.model.Appointment;
import it.orti.io.api.model.STATE;
import it.orti.io.api.model.User;
import lombok.Data;

import java.util.List;

@Data
public class UserResponse {
    private String userId;
    private String firstName;
    private String lastName;
    private String phone;
    private String email;
    private String fileId;
    private String tokenId;
    private STATE role;
    private List<Appointment> appointmentList;


    public UserResponse(User user) {
        userId = user.getUserId();
        firstName = user.getFirstName();
        lastName = user.getLastName();
        phone = user.getPhone();
        email = user.getEmail();
        fileId = user.getFileId();
        tokenId = user.getTokenId();
        role = user.getRole();
        appointmentList = user.getAppointmentList();
    }
}
