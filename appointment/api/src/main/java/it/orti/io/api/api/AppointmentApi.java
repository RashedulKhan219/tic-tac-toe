package it.orti.io.api.api;


import it.orti.io.api.model.AppointmentRequest;
import it.orti.io.api.response.AppointmentResponse;
import it.orti.io.api.service.AppointmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/api/appointment")
public class AppointmentApi {

    @Autowired
    AppointmentService appointmentService;

    @GetMapping
    public List<AppointmentResponse> getAllAppointment() {
        return appointmentService.getAllAppointment();
    }

    @GetMapping("/{appointmentId}")
    public AppointmentResponse getAppointmentById(@PathVariable String appointmentId) {
        return appointmentService.getAppointmentById(appointmentId);
    }

    @GetMapping("/user/{userId}")
    public AppointmentResponse getAppointmentByUserId(@PathVariable String userId) {
        return appointmentService.getAppointmentByUserId(userId);

    }

    @GetMapping("/email/{email}")
    public AppointmentResponse getAppointmentByEmail(@PathVariable String email) {
        return appointmentService.getAppointmentByEmail(email);
    }

    @GetMapping("/appointmentList/{barberId}")
    public List<AppointmentResponse> getAppointmentsByBarberId(@PathVariable String barberId) {
        return appointmentService.getAppointmentsByBarberId(barberId);
    }

    @PostMapping("/{userId}")
    public AppointmentResponse createAppointment(@PathVariable String userId, @RequestBody AppointmentRequest appointmentRequest) {
        return appointmentService.createAppointment(userId, appointmentRequest);
    }

    @PutMapping("/{appointmentId}")
    public AppointmentResponse updateAppointment(@PathVariable String appointmentId, @RequestBody AppointmentRequest appointmentRequest) {
        return appointmentService.updateAppointment(appointmentId, appointmentRequest);
    }

    @DeleteMapping("/{appointmentId}")
    public ResponseEntity<AppointmentResponse> deleteAppointment(@PathVariable String appointmentId) {
        AppointmentResponse response = appointmentService.deleteAppointment(appointmentId);
        if (response == null) return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
