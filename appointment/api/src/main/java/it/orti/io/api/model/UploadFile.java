package it.orti.io.api.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.apache.commons.lang3.RandomStringUtils;

@Data
@AllArgsConstructor
public class UploadFile {
    private String fileId;
    private String fileName;
    private String fileType;
    private String userId;
    private byte[] fileData;

    public UploadFile() {
        fileId = RandomStringUtils.randomNumeric(6);
    }
}
