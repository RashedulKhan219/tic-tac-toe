package it.orti.io.api.service;

import it.orti.io.api.model.Appointment;
import it.orti.io.api.model.STATE;
import it.orti.io.api.model.User;
import it.orti.io.api.response.UserResponse;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class UserService {

    Map<String, User> userMap = new HashMap<>();

    // create user
    public UserResponse createUser(User user) {
        if (validateUserByEmail(user.getEmail())) {
            return null;
        } else {
            if (user.getRole().equals(STATE.ADMIN)) {
                user.setRole(STATE.ADMIN);
            } else if (user.getRole().equals(STATE.BARBER)) {
                user.setRole(STATE.BARBER);
            }
            userMap.put(user.getUserId(), user);
            return new UserResponse(user);
        }
    }

    //get all user
    public List<UserResponse> getAllUserResponse() {
        List<UserResponse> customerResponseList = new ArrayList<>();
        for (User customer : userMap.values()) {
            customerResponseList.add(new UserResponse(customer));
        }
        return customerResponseList;
    }

    // get a single user
    public UserResponse getUserById(String userId) {
        if (userMap.containsKey(userId)) {
            return new UserResponse(userMap.get(userId));
        }
        return null;
    }

    // login user
    public UserResponse loginUser(String email, String password) {
        for (User user : userMap.values()) {
            if (user.getEmail().equals(email) &&
                    user.getPassword().equals(password)) {
                return new UserResponse(user);
            }
        }
        return null;
    }

    // update user
    public UserResponse updateCustomer(String userId, User user) {
        if (userMap.containsKey(userId)) {
            User user1 = userMap.get(userId);
            user1.setFirstName(user.getFirstName());
            user1.setLastName(user.getLastName());
            user1.setPhone(user.getPhone());
            user1.setEmail(user.getEmail());
            return new UserResponse(user1);
        }
        return null;
    }

    // delete user
    public UserResponse deleteUserById(String userId) {
        if (userMap.containsKey(userId)) {
            User user = userMap.get(userId);
            userMap.remove(userId);
            return new UserResponse(user);
        }
        return null;
    }

    // reset password
    public UserResponse resetPassword(String password, String userId) {
        if (userMap.containsKey(userId)) {
            User user = userMap.get(userId);
            user.setPassword(password);
            return new UserResponse(user);
        }
        return null;
    }

    // get all past appointments
    public List<Appointment> getPastAppointments(String userId) {
        if (userMap.containsKey(userId)) {
            List<Appointment> appointmentList = userMap.get(userId).getAppointmentList();
            appointmentList.sort(Comparator.comparing(Appointment::getLocalDateTime));
            return appointmentList;
        }
        return null;
    }


    public boolean validateUserByEmail(String email) {
        for (User user1 : userMap.values()) {
            if (user1.getEmail().equals(email)) {
                return true;
            }
        }
        return false;
    }

    // validate email to get token
    public UserResponse validateEmailToGetToken(String email) {
        for (User user : userMap.values()) {
            if (user.getEmail().equals(email)) {
                return new UserResponse(user);
            }
        }
        return null;
    }

    // get appointmentList by userId
    public List<Appointment> getAppointmentListByUserId(String userId) {
        if (userMap.containsKey(userId)) {
            return userMap.get(userId).getAppointmentList();
        }
        return null;
    }


}
