package it.orti.io.api.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.apache.commons.lang3.RandomStringUtils;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
public class Appointment {

    private String appointmentId;
    private String date;
    private String time;
    private String timeId;
    private String gender;
    private String barberName;
    private String barberId;
    private String userId;
    private String email;
    private String services;
    private LocalDateTime localDateTime;

    public Appointment(String userId, AppointmentRequest appointmentRequest) {
        this.userId = userId;
        date = appointmentRequest.getDate();
        time = appointmentRequest.getTime();
        timeId = appointmentRequest.getTimeId();
        gender = appointmentRequest.getGender();
        barberName = appointmentRequest.getBarberName();
        localDateTime = appointmentRequest.getLocalDateTime();
        email = appointmentRequest.getEmail();
        barberId = appointmentRequest.getBarberId();
        services = appointmentRequest.getServices();
        appointmentId = RandomStringUtils.randomNumeric(6);

    }
}
