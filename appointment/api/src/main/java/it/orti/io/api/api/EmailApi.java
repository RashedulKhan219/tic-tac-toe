package it.orti.io.api.api;

import it.orti.io.api.model.EmailToken;
import it.orti.io.api.response.EmailTokenResponse;
import it.orti.io.api.service.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping("/api/sendmail")
public class EmailApi {

    @Autowired
    EmailService emailService;

    // this mapping verifies user email and generate an email token
    @GetMapping("/{email}/{userId}")
    public ResponseEntity<EmailTokenResponse> getEmailTokenByUserEmail(@PathVariable String email, @PathVariable String userId) {
        EmailTokenResponse response = emailService.getEmailTokenByUserEmail(email, userId);
        if (response == null) return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    // after getting the token verify the token
    @PutMapping("/{emailTokenId}")
    public ResponseEntity<EmailTokenResponse> verifyEmailToken(@PathVariable String emailTokenId) {
        EmailTokenResponse response = emailService.verifyEmailToken(emailTokenId);
        if (response == null) return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }


}
