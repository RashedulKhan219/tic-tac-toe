package it.orti.io.api.api;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class RedirectApi {

    @RequestMapping("/**/{path:[^\\.]+}")
    public String forward() {
        return  "forward:/";
    }
}
