package it.orti.io.api.service;

import it.orti.io.api.model.EmailToken;
import it.orti.io.api.response.EmailTokenResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class EmailService {


    private final JavaMailSender mailSender;

    @Autowired
    public EmailService(JavaMailSender mailSender) {
        this.mailSender = mailSender;
    }

    Map<String, EmailToken> emailTokenMap = new HashMap<>();

    public EmailTokenResponse getEmailTokenByUserEmail(String email, String userId) {
        try {
            EmailToken emailToken = sendEmail(email, userId);
            emailTokenMap.put(emailToken.getEmailTokenId(), emailToken);
            return new EmailTokenResponse(emailToken);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public EmailToken sendEmail(String toEmail,String userId) {
        EmailToken emailToken = new EmailToken();
        emailToken.setUserId(userId);
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setFrom("amitahamed1993@gmail.com");
        mailMessage.setTo(toEmail);
        mailMessage.setText(emailToken.getBody() + "-" + "\n" + emailToken.getEmailTokenId());
        mailMessage.setSubject(emailToken.getSubject() + "from merry hair saloon");
        mailSender.send(mailMessage);
        emailTokenMap.put(emailToken.getEmailTokenId(), emailToken);
        return emailToken;
    }

    public EmailTokenResponse verifyEmailToken(String emailTokenId) {
        if (emailTokenMap.containsKey(emailTokenId)) {
            EmailToken emailToken = emailTokenMap.get(emailTokenId);
            // removing email token after verify
            emailTokenMap.remove(emailTokenId);
            return new EmailTokenResponse(emailToken);
        }
        return null;
    }


}
