package it.orti.io.api.service;

import it.orti.io.api.model.Time;
import it.orti.io.api.model.TimeRequest;
import it.orti.io.api.response.TimeResponse;
import org.springframework.stereotype.Service;

import java.util.*;


@Service
public class TimeService {

    Map<String, Time> barberTimesMap = new HashMap<>();

    // time created by barber
    public TimeResponse createTimeByBarber(TimeRequest timeRequest) {
        Time time1 = new Time(timeRequest);
        barberTimesMap.put(time1.getTimeId(), time1);
        return new TimeResponse(time1);
    }

    // get all barber times
    public List<TimeResponse> getAllTimes() {
        List<TimeResponse> timeList = new ArrayList<>();
        for (Time time : barberTimesMap.values()) {
            timeList.add(new TimeResponse(time));
        }
        timeList.sort(Comparator.comparing(TimeResponse::getTime));
        return timeList;
    }

    // get individual barber timesList by barberId
    public List<TimeResponse> getAllTimesById(String barberId) {
        List<TimeResponse> timeResponseList = new ArrayList<>();
        for (Time time : barberTimesMap.values()) {
            if (time.getBarberId().equals(barberId)) {
                timeResponseList.add(new TimeResponse(time));
            }
        }
        timeResponseList.sort(Comparator.comparing(TimeResponse::getTime));
        return timeResponseList;
    }

    // delete time from the barberTimeMap
    public Time deleteTimeByTimeId(String timeId) {
        if (barberTimesMap.containsKey(timeId)) {
            Time time = barberTimesMap.get(timeId);
            barberTimesMap.remove(timeId);
            return time;
        }
        return null;
    }

}
