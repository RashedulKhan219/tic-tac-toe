import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import 'semantic-ui-css/semantic.min.css'
import {BrowserRouter} from 'react-router-dom'
import de from 'javascript-time-ago/locale/de'
import TimeAgo from 'javascript-time-ago'
import en from 'javascript-time-ago/locale/en'
import SimpleReactLightbox from 'simple-react-lightbox'


TimeAgo.addDefaultLocale(en)
TimeAgo.addLocale(de)

ReactDOM.render(
  <BrowserRouter>
  <React.StrictMode>
  <SimpleReactLightbox>
      <App />
    </SimpleReactLightbox>
  </React.StrictMode>
  </BrowserRouter>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
