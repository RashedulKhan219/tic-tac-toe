import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Switch, Route } from 'react-router-dom';
import Home from './component/home/Home';
import SignUp from './component/user/SignUp';
import Login from './component/user/Login';
import UserResponse from './component/user/UserResponse';
import CreateAppointment from './component/appointment/CreateAppointment';
import AppointmentList from './component/appointment/AppointmentList';
import UpdateUser from './component/user/UpdateUser';
import AppointmentResponse from './component/appointment/AppointmentResponse';
import UpdateAppointment from './component/appointment/UpdateAppointment';
import Footer from './component/home/Footer';
import ExistingAppointment from './component/home/ExistingAppointment';
import UserList from './component/user/UserList';
import ForgetPassword from './component/user/ForgetPassword';
import ChangePassword from './component/user/ChangePassword';
import VerifyToken from './component/user/VerifyToken';
import CheckHistory from './component/user/CheckHistory';
import BusinessProfile from './component/business/BusinessProfile';
import BusinessHome from './component/business/BusinessHome';
import HairDresserProfile from './component/hairdresser/HairDresserProfile';
import UpdateBarber from './component/hairdresser/UpdateBarber';
import BusinessForgetPass from './component/business/BusinessForgetPass';
import VerifyBusinessToken from './component/business/VerifyBusinessToken';
import ResetPassword from './component/business/ResetPassword';
import PageNotFound from './component/home/PageNotFound';





function App() {
  return (
    <div className="App">
       <header className="App-header">
        <Switch>
          <Route path="/" exact component={Home} />
          <Route path="/signup" exact component={SignUp}/>
          <Route path="/login" exact component={Login}/>
          <Route path="/userresponse/:userId" exact component={UserResponse}/>
          <Route path="/updateuser/:userId" exact component={UpdateUser}/>
          <Route path="/createappointment/:userId" exact component={CreateAppointment}/>
          <Route path="/appointmentresponse/:appointmentId/:userId" exact component={AppointmentResponse}/>
          <Route path="/updateappointment/:appointmentId/:barberId" exact component={UpdateAppointment}/>
          <Route path="/appointmentlist" exact component={AppointmentList}/>
          <Route path="/existingappointment/:appointmentId/:userId" exact component={ExistingAppointment} />
          <Route path="/userlist" exact component={UserList} />
          <Route path="/forgetpassword" exact component={ForgetPassword} />
          <Route path="/changepassword/:userId" exact component={ChangePassword} />
          <Route path="/verifytoken/:email/:userId" exact component={VerifyToken}/>         
          <Route path="/checkhistory/:userId" exact component={CheckHistory}/> 
          <Route path="/businessprofile/" exact component={BusinessProfile}/>  
          <Route path="/businessprofile/:barberId" exact component={BusinessProfile}/>        
          <Route path="/businesshome" exact component={BusinessHome}/>        
          <Route path="/hairdresserprofile/:barberId" exact component={HairDresserProfile}/>        
          <Route path="/updatebarber/:barberId" exact component={UpdateBarber}/>        
          <Route path="/businessforgetpassword" exact component={BusinessForgetPass}/>        
          <Route path="/verifybusinesstoken/:email/:barberId" exact component={VerifyBusinessToken}/>        
          <Route path="/resetpassword/:barberId" exact component={ResetPassword}/> 
          <Route component={PageNotFound}/>       
        </Switch>
      </header>
      <Footer/>
    </div>
  );
}

export default App;
