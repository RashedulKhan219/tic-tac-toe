import React,{ useEffect,useState } from 'react'
import { Modal,Spinner,Form,Figure,Col,Row,Container } from 'react-bootstrap'
import { useParams,useHistory } from 'react-router-dom'
import UserApi from '../api/UserApi';
import Navigation from '../home/Navigation';
import { Label,Button } from 'semantic-ui-react';
import AppointmentApi from '../api/AppointmentApi';
import UploadImageApi from '../api/UploadImageApi';
import CONSTANT from '../utils/CONSTANT';
import ReactTimeAgo from 'react-time-ago'
import picture from '../images/no-photo-available-black-profile.jpeg'
import { SRLWrapper } from "simple-react-lightbox";

const UserResponse = () => {

    const { userId } = useParams();
    const [user,setUser] = useState();
    const history = useHistory();
    const [appointment,setAppointment] = useState();
    const [file,setFile] = useState();
    const [show,setShow] = useState(false);
    const [createdFile,setCreatedFile] = useState();
    const [loading,setLoading] = useState(false);
    const [logout,setLogout] = useState(false);
    const [appointmentId,setAppointmentId] = useState();
    const [isCancelClicked,setCancelClicked] = useState(false);
    const [isUploadClicked,setUploadClicked] = useState(false);



    useEffect(() => {
        UserApi.getUserById(userId).then(user => {
            setUser(user)
        })
        AppointmentApi.getAppointmentByUserId(userId).then(appointment => {
            setAppointment(appointment)
            setAppointmentId(appointment.appointmentId)
        })
        UploadImageApi.getFileByUserId(userId).then(file => setCreatedFile(file))
        const saveLogout = localStorage.getItem(CONSTANT.LOCAL_STORAGE.LOGOUT);
        if (saveLogout) {
            setLogout(JSON.parse(saveLogout));
        }

    },[userId])

    const handleScheduleAppointment = () => {
        history.push("/createappointment/" + userId)
    }

    const handleUpdate = () => {
        history.push("/updateuser/" + userId)
    }

    const handleUpdateAppointment = () => {
        history.push("/updateappointment/" + appointmentId + "/" + appointment.barberId);
    }

    const handleDeleteAppointment = () => {
        AppointmentApi.deleteAppointment(appointmentId).then(deleteAppointment => {
       
            if (deleteAppointment) {
                AppointmentApi.getAppointmentById(appointmentId).then(appointment => {
                    setAppointment(appointment)
                    setAppointmentId(appointment.appointmentId);
                });
                handleClose();
                setLoading(false);
                setAppointment(null);
                setUploadClicked(false);
                setCancelClicked(false);
            }
        })
    }

    const handleCancelButton = (event) => {
        event.preventDefault();
        setLoading(true);
        handleShow();
        setTimeout(handleDeleteAppointment,1000);

    }
    const handleCancleChange = () => {
        handleShow();
        setUploadClicked(false);
        setCancelClicked(true);

    }

    const handleClose = () => {
        setShow(false)
    };
    const handleShow = () => setShow(true);

    const handleCreateFile = () => {
        const formData = new FormData();
        formData.append('file',file)
        UploadImageApi.createFile(formData,userId).then(file => {
            if (file) {
                setCreatedFile(file);
                UploadImageApi.getFileByUserId(userId).then(file => {
                    setFile(file)
                    setLoading(false);
                    handleClose();
                    setUploadClicked(true);
                    setCancelClicked(false);
                })
            }
        })
    }

    const handleSubmitButton = (event) => {
        event.preventDefault();
        setLoading(true);
        setTimeout(handleCreateFile,1000);
    }

    const handleUploadChange = (event) => {
        event.preventDefault();
        setUploadClicked(true);
        setCancelClicked(false);
        handleShow();
    }


    if (logout) {
        history.push("/login")
    }


    return <div>
        <Navigation />
        <Container>
            <div className="mt-5 pb-5">
                {!appointmentId &&
                    <Row>
                        <Col md={6} className="ml-auto mr-auto ">
                            <Label basic color='red' size="big" pointing='below' prompt>
                                Currenlty you don't have any appointment.
                            </Label>
                        </Col>
                    </Row>}


                {user && <Row >

                    <Col md={4} sm={6} className="ml-auto mr-auto mb-3">
                        <SRLWrapper>
                            <Figure>
                                <Figure.Image
                                    className="rounded"
                                    style={{ cursor: 'pointer' }}
                                    width={150}
                                    height={150}
                                    alt="171x180"
                                    src={createdFile ? CONSTANT.URL.UPLOADFILE + "/user/" + userId : picture}
                                    thumbnail
                                    
                                />
                            </Figure>
                        </SRLWrapper>

                        <Button disabled={logout} color='pink' content='Upload photo'
                            onClick={handleUploadChange}
                            icon='photo' />
                    </Col>

                    <Col md={4} sm={6} className="ml-auto mr-auto text-left mb-3">
                        <h2>User Information</h2>
                        <p>FirstName: {user.firstName}</p>
                        <p>LastName: {user.lastName}</p>
                        <p>Phone: {user.phone}</p>
                        <p>Email: {user.email}</p>
                        <Button disabled={logout} onClick={handleUpdate} color='teal'>
                            Update info
                        </Button>
                    </Col>


                    <Col md={4} sm={6} className="ml-auto mr-auto text-left">
                        {appointment ? <div>
                            <h2>Upcoming Appointment</h2>

                            <p className="text-warning">
                                Appointment was Scheduled <ReactTimeAgo date={appointment.localDateTime} locale="en-US" />
                            </p>

                            <p>AppointmentID: {appointment.appointmentId}</p>
                            <p>Time: {appointment.time}</p>
                            <p>Date: {appointment.date}</p>
                            <p>Gender: {appointment.gender}</p>
                            <p>Hair-dresser: {appointment.barberName}</p>
                            <p>Services: {appointment.services}</p>

                            <Button.Group>
                                <Button disabled={logout} onClick={handleCancleChange} color='red'>Cancel</Button>
                                <Button.Or />
                                <Button positive disabled={logout} onClick={handleUpdateAppointment} color='green'>Update</Button>
                            </Button.Group>
                            <p className="mt-3">You can update or cancel this appointment</p>
                        </div> :
                            <div className="ml-auto mr-auto pb-5">
                                <p>If you want to schedule an appointment click on schedule appointment</p>
                                <Button color="blue" disabled={logout} onClick={handleScheduleAppointment}>
                                    Schedule an appointment
                            </Button>
                            </div>}
                    </Col>

                </Row>}

            </div>


            <Modal show={show} onHide={handleClose} className="bg-dark" centered>
                <Modal.Header closeButton>

                    {isUploadClicked ? <Modal.Title>Upload image</Modal.Title> : <Modal.Title>Cancel Appointment</Modal.Title>}
                </Modal.Header>
                {isUploadClicked ? <Modal.Body>
                    Do you want to upload an image ?
                </Modal.Body> :
                    <Modal.Body>
                        Do you want to cancel this appointment ?
                </Modal.Body>}
                <Modal.Footer>

                    {isUploadClicked ? <div>

                        <Form onSubmit={handleSubmitButton}>
                            <input
                                type="file"
                                required
                                multiple={false}
                                id="upload-button"
                                onChange={(e) => setFile(e.target.files[0])}
                            />
                            <Button type="submit" color='instagram'>submit
                    {loading && <Spinner animation="border" size="sm" variant="white" />}
                            </Button>
                        </Form>
                    </div> : <div>
                        <Button type="submit" onClick={handleClose} color='grey'>
                            No
                    </Button>
                        <Button type="submit" color="red" onClick={handleCancelButton}>
                            Yes, cancel
                        {loading && <Spinner animation="border" size="sm" variant="white" />}
                        </Button>
                    </div>}

                </Modal.Footer>
            </Modal>

        </Container>
    </div>

}

export default UserResponse