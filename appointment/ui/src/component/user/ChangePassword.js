import React,{ useState } from 'react'
import Navigation from '../home/Navigation'
import { Form,Button,Spinner,Col,Container, Alert } from 'react-bootstrap';
import { useParams } from 'react-router-dom';
import { useHistory } from 'react-router-dom'
import CONSTANT from '../utils/CONSTANT';
import { useEffect } from 'react';
import UserApi from '../api/UserApi';


const ChangePassword = () => {
    
    const [loading,setLoading] = useState(false);
    const [password,setPassword] = useState();
    const [confirmPassword,setConfirmPassword] = useState();
    const { userId } = useParams();
    const history = useHistory();
    const [passChanged,setPassChanged] = useState(false);
    const [error, setError] = useState(false);
    const [currentType, setCurrentType] = useState('password');



    useEffect(() => {
        const savePassChanged = localStorage.getItem(CONSTANT.LOCAL_STORAGE.USERPASSCHANGED);
        if (savePassChanged) {
            setPassChanged(JSON.parse(savePassChanged));
        }

    },[])

    const handleChangePassword = () => {
            UserApi.resetPassword(password,userId).then(user => {
                localStorage.setItem(CONSTANT.LOCAL_STORAGE.USERPASSCHANGED,JSON.stringify(true))
                setLoading(false);
                history.push("/login")
            })
    }

    const handleSubmitButton = (event) => {
        event.preventDefault();
        setLoading(true)
        if (password === confirmPassword) {
        setTimeout(handleChangePassword,1000)
        } else{
            setLoading(false);
           return setError(true);
        }
    }

    const handleShowPassword = () => {
        if(currentType === 'password'){
            setCurrentType('text');
        } else {
            setCurrentType('password');
        }
    }

    if (passChanged) {
        history.push("/login")
    }

    return <div>
        <Navigation />
        <Container className="mt-5 pt-5">

            <Col md={5} className="ml-auto mr-auto text-left">
                <Form onSubmit={handleSubmitButton}>
                    <Form.Label>Password</Form.Label>
                    <Form.Control
                        required
                        type={currentType && currentType}
                        placeholder="Enter new password"
                        onChange={(e) => setPassword(e.target.value)}
                        className='mb-3'
                        maxLength={15}
                        minLength={5}
                        
                    />
                    <Form.Label>Confirm-password</Form.Label>
                    <Form.Control
                        required
                        type={currentType && currentType}
                        placeholder="confirm - password"
                        onChange={(e) => setConfirmPassword(e.target.value)}
                        className='mb-3'
                        maxLength={15}
                        minLength={5}
                    />
                    <Form.Group className="text-left" controlId="formBasicCheckbox">
                        <Form.Check 
                        type="checkbox" 
                        label="Show password" 
                        onClick={handleShowPassword} />
                    </Form.Group>
                    <Button type='submit'>
                       Change
                    {loading && <Spinner animation="border" size="sm" variant='light' />}
                    </Button>
                    {error && <Alert variant="danger" className="mt-4" dismissible onClose={()=> setError(false)}>
                        Password doesn't match..!
                        </Alert>}
                </Form>

            </Col>
        </Container>
    </div>

}

export default ChangePassword