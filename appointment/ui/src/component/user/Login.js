import React,{ useState,useEffect } from 'react'
import { Form,Button,Container,Alert,Row,Col } from 'react-bootstrap'
import Navigation from '../home/Navigation'
import UserApi from '../api/UserApi';
import { useHistory,Link } from 'react-router-dom';
import CONSTANT from '../utils/CONSTANT';

const Login = () => {

    const [email,setEmail] = useState();
    const [password,setPassword] = useState();
    const history = useHistory();
    const [alert,setAlert] = useState(false);
    const [logout,setLogout] = useState(false)
    const [user,setUser] = useState();
    const [passChanged,setPassChanged] = useState(false);
    const [type,setType] = useState("password");
    const [showPassword,setShowPasssword] = useState("Show passowrd")



    useEffect(() => {
        const saveLogout = localStorage.getItem(CONSTANT.LOCAL_STORAGE.LOGOUT);
        const saveUser = localStorage.getItem(CONSTANT.LOCAL_STORAGE.USER);
        const passChanged = localStorage.getItem(CONSTANT.LOCAL_STORAGE.USERPASSCHANGED);
        if (saveLogout && saveUser && passChanged) {
            setLogout(JSON.parse(saveLogout));
            setUser(JSON.parse(saveUser));
            setPassChanged(JSON.parse(passChanged));
        }
    },[])


    const handleLogin = (event) => {
        event.preventDefault();
        UserApi.loginUser(email,password).then(user => {
            localStorage.setItem(CONSTANT.LOCAL_STORAGE.USER,JSON.stringify(user));
            localStorage.setItem(CONSTANT.LOCAL_STORAGE.LOGOUT,JSON.stringify(false));
            localStorage.setItem(CONSTANT.LOCAL_STORAGE.USERPASSCHANGED,JSON.stringify(false));
            if (user && user.role === 'USER') {
                history.push("/userresponse/" + user.userId)
            }
        }).catch(error => {
            setAlert(true)
        })
    }



    const handleClose = () => {
        setPassChanged(false);
        localStorage.setItem(CONSTANT.LOCAL_STORAGE.USERPASSCHANGED,JSON.stringify(false));
    }


    const handleShowPassword = (event) => {
        const value = document.getElementById("myPassword");
        console.log(value)
        if (value.type === "password") {
            setType("text");
            setShowPasssword("Hide password")
        } else {
            setType("password");
            setShowPasssword("Show password")
        }

    }


    if (user) {
        if (user.role === 'BARBER') {
            history.push("/hairdresserprofile/" + user.barberId)
        } else if (user.role === 'ADMIN') {
            history.push("/businessprofile/" + user.barberId)
        } else if (user.role === 'USER') {
            history.push("/userresponse/" + user.userId);
        }
    }

    if (logout && user) {
        if (user.role === 'BARBER') {
            history.push("/businesshome")
        } else if (user.role === 'ADMIN') {
            history.push("/businesshome")
        } else if (user.role === 'USER') {
            history.push("/login");
        }
    }

    return <div >
        <Navigation />
        <Container className="mt-5 pt-5 pb-5">

            <p >Please login to your account</p>

            <Form className="text-left" onSubmit={handleLogin}>

                <Form.Group as={Row} controlId="formHorizontalEmail" >
                    <Col sm={6} className="ml-auto mr-auto">
                        <Form.Label >
                            Email
                    </Form.Label>
                        <Form.Control type="email" placeholder="Enter email-address" required
                            className="mb-2" onChange={(e) => setEmail(e.target.value.toLowerCase())} />
                    </Col>
                </Form.Group>

                <Form.Group as={Row} controlId="formHorizontalPassword">
                    <Col sm={6} className="ml-auto mr-auto">
                        <Form.Label >
                            Password
                        </Form.Label>
                        <Form.Control type={type && type} placeholder="Enter password" required maxLength={15} minLength={5}
                            className="mb-2" onChange={(e) => setPassword(e.target.value)} id="myPassword" />

                        <div className="text-right" >
                            <Link style={{ color: 'cyan' }} to="/forgetpassword">ForgetPassword?</Link>
                        </div>
                        <Form.Group className="text-left" controlId="formBasicCheckbox">
                            <Form.Check onClick={handleShowPassword} type="checkbox" label={showPassword && showPassword} />
                        </Form.Group>
                    </Col>
                </Form.Group>

                <Form.Group as={Row} >
                    <Col sm={6} className="ml-auto mr-auto">
                        <Button type="submit" >Login</Button>
                    </Col>
                </Form.Group>

                <Form.Group as={Row}>
                    <Col sm={6} className="ml-auto mr-auto text-center">
                        {alert && <Alert variant="danger" dismissible className="text-left"
                            onClose={() => setAlert(false)} >
                            <p>
                                The email or password you entered doesn't match with our records!
                                Please enter valid email and password.
                            </p>
                        </Alert>}
                    </Col>
                </Form.Group>

            </Form>

            <p>
                Need an account ?
                 <Link className="text-warning" to="/signup">
                    {" "}SignUp
                </Link>
            </p>
            <Col sm={6} className="ml-auto mr-auto">
                {passChanged && <Alert variant="success" dismissible onClose={handleClose} >
                    Password has changed successfully. login with the new password.
                </Alert>}
            </Col>

            <div>

            </div>

        </Container>

    </div>

}
export default Login