import React,{ useEffect,useState } from 'react'
import { Container,Row,Col,Card } from 'react-bootstrap'
import { useParams } from 'react-router'
import UserApi from '../api/UserApi'
import Navigation from '../home/Navigation'
import ReactTimeAgo from 'react-time-ago'

const CheckHistory = () => {
    const [allAppointments,setAllAppointments] = useState([]);
    const { userId } = useParams();


    useEffect(() => {
        UserApi.getAppointmentListByUserId(userId).then(appointment => setAllAppointments(appointment));
    },[userId])

    console.log(allAppointments)
    return <div>
        <Navigation />
        <Container className="mt-5 pb-5">
            <h2>Past appointment history </h2>
            {allAppointments.length !== 0 && <Row className="mt-5">

                {allAppointments.map((appointment,index) => {
                    return <Col sm={6} md={4} key={index}
                        className="text-left rounded border shadow-lg  mb-3  ml-auto mr-auto">
                        <Card.Header>Date: {appointment.date}</Card.Header>
                        <Card.Body>
                            <p>Appointment-ID: {appointment.appointmentId}</p>
                            <p>Time: {appointment.time}</p>
                            <p>Date: {appointment.date}</p>
                            <p>Hair-dresser: {appointment.barberName}</p>
                            <p>
                            Appointment was taken <ReactTimeAgo date={appointment.localDateTime} locale="en-US" />
                            </p>
              
                        </Card.Body>

                    </Col>

                })}

            </Row>}
        </Container>
    </div>
}
export default CheckHistory