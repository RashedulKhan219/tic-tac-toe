import React,{ useState,useEffect } from 'react'
import Navigation from '../home/Navigation'
import { Container,Form,Button,Spinner } from 'react-bootstrap'
import UserApi from '../api/UserApi';
import { useParams,useHistory } from 'react-router-dom';
import { Card } from 'semantic-ui-react';
import CONSTANT from '../utils/CONSTANT';

const UpdateUser = () => {
    const { userId } = useParams();
    const [firstName,setFirstName] = useState();
    const [lastName,setLastName] = useState();
    const [email,setEmail] = useState();
    const [phone,setPhone] = useState();
    const history = useHistory();
    const [loading,setLoading] = useState(false);
    const [logout,setLogout] = useState(false);

    useEffect(() => {
        UserApi.getUserById(userId).then(user => {
            setFirstName(user.firstName);
            setLastName(user.lastName);
            setEmail(user.email);
            setPhone(user.phone);
        })

        const saveLogout = localStorage.getItem(CONSTANT.LOCAL_STORAGE.LOGOUT);
        if (saveLogout) {
            setLogout(JSON.parse(saveLogout));
        }

    },[userId])

    const handleUpdate = () => {

        UserApi.updateUser(userId,firstName,lastName,phone,email).then(updateUser => {
        
            localStorage.setItem(CONSTANT.LOCAL_STORAGE.USER, JSON.stringify(updateUser));
            if (updateUser && updateUser.role === 'USER') {
                setLoading(false);
                history.push("/userresponse/" + updateUser.userId);
            } else if (updateUser && updateUser.role === 'BARBER') {
                setLoading(false);
                history.push("/hairdresserprofile/" + updateUser.userId);
            } else if (updateUser && updateUser.role === 'ADMIN') {
                setLoading(false);
                history.push("/businessprofile/" + updateUser.userId);
            } else {
                setLoading(false);
            }

        })

    }

    const handleSaveChangesButton = (event) => {
        event.preventDefault();
        setLoading(true);
        setTimeout(handleUpdate,1000);

    }

    if (logout) {
        history.push("/login")
    }


    return <div>
        <Navigation />
        <Container >
            <div className="row">
                <Card className="col-lg-4 ml-auto mr-auto p-5 mt-5 text-left" style={{ backgroundColor: 'rebeccapurple' }}>
                    <Form onSubmit={handleSaveChangesButton} disabled={logout}>
                        <h1 className="text-center">update</h1>
                        <Form.Label>FirstName:</Form.Label>
                        <Form.Control type="text" defaultValue={firstName} className="text-capitalize mb-2" required
                            onChange={(e) => setFirstName(e.target.value)} />
                        <Form.Label>LastName:</Form.Label>
                        <Form.Control type="text" defaultValue={lastName} className="text-capitalize mb-2" required
                            onChange={(e) => setLastName(e.target.value)} />
                        <Form.Label>Phone:</Form.Label>
                        <Form.Control type="text" defaultValue={phone} className="mb-2" required
                            onChange={(e) => setPhone(e.target.value)} />
                        <Form.Label>Email:</Form.Label>
                        <Form.Control type="email" defaultValue={email} className="mb-4" required
                            onChange={(e) => setEmail(e.target.value.toLowerCase())} />
                        <Button type='submit' disabled={logout} className="col-lg" variant="info">
                            Save all changes
                      {loading && <Spinner animation='border' size='sm' variant='white' />}
                        </Button>

                    </Form>
                </Card>

            </div>

        </Container>

    </div>

}

export default UpdateUser