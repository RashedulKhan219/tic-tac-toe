import React from 'react'
import Navigation from '../home/Navigation'
import { Form,Button,Spinner,Container,Col, Alert } from 'react-bootstrap'
import { useState } from 'react'
import { useHistory } from 'react-router-dom'
import EmailApi from '../api/EmailApi'
import CONSTANT from '../utils/CONSTANT'
import UserApi from '../api/UserApi'



const ForgetPassword = () => {
    const [loading,setLoading] = useState(false);
    const history = useHistory();
    const [email,setEmail] = useState();
    const [error,setError] = useState(false);



    const handleNextButton = (event) => {
        event.preventDefault();
        setLoading(true);
        UserApi.validateEmailToGetToken(email).then(user => {
                EmailApi.getEmailTokenByUserEmail(email, user.userId).then(token => {
                    if (token) {
                        localStorage.setItem(CONSTANT.LOCAL_STORAGE.USERPASSCHANGED,JSON.stringify(false));
                        setLoading(false)
                        history.push("/verifytoken/" + email + "/" + user.userId);
                    } 
                })
        }).catch(error => {
            setLoading(false)
            setError(true);
        })

    }

    const handleCancelButton =()=>{
        history.push("/login")
    }


    return <div>
        <Navigation />
        <Container className="mt-5 pt-5">
            <p>Please enter your email to verify</p>
            <Col md={5} className=" ml-auto mr-auto text-left">
                <Form onSubmit={handleNextButton}>
                    <Form.Control
                        type="email"
                        placeholder="Enter email-address"
                        required
                        onChange={(e) => setEmail(e.target.value.toLowerCase())}
                    />
                    <div className="text-right">
                    <Button variant='secondary' onClick={handleCancelButton} className='mt-3 mr-2'>
                          Cancel
                    </Button>
                    <Button type="submit" className='mt-3'>
                            Search
                            {loading && <Spinner animation='border' size='sm' variant='light' />}
                    </Button>
                    </div>
                    {error && <Alert variant="danger mt-3" dismissible onClose={()=> setError(false)}>
                        Email doesn't exist in our record's.
                        </Alert>}
                </Form>
            </Col>

        </Container>
    </div>

}

export default ForgetPassword