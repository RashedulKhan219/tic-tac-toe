import React,{ useEffect,useState } from 'react'
import { Container,Table,Figure,Alert } from 'react-bootstrap';
import UserApi from '../api/UserApi';
import { Input } from 'semantic-ui-react'
import TablePagination from '@material-ui/core/TablePagination';
import Navigation from '../home/Navigation';
import { useHistory } from 'react-router-dom';
import CONSTANT from '../utils/CONSTANT';
import picture from '../images/no-photo-available-black-profile.jpeg'
import { SRLWrapper } from "simple-react-lightbox";



const UserList = () => {

    const [users,setUsers] = useState([]);
    const [search,setSearch] = useState('');
    const [isLogout,setIsLogout] = useState(false);
    const history = useHistory();



    useEffect(() => {
        UserApi.getAllUser().then(users => setUsers(users))
        const logout = localStorage.getItem(CONSTANT.LOCAL_STORAGE.LOGOUT)
        if (logout) {
            setIsLogout(JSON.parse(logout))
        }


    },[])

    const [page,setPage] = React.useState(0);
    const [rowsPerPage,setRowsPerPage] = React.useState(10);

    const handleChangePage = (event,newPage) => {
        setPage(newPage);
    }

    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(parseInt(event.target.value,10));
        setPage(3);
    }


    if (isLogout) history.push("/login")

    return <div>
        <Navigation />
        <Container className="mt-5 pb-5">
            <h2>UserList</h2>
            {users.length !== 0 && <div className='mb-3 text-light text-left'>
                <Input 
                size='mini' 
                iconPosition='right' 
                labelPosition='left' 
                label='Find user....' 
                type='text' 
                icon='search' 
                onChange={(e) => setSearch(e.target.value)} />
            </div>}
            {users.length !== 0 ?
                <div>
                    <Table striped bordered hover variant="dark">

                        <thead>
                            <tr>
                                <th>Photo</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Role</th>
                            </tr>
                        </thead>

                        <tbody>

                            {users.filter(user => {
                                if (search === '') {
                                    return user;
                                } else if (user.firstName.toLowerCase().includes(search.toLowerCase())
                                    || user.lastName.toLowerCase().includes(search.toLowerCase())
                                    || user.email.includes(search)
                                ) {
                                    return user;
                                } else {
                                    return null;
                                }
                            }).slice(page * rowsPerPage,page * rowsPerPage + rowsPerPage).map((user,index) => {

                                return <tr key={index}>



                                    <td >
                                        <SRLWrapper>
                                            <Figure>
                                                <Figure.Image
                                                    className="rounded"
                                                    style={{ cursor: 'pointer' }}
                                                    width={50}
                                                    height={50}
                                                    alt="171x180"
                                                    src={user.fileId ? CONSTANT.URL.UPLOADFILE + "/" + user.fileId : picture}
                                                    roundedCircle
                                                />
                                            </Figure>
                                        </SRLWrapper>

                                    </td>

                                    <td className="text-capitalize" >{user.firstName + " " + user.lastName}</td>

                                    <td>{user.email}</td>
                                    <td>{user.phone}</td>
                                    <td>{user.role}</td>

                                </tr>
                            })}

                        </tbody>

                    </Table>

                    <TablePagination

                        className="bg-dark text-light"
                        component="table"
                        count={100}
                        page={page}
                        onChangePage={handleChangePage}
                        rowsPerPage={rowsPerPage}
                        onChangeRowsPerPage={handleChangeRowsPerPage}
                    />
                </div> : <Alert className="col-lg-5 ml-auto mr-auto" variant="danger">There is no user</Alert>}

        </Container>
    </div>
}
export default UserList