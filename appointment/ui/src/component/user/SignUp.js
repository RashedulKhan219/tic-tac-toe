import React,{ useState,useEffect } from 'react'
import { Form,Button,Container,Alert,Row,Col } from 'react-bootstrap'
import UserApi from '../api/UserApi';
import Navigation from '../home/Navigation';
import { useHistory,Link } from 'react-router-dom';
import CONSTANT from '../utils/CONSTANT';

const SignUp = () => {
    const [firstName,setFirstName] = useState();
    const [lastName,setLastName] = useState();
    const [email,setEmail] = useState();
    const [phone,setPhone] = useState();
    const [password,setPassword] = useState();
    const history = useHistory();
    const [alert,setAlert] = useState(false);
    const [messege,setMessege] = useState('');
    const [type,setType] = useState("password");
    const [logout,setLogout] = useState(false)
    const [user,setUser] = useState();
    const [showPassword,setShowPasssword] = useState("Show passowrd")


    useEffect(() => {
        const localUser = localStorage.getItem(CONSTANT.LOCAL_STORAGE.USER);
        const logout = localStorage.getItem(CONSTANT.LOCAL_STORAGE.LOGOUT);
        if (localUser && logout) {
            setUser(JSON.parse(localUser));
            setLogout(true);
        }
    },[])


    const handleSubmit = (event) => {
        event.preventDefault()
        UserApi.createUser(firstName,lastName,phone,email,password).then(user => {
            localStorage.setItem(CONSTANT.LOCAL_STORAGE.USER,JSON.stringify(user));
            localStorage.setItem(CONSTANT.LOCAL_STORAGE.LOGOUT,JSON.stringify(false));
            localStorage.setItem(CONSTANT.LOCAL_STORAGE.USERPASSCHANGED,JSON.stringify(false));
            if (user && user.role === 'USER') {
                history.push("/userresponse/" + user.userId)
            }
        }).catch(error => {
            setAlert(true);
        })
    }


    const handlePhoneNumber = (event) => {
        let phone = event.target.value;
        phone = phone.replace(/[^\d]/g,"");

        //check if number length equals to 10
        if (phone.length > 11) {
            //reformat and return phone number
            return setMessege("Phone number is not valid.");
        }

        var cleaned = ('' + phone).replace(/\D/g,'');
        var match = cleaned.match(/^(1|)?(\d{3})(\d{3})(\d{4})$/);
        if (match) {
            var intlCode = (match[1] ? '+1 ' : '');
            return setPhone([intlCode,'(',match[2],') ',match[3],'-',match[4]].join(''));
        }
    }


    const handleShowPassword = (event) => {
        const x = document.getElementById("myPassword");
        if (x.type === "password") {
            setType("text");
            setShowPasssword('Hide password')
        } else {
            setType("password");
            setShowPasssword("Show password");
        }

    }

    if (!logout && user) {
        if (user.role === 'BARBER') {
            history.push("/hairdresserprofile/" + user.barberId)
        } else if (user.role === 'ADMIN') {
            history.push("/businessprofile/" + user.barberId)
        } else if (user.role === 'USER') {
            history.push("/userresponse/" + user.userId);
        }
    }
    if (logout && user) {
        if (user.role === 'BARBER') {
            history.push("/businesshome")
        } else if (user.role === 'ADMIN') {
            history.push("/businesshome")
        } else if (user.role === 'USER') {
            history.push("/login");
        }
    }

    return <div >
        <Navigation />
        <Container className="mt-3 pb-5">
            <Row >
                <Col md={5} className=" ml-auto mr-auto shadow-lg p-5 rounded" >
                    <h2 className="p-2">SignUp</h2>
                    <Form onSubmit={handleSubmit} className="text-left">
                        <Form.Label>FirstName</Form.Label>
                        <Form.Control type="text" placeholder="FirstName" required className="mb-2"
                            onChange={(e) => setFirstName(e.target.value)} />
                        <Form.Label>LastName</Form.Label>
                        <Form.Control type="text" placeholder="LastName" required className="mb-2"
                            onChange={(e) => setLastName(e.target.value)} />
                        <Form.Label>Phone</Form.Label>
                        <Form.Control type="text" placeholder="Phone" pattern="[0-9]*" required className="mb-2"
                            onChange={handlePhoneNumber} />
                        {messege ? <Alert className="text-warning" dismissible onClose={() => setMessege(null)}>{messege}</Alert> : null}
                        <Form.Label>Email</Form.Label>
                        <Form.Control type="email" placeholder="Email" required className="mb-2"
                            onChange={(e) => setEmail(e.target.value.toLowerCase())} />
                        <Form.Label>Password</Form.Label>
                        <Form.Control type={type && type} placeholder="Password" required className="mb-3" maxLength={15} minLength={5}
                            onChange={(e) => setPassword(e.target.value)} id="myPassword" />
                        <Form.Group className="text-left" controlId="formBasicCheckbox">
                            <Form.Check onClick={handleShowPassword} type="checkbox" label={showPassword && showPassword} />
                        </Form.Group>

                        {alert && <Alert variant="danger" onClose={() => setAlert(false)} dismissible>
                            <p>Already have account. Can not create</p>
                        </Alert>}
                        <Button type="submit" className="col-lg mb-3" style={{ backgroundColor: 'purple' }}>
                            Submit
                        </Button>

                        <p className="text-center"> Already have account?<Link className="text-warning" to="/login"> Login</Link></p>

                    </Form>

                </Col>

            </Row>

        </Container>

    </div>

}
export default SignUp