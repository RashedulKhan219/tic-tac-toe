import React from 'react';
import Navigation from '../home/Navigation';
import { useState } from 'react';
import EmailApi from '../api/EmailApi';
import { useHistory, useParams } from 'react-router-dom';
import { Alert,Form,Button,Spinner,Container,Col } from 'react-bootstrap';

const VerifyToken = () => {

    const [verificationCode,setVerificationCode] = useState();
    const history = useHistory();
    const [loading,setLoading] = useState(false);
    const [tokenError, setTokenError] = useState(false);
    const { email, userId} = useParams();
    const [newEmailtoken, setNewEmailToken] = useState("A verification code has been sent to your email.");


    const handleVerify = () => {
        EmailApi.verifyEmailToken(verificationCode).then(token => {
            setLoading(false);
            history.push("/changepassword/" + token.userId);
        }).catch(error => {
            setLoading(false);
            return setTokenError(true);
        })
    }

    const handleVerifyButton = (event) => {
        event.preventDefault();
        setLoading(true);
        setTimeout(handleVerify,1000);
    }

    const handlegenerateNewCode =(event)=>{
        EmailApi.getEmailTokenByUserEmail(email, userId).then( token => {
            if(token){
            setNewEmailToken("A new verification code has been sent to your email.");
            }
        })
    }

    return <div>
        <Navigation />
        <Container className='mt-5 pt-5'>
            <Col md={5} className=" ml-auto mr-auto">
              {newEmailtoken &&  <Alert  variant='success' onClose={()=> setNewEmailToken(null)} dismissible>
                  {newEmailtoken}
                  </Alert>}
                <Form onSubmit={handleVerifyButton} className="text-left mt-5">
                    <Form.Control
                        required
                        placeholder="Enter verification code"
                        onChange={(e) => setVerificationCode(e.target.value)}
                        className="mb-4"
                    />
                    <div className="text-right">
                        <Button  className="mr-2 text-light" variant="outline-secondary"
                        onClick={ handlegenerateNewCode}>
                            Genarate a new code
                        </Button>
                    <Button type='submit'>
                        Submit
                             {loading && <Spinner animation='border' size='sm' variant='light' />}
                    </Button>
                    </div>
                    {tokenError && <Alert variant="danger" className="mt-5" dismissible onClose={()=> setTokenError(false)}>
                        Invalid verificationCode...!
                        </Alert>}
                </Form>

            </Col>

        </Container>


    </div>

}

export default VerifyToken