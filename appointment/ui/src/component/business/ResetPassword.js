import React,{ useState } from 'react'
import Navigation from '../home/Navigation'
import { Form,Button,Spinner,Col,Container, Alert } from 'react-bootstrap';
import { useParams } from 'react-router-dom';
import { useHistory } from 'react-router-dom'
import CONSTANT from '../utils/CONSTANT';
import { useEffect } from 'react';
import BarberApi from '../api/BarberApi';

const ResetPassword = () => {
    const [loading,setLoading] = useState(false);
    const [password,setPassword] = useState();
    const [confirmPassword,setConfirmPassword] = useState();
    const { barberId } = useParams();
    const history = useHistory();
    const [passChanged,setPassChanged] = useState(false);
    const [error, setError] = useState(false);
    const [currentType, setCurrentType] = useState('password');



    useEffect(() => {
        const savePassChanged = localStorage.getItem(CONSTANT.LOCAL_STORAGE.BUSPASSCHANGED);
        if (savePassChanged) {
            setPassChanged(JSON.parse(savePassChanged));
        }

    },[])

    const handleChangePassword = () => {
            BarberApi.resetPassword(password,barberId).then(user => {
                localStorage.setItem(CONSTANT.LOCAL_STORAGE.BUSPASSCHANGED,JSON.stringify(true))
                setLoading(false);
                history.push("/businesshome")
            })
    }

    const handleSubmitButton = (event) => {
        event.preventDefault();
        setLoading(true)
        if (password === confirmPassword) {
        setTimeout(handleChangePassword,1000)
        } else  if (password !== confirmPassword){
            setLoading(false);
            setError(true);
        }
    }

    const handleShowPassword = () => {
        if(currentType === 'password'){
            setCurrentType('text');
        } else {
            setCurrentType('password');
        }
    }

    if (passChanged) {
        history.push("/businesshome")
    }

    return <div>
        <Navigation />
        <Container className="mt-5 pt-5">

            <Col md={5} className="ml-auto mr-auto text-left">
                <Form onSubmit={handleSubmitButton}>
                    <Form.Label>Password</Form.Label>
                    <Form.Control
                        required
                        type={currentType && currentType}
                        placeholder="Enter new password"
                        onChange={(e) => setPassword(e.target.value)}
                        className='mb-3'
                        maxLength={15}
                        minLength={5}
                        
                    />
                    <Form.Label>Confirm-password</Form.Label>
                    <Form.Control
                        required
                        type={currentType && currentType}
                        placeholder="confirm - password"
                        onChange={(e) => setConfirmPassword(e.target.value)}
                        className='mb-3'
                        maxLength={15}
                        minLength={5}
                    />
                    <Form.Group className="text-left" controlId="formBasicCheckbox">
                        <Form.Check onClick={handleShowPassword} currentType="checkbox" label="Show password" />
                    </Form.Group>
                    <Button type='submit'>
                        submit
                    {loading && <Spinner animation="border" size="sm" variant='light' />}
                    </Button>

                    {error && <Alert variant="danger" className="mt-4" dismissible onClose={()=> setError(false)}>
                        Password doesn't match..!
                        </Alert>}
                </Form>

            </Col>
        </Container>
    </div>

}

export default ResetPassword