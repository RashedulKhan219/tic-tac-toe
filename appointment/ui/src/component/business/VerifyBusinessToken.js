import React from 'react';
import Navigation from '../home/Navigation';
import { useState } from 'react';
import EmailApi from '../api/EmailApi';
import { useHistory, useParams } from 'react-router-dom';
import { Alert,Form,Button,Spinner,Container,Col } from 'react-bootstrap';

const VerifyBusinessToken = () => {

    const [verificationCode,setVerificationCode] = useState();
    const history = useHistory();
    const {email, barberId} = useParams();
    const [loading,setLoading] = useState(false);
    const [tokenError, setTokenError] = useState(false);
    const [message, setMessage] = useState("A verification code has been sent to your email.") 

    const handleVerify = () => {
        setLoading(false)
        EmailApi.verifyEmailToken(verificationCode).then(token => {
            setLoading(false);
            history.push("/resetpassword/" + token.userId)
        }).catch(error =>{
            setLoading(false);
            setTokenError(true);
        })
    }
    const handleVerifyButton = (event) => {
        event.preventDefault();
        setLoading(true);
        setTimeout(handleVerify,1000);

    }

    const handlegenerateNewCode =()=>{
        EmailApi.getEmailTokenByUserEmail(email, barberId).then( token => {
            if(token){
                setMessage("A new verification code has been sent to your email.")
            }
        })
    }

    return <div>
        <Navigation />
        <Container className='mt-5 pt-5'>
            <Col md={5} className=" ml-auto mr-auto">
             {message &&  <Alert variant='success' onClose={()=> setMessage(null)} dismissible>{message}</Alert>}
                <Form onSubmit={handleVerifyButton} className="text-left mt-5">
                    <Form.Control
                        required
                        placeholder="Enter verification code"
                        onChange={(e) => setVerificationCode(e.target.value)}
                        className="mb-4"
                    />
                 <div className="text-right">
                        <Button  className="mr-2 text-light" variant="outline-secondary"
                        onClick={ handlegenerateNewCode}>
                            Genarate a new code
                        </Button>
                    <Button type='submit'>
                        Submit
                             {loading && <Spinner animation='border' size='sm' variant='light' />}
                    </Button>
                    </div>
                    {tokenError && <Alert variant="danger" className="mt-5" dismissible onClose={()=> setTokenError(false)}>
                    Invalid verificationCode...!
                        </Alert>}

                </Form>

            </Col>

        </Container>


    </div>

}

export default VerifyBusinessToken