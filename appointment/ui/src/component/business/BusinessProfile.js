import React,{ useEffect,useState } from 'react';
import { Container,Form,Modal,Row,Col,Button,Spinner,Alert } from 'react-bootstrap';
import { useHistory,useParams } from 'react-router';
import BarberApi from '../api/BarberApi';
import Navigation from '../home/Navigation';
import CONSTANT from '../utils/CONSTANT';


const BusinessProfile = () => {
    const { barberId } = useParams()
    const [barber,setBarber] = useState();
    const history = useHistory();
    const [isLogout,setLogout] = useState(false);
    const [barbers,setBarbers] = useState([]);
    const [show,setShow] = useState(false);
    const [loading,setLoading] = useState(false);
    const [barberId2,setBarberId2] = useState();
    const [deletedBarber,setDeletdBarber] = useState();

    useEffect(() => {
        const logout = localStorage.getItem(CONSTANT.LOCAL_STORAGE.LOGOUT);
        if (logout) {
            setLogout(JSON.parse(logout))
        }
        BarberApi.getBarberById(barberId).then(barber => setBarber(barber))
        BarberApi.getAllBarber().then(barbers => setBarbers(barbers))
    },[barberId])


    const handleUpdate = () => {
        history.push("/updatebarber/" + barberId);
    }

    const handleDeleteBarber = () => {
        setShow(false);
        BarberApi.deleteBarberProfile(barberId2).then(deleted => {
            if (deleted) {
                setDeletdBarber(deleted);
                setLoading(false);
                BarberApi.getAllBarber().then(barbers => setBarbers(barbers))
            }
        })
    }

    const handleDelete = (event) => {
        event.preventDefault();
        setLoading(true);
        handleClose();
        setTimeout(handleDeleteBarber,1000);
    }

    const handleShow = (event) => {
        event.preventDefault();
        setShow(true);
    }

    const handleClose = () => {
        setShow(false);
    }

    if (isLogout) {
        history.push("/businesshome");
    }

    return <div>
        <Navigation />
        <Container className="mt-5">
            <h1>Welcome to Merry Hair Cutting</h1>

            <Row className="mt-5">

                {barber && <Col md={6} sm={6} className="text-left mb-5" >
                    <h2>User Information</h2>
                    <p>FirstName: {barber.firstName}</p>
                    <p>LastName: {barber.lastName}</p>
                    <p>Phone: {barber.phone}</p>
                    <p>Email: {barber.email}</p>
                    <Button onClick={handleUpdate} variant="outline-primary text-light" >
                        Update info
                        </Button>
                </Col>}
                {barbers.length > 1 ? <Col md={6} className="text-left mb-5" >
                    <Form onSubmit={handleShow}>
                        <h2>Hair-dresserList</h2>
                        {barbers.map((barber,index) => {
                            return barber.role !== 'ADMIN' && <Form.Check

                                type="radio"
                                label={barber.firstName + " " + barber.lastName}
                                name="formVerticalRadios"
                                id="formVerticalRadios1"
                                value={barber.barberId}
                                required
                                className="mb-3"
                                onChange={(e) => setBarberId2(e.target.value)}
                            />
                        })}

                        <Button type="submit" variant="danger">
                            Delete hair-dresser
                        {loading && <Spinner animation="border" size="sm" variant="light" />}
                        </Button>

                        {deletedBarber && <Alert className="text-success" dismissible onClose={() => setDeletdBarber(null)}>
                            Successfully deleted.
                        </Alert>}
                    </Form>
                </Col> : <Col md={6} className="text-left mb-5" >

                    <h3>There is no hair dresser available</h3>
                </Col>}

            </Row>

        </Container>

        <Modal show={show} bg="dark" onHide={handleClose}>
            <Modal.Header closeButton>
                <Modal.Title>Delete Hair-dresser</Modal.Title>
            </Modal.Header>
            <Modal.Body>Are you sure you want to delete?</Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={handleClose}>
                    No
          </Button>
                <Button variant="danger" onClick={handleDelete}>
                    Yes, Delete
          </Button>
            </Modal.Footer>
        </Modal>
    </div>
}
export default BusinessProfile