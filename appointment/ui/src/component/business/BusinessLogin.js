import React,{ useState,useEffect } from 'react'
import { Form,Button,Alert,Row,Col } from 'react-bootstrap'
import { useHistory,Link } from 'react-router-dom';
import CONSTANT from '../utils/CONSTANT';
import BarberApi from '../api/BarberApi';

const BusinessLogin = () => {

    const [email,setEmail] = useState();
    const [password,setPassword] = useState();
    const history = useHistory();
    const [message,setMessage] = useState(false);
    const [logout,setLogout] = useState(true)
    const [localbarber,setLocalbarber] = useState();
    const [passChanged,setPassChanged] = useState(false);
    const [type,setType] = useState("password");
    const [showPassword, setShowPasssword] = useState("Show passowrd")



    useEffect(() => {
        const saveLogout = localStorage.getItem(CONSTANT.LOCAL_STORAGE.LOGOUT);
        const savebarber = localStorage.getItem(CONSTANT.LOCAL_STORAGE.USER);
        const passChanged = localStorage.getItem(CONSTANT.LOCAL_STORAGE.BUSPASSCHANGED);
        if (saveLogout && savebarber && passChanged) {
            setLogout(JSON.parse(saveLogout));
            setLocalbarber(JSON.parse(savebarber));
            setPassChanged(JSON.parse(passChanged));
        }
    },[])


    const handleLogin = (event) => {
        event.preventDefault();
        BarberApi.login(email,password).then(barber => {
            localStorage.setItem(CONSTANT.LOCAL_STORAGE.USER,JSON.stringify(barber));
            localStorage.setItem(CONSTANT.LOCAL_STORAGE.LOGOUT,JSON.stringify(false));
            localStorage.setItem(CONSTANT.LOCAL_STORAGE.BUSPASSCHANGED,JSON.stringify(false));
            if (barber && barber.role === 'ADMIN') {
                return history.push("/businessprofile/" + barber.barberId)
            } else if (barber && barber.role === 'BARBER') {
                return history.push("/hairdresserprofile/" + barber.barberId)
            } 
        }).catch(error =>{
            setMessage(" The email or password you entered doesn't match with our records!")
        })
    }



    const handleClose = () => {
        setPassChanged(false);
        localStorage.setItem(CONSTANT.LOCAL_STORAGE.BUSPASSCHANGED,JSON.stringify(false));
    }


    const handleShowPassword = (event) => {
        const value = document.getElementById("myPassword");
        console.log(value)
        if (value.type === "password") {
            setType("text");
            setShowPasssword("Hide password");
        } else {
            setType("password");
            setShowPasssword("Show password");
        }

    }

    if (!logout && localbarber) {
        history.push("/businesslogin/" + localbarber.barberId);
    }

    return <div >

        <Form className="shadow-lg rounded p-3" onSubmit={handleLogin}>
            Business use only
            <h2>Login </h2>
            <Form.Group as={Row} controlId="formHorizontalEmail"  >
                <Col sm={6} md={8} className="ml-auto mr-auto text-left">
                    <Form.Label >
                        Email
                    </Form.Label>
                    <Form.Control type="email" placeholder="Enter email-address" required
                        className="mb-2" onChange={(e) => setEmail(e.target.value.toLowerCase())} />
                </Col>
            </Form.Group>

            <Form.Group as={Row} controlId="formHorizontalPassword">
                <Col sm={6} md={8} className="ml-auto mr-auto text-left">
                    <Form.Label >
                        Password
                        </Form.Label>
                    <Form.Control type={type && type} placeholder="Enter password" required maxLength={15} minLength={5}
                        className="mb-2" onChange={(e) => setPassword(e.target.value)} id="myPassword" />
                    <div className="text-right" >
                        <Link style={{ color: 'cyan' }} to="/businessforgetpassword">ForgetPassword?</Link>
                    </div>
                    
                </Col>
            </Form.Group>
         
            <Form.Group as={Row} >
                <Col sm={6} md={8} className="ml-auto mr-auto text-left">
                <Form.Group className="text-left" controlId="formBasicCheckbox">
                        <Form.Check 
                        type="checkbox" 
                        label={showPassword && showPassword} 
                        onClick={handleShowPassword} />
                    </Form.Group>
                    <Button type="submit" >Login</Button>
                </Col>
            </Form.Group>

            <Form.Group as={Row}>
                <Col sm={6} className="ml-auto mr-auto text-center">
                    {message && <Alert variant="danger" dismissible className="text-left"
                        onClose={() => setMessage(false)} >
                        <p>
                            {message}
                            </p>
                    </Alert>}
                </Col>
            </Form.Group>

        </Form>

        <Col className="ml-auto mr-auto mt-3">
            {passChanged && <Alert variant="success" dismissible onClose={handleClose} >
                Password has changed successfully. login with new password.
                </Alert>}
        </Col>

        <div>

        </div>

    </div>

}
export default BusinessLogin