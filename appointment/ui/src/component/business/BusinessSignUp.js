import React,{ useEffect,useState } from 'react'
import { Form,Button,Alert,Col } from 'react-bootstrap'
import { useHistory } from 'react-router-dom';
import CONSTANT from '../utils/CONSTANT';
import BarberApi from '../api/BarberApi';


const BusinessSignUp = () => {
    const [firstName,setFirstName] = useState();
    const [lastName,setLastName] = useState();
    const [email,setEmail] = useState();
    const [phone,setPhone] = useState();
    const [password,setPassword] = useState();
    const [role,setRole] = useState();
    const history = useHistory();
    const [message,setMessage] = useState(false);
    const [messege,setMessege] = useState('');
    const [logout,setLogout] = useState(true)
    const [localbarber,setLocalbarber] = useState();
    const [showPassword, setShowPasssword] = useState("Show password");
    const [currentType, setCurrentType] = useState("password");
    


    useEffect(() => {
        const saveLogout = localStorage.getItem(CONSTANT.LOCAL_STORAGE.LOGOUT);
        const savebarber = localStorage.getItem(CONSTANT.LOCAL_STORAGE.USER);
        if (saveLogout && savebarber) {
            setLogout(JSON.parse(saveLogout));
            setLocalbarber(JSON.parse(savebarber));
        }
    },[])

    const handleSubmit = (event) => {
        event.preventDefault()
        BarberApi.createBarberProfile(firstName,lastName,phone,email,password,role).then(barber => {
            localStorage.setItem(CONSTANT.LOCAL_STORAGE.USER,JSON.stringify(barber));
            localStorage.setItem(CONSTANT.LOCAL_STORAGE.LOGOUT,JSON.stringify(false));
            localStorage.setItem(CONSTANT.LOCAL_STORAGE.BUSPASSCHANGED,JSON.stringify(false));
            if (barber && barber.role === 'BARBER') {
                return history.push("/hairdresserprofile/" + barber.barberId)
            } else if (barber && barber.role === 'ADMIN') {
                return history.push("/businessprofile/" + barber.barberId)
            }
        }).catch(error =>{
            setMessage("Already have acccount. can not create.")
        })
    }

    const handlePhoneNumber = (event) => {
        let phone = event.target.value;
        phone = phone.replace(/[^\d]/g,"");

        //check if number length equals to 10
        if (phone.length > 11) {
            //reformat and return phone number
            return setMessege("Phone number is not valid.");
        }

        var cleaned = ('' + phone).replace(/\D/g,'');
        var match = cleaned.match(/^(1|)?(\d{3})(\d{3})(\d{4})$/);
        if (match) {
            var intlCode = (match[1] ? '+1 ' : '');
            return setPhone([intlCode,'(',match[2],') ',match[3],'-',match[4]].join(''));
        }
    }

    const handleShowPassword = () => {
      
        if ( currentType === "password") {
            setCurrentType("text");
            setShowPasssword("Hide password");
        } else {
            setCurrentType("password");
            setShowPasssword("Show password");
        }

    }

    if (!logout && localbarber) {
        history.push("/businesslogin/" + localbarber.barberId);
    }

    return <div >
        <Form onSubmit={handleSubmit} className="shadow-lg rounded p-3">
            Business use only
            <h2>Register</h2>
            <Form.Row className="text-left">
                <Form.Group as={Col} >
                    <Form.Label>FirstName</Form.Label>
                    <Form.Control type="text" placeholder="FirstName" required className="mb-3"
                        onChange={(e) => setFirstName(e.target.value)} />
                </Form.Group>

                <Form.Group as={Col} >
                    <Form.Label>LastName</Form.Label>
                    <Form.Control type="text" placeholder="LastName" required className="mb-3"
                        onChange={(e) => setLastName(e.target.value)} />
                </Form.Group>
            </Form.Row>

            <Form.Row className="text-left">
                <Form.Group as={Col} controlId="formGridEmail" >
                    <Form.Label>Email</Form.Label>
                    {messege ? <Alert dismissible onClose={() => setMessege(null)}>{messege}</Alert> : null}
                    <Form.Control type="email" placeholder="Enter business email" required className="mb-3"
                        onChange={(e) => setEmail(e.target.value.toLowerCase())} />
                </Form.Group>
                <Form.Group as={Col} >
                    <Form.Label>Phone</Form.Label>
                    <Form.Control type="text" placeholder="Phone number" pattern="[0-9]*" required className="mb-3"
                        onChange={handlePhoneNumber} />
                </Form.Group>


            </Form.Row>
            <Form.Row className="text-left">
                <Form.Group as={Col} controlId="formGridPassword">
                    <Form.Label>Password</Form.Label>
                    <Form.Control type={currentType && currentType} placeholder="Password" required className="mb-3" maxLength={15} minLength={5}
                        onChange={(e) => setPassword(e.target.value)} id="myPassword" />
                </Form.Group>

                <Form.Group as={Col} controlId="formBasicCheckbox">
                    <Form.Label>Confirm your role.</Form.Label>
                    <Form.Check type="radio" required label="Business owner" value='ADMIN' name="formHorizontalRadios1"
                        id="formHorizontalRadios43" onChange={(e) => setRole(e.target.value)} />
                    <Form.Check type="radio" required label="Hair-dresser" value='BARBER' name="formHorizontalRadios1"
                        id="formHorizontalRadios4" onChange={(e) => setRole(e.target.value)} />
                </Form.Group>
                
            </Form.Row>
            <Form.Row>
            <Form.Group className="text-left" controlId="formBasicCheckbox">
                        <Form.Check onClick={handleShowPassword} type="checkbox" label={showPassword && showPassword} />
                    </Form.Group>
            </Form.Row>


           


            {message && <Alert variant="danger" onClose={() => setMessage(null)} dismissible>
                <p>{message}</p>
            </Alert>}
            <Button type="submit" className="col-lg mb-3" >

                Submit</Button>

        </Form>

    </div>

}
export default BusinessSignUp