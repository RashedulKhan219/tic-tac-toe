import React,{useState, useEffect} from 'react'
import { Col,Container,Row } from 'react-bootstrap'
import { useHistory } from 'react-router'
import Navigation from '../home/Navigation'
import CONSTANT from '../utils/CONSTANT'
import BusinessLogin from './BusinessLogin'
import BusinessSignUp from './BusinessSignUp'

const BusinessHome = () => {


    const [logout,setLogout] = useState(true)
    const [localbarber,setLocalbarber] = useState()
    const history = useHistory();
    

    useEffect(()=>{
        const saveLogout = localStorage.getItem(CONSTANT.LOCAL_STORAGE.LOGOUT);
        const savebarber = localStorage.getItem(CONSTANT.LOCAL_STORAGE.USER);
        if (saveLogout && savebarber) {
            setLogout(JSON.parse(saveLogout));
            setLocalbarber(JSON.parse(savebarber));
        }
    },[])

    if (!logout && localbarber) {
        if(localbarber.role === 'BARBER'){
        history.push("/hairdresserprofile/" + localbarber.barberId);
        } else if(localbarber.role === 'ADMIN'){
            history.push("/businessprofile/" + localbarber.barberId);
        } else if(localbarber.role === 'USER') {
            history.push("/userresponse/" + localbarber.userId);
        }
    }

    if(logout && localbarber){
        if(localbarber.role === 'USER'){
            history.push("/login")
        } else {
            history.push("/businesshome");
        }
        
    }


    return <div>
        <Navigation />
        <Container className="mt-5 pb-5">
            <h3>If you already have an account just login else resigter  </h3>

            <Row>
                <Col md={6} className="ml-auto mr-auto mb-5">
                    <BusinessLogin />
                </Col>

                <Col md={6} className="ml-auto mr-auto">
                    <BusinessSignUp />
                </Col>
            </Row>
        </Container>


    </div>
}
export default BusinessHome