import React from 'react'
import Navigation from '../home/Navigation'
import { Form,Button,Spinner,Container,Col,Alert } from 'react-bootstrap'
import { useState } from 'react'
import { useHistory } from 'react-router-dom'
import EmailApi from '../api/EmailApi'
import BarberApi from '../api/BarberApi'
import CONSTANT from '../utils/CONSTANT'


const BusinessForgetPass = () => {
    const [loading,setLoading] = useState(false);
    const history = useHistory();
    const [email,setEmail] = useState();
    const [message,setMeesage] = useState();



    const handleNextButton = (event) => {
        event.preventDefault();
        setLoading(true);

        BarberApi.validateEmailToGetToken(email).then(barber => {
            EmailApi.getEmailTokenByUserEmail(email,barber.barberId).then(token => {
                localStorage.setItem(CONSTANT.LOCAL_STORAGE.BUSPASSCHANGED,JSON.stringify(false));
                setLoading(false)
                history.push("/verifybusinesstoken/" + email + "/" + barber.barberId);
            })

        }).catch(error => {
            setLoading(false)
          setMeesage("Email doesn't exist");
        })

    }


    const handleCancelButton =()=>{
        history.push("/businesshome")
    }
    return <div>
        <Navigation />
        <Container className="mt-5 pt-5">
            <p>Please enter your email to verify</p>
            <Col md={5} className=" ml-auto mr-auto text-left">
                <Form onSubmit={handleNextButton}>
                    <Form.Control
                        type="email"
                        placeholder="Enter email-address"
                        required
                        onChange={(e) => setEmail(e.target.value.toLowerCase())}
                    />
                    <div className="text-right">
                    <Button variant='secondary' onClick={handleCancelButton} className='mt-3 mr-2'>
                          Cancel
                    </Button>
                    <Button type="submit" className='mt-3'>
                            Search
                            {loading && <Spinner animation='border' size='sm' variant='light' />}
                    </Button>
                    </div>
                    {message && <Alert className="text-warning" dismissible onClose={() => setMeesage(null)}>
                        {message}
                        </Alert>}

                </Form>
            </Col>

        </Container>
    </div>

}

export default BusinessForgetPass