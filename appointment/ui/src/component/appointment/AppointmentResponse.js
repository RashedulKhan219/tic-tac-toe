import React,{ useEffect,useState } from 'react'
import { useParams,useHistory } from 'react-router-dom'
import AppointmentApi from '../api/AppointmentApi';
import Navigation from '../home/Navigation';
import { Container} from 'react-bootstrap';
import { Card,Button,Image } from 'semantic-ui-react';
import UserApi from '../api/UserApi';
import CONSTANT from '../utils/CONSTANT';
import ReactTimeAgo from 'react-time-ago/commonjs/ReactTimeAgo';
import picture from '../images/appointment.png'

const AppointmentResponse = () => {

    const { appointmentId,userId } = useParams();
    const [appointment,setAppointment] = useState();
    const [user,setUser] = useState([]);
    const history = useHistory();
    const [logout,setLogout] = useState(false);


    useEffect(() => {
        const saveLogout = localStorage.getItem(CONSTANT.LOCAL_STORAGE.LOGOUT);
        if (saveLogout) {
            setLogout(JSON.parse(saveLogout));
        }
        AppointmentApi.getAppointmentById(appointmentId).then(appointment => setAppointment(appointment));
        UserApi.getUserById(userId).then(user => setUser(user));
    },[userId,appointmentId])
   

    const handleFinish = () => {
        history.push("/userresponse/" + userId);
    }

    if (logout) {
        history.push("/login")
    }


    return <div>
        <Navigation />

        <Container>
            <div className="row mt-5 pb-5">
                {appointment && <Card className="ml-auto mr-auto text-left" >
                    <Card.Content>
                        <Image
                            floated='right'
                            size='tiny'
                            src={picture}
                        />
                        <Card.Header> Appointment information</Card.Header>
                        <Card.Meta>
                            Appointment scheduled <ReactTimeAgo date={appointment.localDateTime} locale="en-US" />
                        </Card.Meta>
                        <Card.Description >
                            {user && <p className="text-capitalize">Name: {user.firstName + " " + user.lastName}</p>}
                            <p>Appointment-ID: {appointment.appointmentId}</p>
                            <p>Date: {appointment.date}</p>
                            <p>Time: {appointment.time}</p>
                            <p>Phone: {user.phone}</p>
                            <p>Email: {user.email}</p>
                            <p>Hair-dresser: {appointment.barberName} </p>
                            <p>Services: {appointment.services} </p>
                        </Card.Description>
                    </Card.Content>
                    <Card.Content extra>
                        <div className='ui two buttons'>
                            <Button color='blue' disabled={logout} onClick={handleFinish}>
                                Finish       
                            </Button>
                        </div>
                        <p className="text-dark text-center mt-2">Now click on Finish</p>
                    </Card.Content>
                </Card>}
            </div>

         

        </Container>

    </div>

}

export default AppointmentResponse