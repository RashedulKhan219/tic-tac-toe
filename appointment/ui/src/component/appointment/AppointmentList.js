import React,{ useEffect,useState } from 'react'
import AppointmentApi from '../api/AppointmentApi'
import { Container,Table,Alert} from 'react-bootstrap';
//import { Input } from 'semantic-ui-react'
import TablePagination from '@material-ui/core/TablePagination';
import ReactTimeAgo from 'react-time-ago'
import Navigation from '../home/Navigation';
import CONSTANT from '../utils/CONSTANT';
import { useHistory } from 'react-router-dom';
import BarberApi from '../api/BarberApi';

const AppointmentList = () => {

    const [allAppointment,setAllAppointment] = useState([]);
    const [search,setSearch] = useState('');
    const [isLogout,setIsLogout] = useState(false);
    const history = useHistory();
    const [allBarber,setAllBarber] = useState([]);



    useEffect(() => {
        const logout = localStorage.getItem(CONSTANT.LOCAL_STORAGE.LOGOUT)
        if (logout) {
            setIsLogout(JSON.parse(logout))
        }
        AppointmentApi.getAllAppointment().then(appointments => {
            setAllAppointment(appointments)
        })
        BarberApi.getAllBarber().then(all => setAllBarber(all))
    },[])


    const [page,setPage] = React.useState(0);
    const [rowsPerPage,setRowsPerPage] = React.useState(10);

    const handleChangePage = (event,newPage) => {
        setPage(newPage);
    }

    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(parseInt(event.target.value,10));
        setPage(3);
    }


   
    if (isLogout) history.push("/businesshome")


    return <div>
        <Navigation />
        <Container className="mt-5 pb-5">
          
            <h2>AppointmentList</h2>
            {/* {allAppointment.length !== 0 && <div className='mb-3 text-light text-left'>
                <Input size='mini'
                    iconPosition='right'
                    labelPosition='left'
                    label='Find user....'
                    type='text' icon='search'
                    onChange={(e) => setSearch(e.target.value)} />
            </div>} */}

            {allAppointment.length !== 0 ? <div>
                <Table striped bordered hover variant="dark">
                    <thead>
                        <tr>
                            <th>Appointment-ID</th>
                            <th>Email</th>
                            <th>Gender</th>
                            <th>Date</th>
                            <th>Time</th>
                            <th>Hair-dresser</th>
                            <th>Appointment taken</th>
                           

                        </tr>
                    </thead>
                    <tbody >

                    {allAppointment.filter(appointment => {
                        if (search === '') {
                            return appointment;
                        } else if (appointment.gender.toLowerCase().includes(search.toLowerCase())
                            || appointment.barber.toLowerCase().includes(search.toLowerCase())
                            || appointment.email.toLowerCase().includes(search.toLowerCase())
                        ) {
                            return appointment;
                        } else {
                            return null;
                        }
                    }).slice(page * rowsPerPage,page * rowsPerPage + rowsPerPage).map((appointment,index) => {
                        return <tr key={index}>
                                    <td>{appointment.appointmentId}</td>
                                    <td>{appointment.email}</td>
                                    <td >{appointment.gender}</td>
                                    <td>{appointment.date}</td>
                                    <td>{appointment.time}</td>
                                    <td >{appointment.barberName}</td>
                                    <td >{
                                        <ReactTimeAgo date={appointment.localDateTime} locale="en-US" />
                                    } </td>
                                
                                </tr>
                    })}
                     </tbody>

                </Table>

                <TablePagination
                    className="bg-dark text-light"
                    component="table"
                    count={100}
                    page={page}
                    onChangePage={handleChangePage}
                    rowsPerPage={rowsPerPage}
                    onChangeRowsPerPage={handleChangeRowsPerPage}
                />


            </div> : <Alert variant="warning" className="col-lg-5 ml-auto mr-auto">
                Appointment hasn't been scheduled yet
                    </Alert>}

                {allBarber && allBarber.map((barber,index) => {
                return <p>{barber.barberName}</p>
            })}
           

           

        </Container>

    </div>
}

export default AppointmentList