import React,{ useState,useEffect } from 'react'
import { useParams,useHistory } from 'react-router-dom'
import Navigation from '../home/Navigation';
import { Container,Form,Spinner,Alert,Col,Row } from 'react-bootstrap';
import AppointmentApi from '../api/AppointmentApi';
import { Button } from 'semantic-ui-react'
import CONSTANT from '../utils/CONSTANT';
import TimeApi from '../api/TimeApi';
import UserApi from '../api/UserApi';
import BarberApi from '../api/BarberApi';


const CreateAppointment = () => {

    const { userId } = useParams();
    const [date,setDate] = useState();
    const [time,setTime] = useState();
    const [timeId,setTimeId] = useState();
    const [gender,setGender] = useState();
    const [barberId,setBarberId] = useState();
    const history = useHistory();
    const [loading,setLoading] = useState(false);
    const [barberName,setBarberName] = useState();
    const [logout,setLogout] = useState(false);
    const [user,setUser] = useState();
    const [times,setTimes] = useState([]);
    const [message,setMessage] = useState();
    const [barbers,setBarbers] = useState([]);
    const [appointment,setAppointment] = useState();
    const [services,setServices] = useState("");



    useEffect(() => {
        const saveLogout = localStorage.getItem(CONSTANT.LOCAL_STORAGE.LOGOUT);
        if (saveLogout) {
            setLogout(JSON.parse(saveLogout));
        }
        UserApi.getUserById(userId).then(user => setUser(user));
        BarberApi.getAllBarber().then(barbers => setBarbers(barbers))
        TimeApi.getAllTimes().then(times => setTimes(times))
        AppointmentApi.getAppointmentByUserId(userId).then(appointment => setAppointment(appointment));
    },[userId])


    const handleCreateAppointment = () => {
        if (time && gender && barberName && date) {
            AppointmentApi.createAppointment(date,time,timeId,gender,barberName,barberId,user.email,services,userId).then(appointment => {
                if (appointment) {

                    TimeApi.deleteTimeByTimeId(timeId).then(deleteTime => {
                        if (deleteTime) {
                            setLoading(false);
                            return history.push("/appointmentresponse/" + appointment.appointmentId + "/" + userId);
                        }
                    })

                }
            })
        } else if (!time) {
            setMessage(true);
            return setLoading(false);
        }

    }


    const handleCreateAppointmentButton = (event) => {
        event.preventDefault();
        setLoading(true);
        setTimeout(handleCreateAppointment,1000);
    }

    const handleChangeBarber = (event) => {
        const id = event.target.getAttribute("barberId");
        setBarberId(id);
        setBarberName(event.target.value);

    }

    const handleChangeTime = (event) => {
        const getTimeId = event.target.getAttribute("timeId");
        const getDate = event.target.getAttribute("date");
        const time = event.target.value;
        setTimeId(getTimeId);
        setTime(time);
        setDate(getDate)

    }



    function handleServices() {
        const checkboxes = document.querySelectorAll(`input[name="service"]:checked`);
        let values = [];
        checkboxes.forEach((formcheck) => {
            values.push(formcheck.value);
        });
        let result = " ";
        for (let i = 0; i < values.length; i++) {
            result = result + values[i];
            if(i === values.length -1) continue;
            result = result + ", "
        }
        return setServices(result);

    }


    if (logout) {
        return history.push("/login");
    }
    

    return <div>
        <Navigation />
        <Container className="mt-5">
            <h2>Create Appointment</h2>
            <p>* indicates required fields</p>

            <Col md={7} sm={6} className="ml-auto mr-auto">
                <hr className="border" />
                <Form onSubmit={handleCreateAppointmentButton}>
                    <Form.Row>
                        <Form.Group as={Col} controlId="formGridEmail" className="text-left mr-2">
                            <h4> ✅ Select gender*</h4>
                            <Form.Check
                                type="radio"
                                label="male"
                                name="formVerticalRadios"
                                id="formVerticalRadios1"
                                value="male"
                                required
                                onChange={(e) => setGender(e.target.value)}
                            />
                            <Form.Check
                                type="radio"
                                label="kids"
                                value="kids"
                                name="formVerticalRadios"
                                id="formVerticalRadios1"
                                onChange={(e) => setGender(e.target.value)}
                            />
                            <Form.Check
                                type="radio"
                                label="other"
                                value="other"
                                name="formVerticalRadios"
                                id="formVerticalRadios1"
                                onChange={(e) => setGender(e.target.value)}
                            />
                        </Form.Group>

                        <Form.Group as={Col} className="text-left">
                            <h4> ✅ Select services*</h4>
                            <Form.Check 
                            type="checkbox" 
                            label="Hair cut" 
                            name="service" 
                            value="Hair cut" 
                            onChange={handleServices} />

                            <Form.Check 
                            type="checkbox" 
                            label="Shave" 
                            value="Shave" 
                            name="service" 
                            onChange={handleServices} />

                            <Form.Check 
                            type="checkbox" 
                            label="Hair cut & Shave" 
                            name="service" 
                            value="Hair cut & Shave" 
                            onChange={handleServices} />

                            <Form.Check 
                            type="checkbox" 
                            label="Hair cut & Color" 
                            name="service" 
                            value="Hair cut & Color" 
                            onChange={handleServices} />
                            
                            <Form.Check 
                            type="checkbox" 
                            label="Color" 
                            value="Color" 
                            name="service" 
                            onChange={handleServices} />
                        </Form.Group>
                    </Form.Row>


                    <Form.Row>
                        <Form.Group as={Col} className="text-left">
                            <h4> ✅ Select hair-dresser*</h4>
                            {barbers.length !== 0 && barbers.map((barber,index) => {
                                return barber.role === 'BARBER' && <Form.Check
                                    type="radio"
                                    key={index}
                                    required
                                    label={barber.firstName + " " + barber.lastName}
                                    value={barber.firstName + " " + barber.lastName}
                                    name="formHorizontalRadios"
                                    id="formHorizontalRadios1"
                                    barberId={barber.barberId}
                                    onChange={handleChangeBarber}
                                />
                            })}
                        </Form.Group>
                    </Form.Row>

                    <Form.Row>


                        <Form.Group as={Col} controlId="formGridState" className="text-left mt-3">
                            <p>When you choose a hair dresser, available date & time will be shown below</p>
                            {times.length !== 0 &&  barberId && <Form.Label>✅ Select Date & time*</Form.Label>}
                            {times.length !== 0 && times.map((time,index) => {
                                return barberId === time.barberId && <Form.Check
                                    className="mb-2"
                                    type="radio"
                                    key={index}
                                    required
                                    label={time.date + " & " + time.time}
                                    value={time.time}
                                    name="formDiagonalRadios"
                                    id="formHorizontalRadios1"
                                    timeId={time.timeId}
                                    date={time.date}
                                    onChange={handleChangeTime}
                                />

                            })}
                        </Form.Group>

                    </Form.Row>
                    <Row>

                        <Col className="text-left">

                        </Col>
                    </Row>


                    {!appointment ? <Form.Row>
                        <Col md={5} className="ml-auto mr-auto pb-5">
                            {time && barberId && date && gender && <p>All fields were selected. Now click on create appointment</p>}
                            <Button type="submit" color='facebook'>
                                Create Appointment
                            {loading && <Spinner animation="border" size="sm" variant="light" />}
                            </Button>
                            {message && <Alert variant="danger" className="mt-3">Cannot create appoinment. No time available</Alert>}
                        </Col>
                    </Form.Row> : <h3>Cannot schedule. You already have an appointment</h3>}

                </Form>
            </Col>


        </Container>


    </div>

}

export default CreateAppointment
