import React,{ useState,useEffect } from 'react'
import Navigation from '../home/Navigation'
import { Button } from 'semantic-ui-react'
import { Form,Container,Row,Col, Spinner } from 'react-bootstrap'
import { useParams,useHistory } from 'react-router-dom'
import AppointmentApi from '../api/AppointmentApi'
import CONSTANT from '../utils/CONSTANT'
import TimeApi from '../api/TimeApi'


const UpdateAppointment = () => {

    const [gender,setGender] = useState();
    const [email,setEmail] = useState();
    const [date,setDate] = useState();
    const [time,setTime] = useState();
    const [barberName,setBarberName] = useState();
    const {appointmentId,barberId} = useParams();
    const history = useHistory();
    const [loading,setLoading] = useState(false);
    const [logout,setLogout] = useState(false);
    const [timeId,setTimeId] = useState();
    const [times,setTimes] = useState([]);



    useEffect(() => {
        const saveLogout = localStorage.getItem(CONSTANT.LOCAL_STORAGE.LOGOUT);
        if (saveLogout) {
            setLogout(JSON.parse(saveLogout));
        }
        AppointmentApi.getAppointmentById(appointmentId).then(appointment => {
            setGender(appointment.gender);
            setDate(appointment.date);
            setTime(appointment.time);
            setBarberName(appointment.barberName);
            setTimeId(appointment.timeId)
            setEmail(appointment.email)

        })

        TimeApi.getAllTimesByBarberId(barberId).then(times => setTimes(times));

    },[appointmentId,barberId])


    const handleUpdateAppointment = () => {
        
        AppointmentApi.updateAppointment(appointmentId,date,time,timeId,gender,barberName, barberId, email).then(updateAppointment => {
            if(updateAppointment){
                TimeApi.deleteTimeByTimeId(timeId).then(time => {
                    if(time){
                       return history.push("/userresponse/" + updateAppointment.userId);
                    }
                })
            }


        })
    }

    const handleUpdateButton = (event) => {
        event.preventDefault();
        setLoading(true)
        setTimeout(handleUpdateAppointment,1000);
    }

    const handleTimeChange = (event) => {
        const id = event.target.getAttribute("timeId")
        setTimeId(id);
        setTime(event.target.value);
    }

    if (logout) {
        history.push("/login")
    }


    return <div>
        <Navigation />

        <Container className="mt-5">
            <h2>Update Appointment</h2>

            <Row>
                <Col md={5} className="text-left ml-auto mr-auto">
                    <Form onSubmit={handleUpdateButton}>
                        <Form.Label>Gender</Form.Label>
                        <Form.Control className="mb-4" defaultValue={gender} 
                        onChange={(e) => setGender(e.target.value)} />
                        <Form.Label>Date</Form.Label>
                        <Form.Control className="mb-4" defaultValue={date} 
                        onChange={(e) => setDate(e.target.value)} />
                        <Form.Label>Time</Form.Label>
                        <Form.Control className="mb-4" defaultValue={time} 
                        onChange={(e) => setTime(e.target.value)} />

{times.length !== 0 && <Form.Row>

                            <Form.Group as={Col} controlId="formGridPassword">
                                <Form.Label>Choose Available Time. ({barberName + " is avaiable at following times"})</Form.Label>
                                {times.length !== 0 && times.map((time,index) => {
                                    return <Form.Check
                                    type="radio"
                                    key={index}
                                    required
                                    label={time.time}
                                    value={time.time}
                                    name="formDiagonalRadios"
                                    id="formHorizontalRadios1"
                                    timeId={time.timeId}
                                    onChange={handleTimeChange}
                                    />
                                })}
                            </Form.Group>
                        </Form.Row>}

                     {times.length !== 0 ?  <Button type="submit" color="brown">
                            Update
                            {loading && <Spinner animation="border" size="sm" variant="light" /> }
                            </Button> : <h3 className="text-warning">Cannot update Appointment. There is no available time.</h3>}
                    </Form>

                
                </Col>

            </Row>

            

        </Container>








    </div>
}

export default UpdateAppointment