const BASE_URL = "/api"
//const BASE_URL = "http://localhost:8080/api"

export default{
    
    URL:{
        BASE: BASE_URL,
        USER:BASE_URL + '/user',
        APPOINTMENT: BASE_URL + '/appointment',
        UPLOADFILE: BASE_URL + '/uploadFile',
        EMAIL: BASE_URL + '/sendmail',
        TIME: BASE_URL + '/time',
        BARBER: BASE_URL + '/barber'
    },

    LOCAL_STORAGE:{
        USER:'USER',
        APPOINTMENT:'APPOINTMENT',
        LOGOUT:'LOGOUT',
        USERPASSCHANGED:'USERPASSCHANGED',
        BUSPASSCHANGED:'BUSPASSCHANGED',
        APPOINTMENTCREATED:'APPOINTMENTCREATED'        
    }

}