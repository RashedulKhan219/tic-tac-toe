import React,{ useEffect,useState } from 'react';
import { Container,InputGroup,Form,FormControl,Row,Col,Button,Card,Figure,Modal,Spinner } from 'react-bootstrap';
import { useHistory,useParams } from 'react-router';
import AppointmentApi from '../api/AppointmentApi';
import BarberApi from '../api/BarberApi';
import TimeApi from '../api/TimeApi';
import Navigation from '../home/Navigation';
import CONSTANT from '../utils/CONSTANT';
import ReactTimeAgo from 'react-time-ago'
import picture from '../images/no-photo-available-black-profile.jpeg'
import { SRLWrapper } from "simple-react-lightbox";
import UploadImageApi from '../api/UploadImageApi';


const HairDresserProfile = () => {

    const { barberId } = useParams();
    const history = useHistory();
    const [times,setTimes] = useState([]);
    const [appointments,setAppointments] = useState([]);
    const [barber,setBarber] = useState();
    const [isLogout,setLogout] = useState(false);
    const [time,setTime] = useState();
    const [date,setDate] = useState();
    const [timeId,setTimeId] = useState();
    const [isButtonClicked,setButtonClicked] = useState(false);
    const [createdFile,setCreatedFile] = useState();
    const [show,setShow] = useState(false);
    const [file,setFile] = useState();
    const [loading,setLoading] = useState(false);
    const [isDeleteButton,setDeleteButton] = useState(false);
    const [appointmentId,setAppointmetId] = useState();
    const [addDateTime, setAddDateTime] = useState(false);



    useEffect(() => {
        const logout = localStorage.getItem(CONSTANT.LOCAL_STORAGE.LOGOUT);
        if (logout) {
            setLogout(JSON.parse(logout));
        }
        TimeApi.getAllTimesByBarberId(barberId).then(times => setTimes(times))
        BarberApi.getBarberById(barberId).then(barber => setBarber(barber))
        AppointmentApi.getAppointmentsByBarberId(barberId).then(appointments => setAppointments(appointments));
        UploadImageApi.getFileByUserId(barberId).then(file => setCreatedFile(file));
    },[barberId])



    const handleCreateTime = (event) => {
        event.preventDefault();
        TimeApi.createTimeByBarber(time,date,barberId,barber.firstName).then(time => {
            if (time) {
                TimeApi.getAllTimesByBarberId(barberId).then(times => setTimes(times))
                event.target.reset();
            }
        })

    }

    const handleDeleteTime = (event) => {
        event.preventDefault();
        if (timeId) {
            TimeApi.deleteTimeByTimeId(timeId).then(deletedTime => {
                if (deletedTime) {
                    TimeApi.getAllTimesByBarberId(barberId).then(times => setTimes(times))
                }
            })
        }

    }

    const handleUpdate = () => {
        history.push("/updatebarber/" + barberId);
    }

    const handleCheckAppointmentListButton = () => {
        if (isButtonClicked === false) {
            setButtonClicked(true);
        } else {
            setButtonClicked(false);
        }
    }

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    const uploadImage = () => {
        if (file) {
            const formData = new FormData();
            formData.append("file",file);
            UploadImageApi.createFile(formData,barberId).then(file => {
                if (file) {
                    setCreatedFile(file);
                    UploadImageApi.getFileByUserId(barberId).then(file => {
                        setCreatedFile(file);
                        setLoading(false);
                        handleClose();
                    })
                }
            })
        }
    }

    const handleSubmitImageButton = (event) => {
        event.preventDefault();
        setLoading(true);
        setTimeout(uploadImage,1000);
    }

    const handleChangeDelete = (event) => {
        handleShow();
        setDeleteButton(true);
        setAppointmetId(event.target.value);
    }

    const handleChangeUploadPhoto = () => {
        handleShow();
        setDeleteButton(false);
    }

    const handleDeleteAppoitment = () => {
        handleClose();
        if (appointmentId) {
            AppointmentApi.deleteAppointment(appointmentId).then(deletedAppointment => {
                if (deletedAppointment) {
                    setLoading(false);
                    setShow(false);
                    AppointmentApi.getAppointmentsByBarberId(barberId).then(appointments => setAppointments(appointments));
                }
            })
        }

    }

    const handleDeleteButton = (event) => {
        event.preventDefault();
        handleClose();
        setLoading(true);
        setTimeout(handleDeleteAppoitment,1000);
    }


    const handleAddDateTime = () => {
        if(addDateTime === false){
            setAddDateTime(true);
        } else {
            setAddDateTime(false);
        }
    }


    if (isLogout) {
        history.push("/businesshome")
    }

    return <div >
        <Navigation />

        <Container className="mt-5 pb-5">
            <h1 >Welcome to Merry Hair Cutting</h1>
            <Row className="mt-5">

                {barber && <Col md={4} sm={6} className="text-left mb-5" >

                    <SRLWrapper>
                        <Figure>
                            <Row>
                                <Col md={8} className="text-left">
                                    <Figure.Image
                                        className="rounded"
                                        style={{ cursor: 'pointer' }}
                                        width={100}
                                        height={100}
                                        alt="171x180"
                                        src={createdFile ? CONSTANT.URL.UPLOADFILE + "/user/" + barberId : picture}
                                        thumbnail
                                    />
                                </Col>
                                <Col md={8} className="text-left">
                                    <Button onClick={handleChangeUploadPhoto} variant="info">Upload photo</Button>
                                </Col>
                            </Row>
                        </Figure>
                    </SRLWrapper>


                    <h2>Hair-dresser Information</h2>
                    <p>FirstName: {barber.firstName}</p>
                    <p>LastName: {barber.lastName}</p>
                    <p>Phone: {barber.phone}</p>
                    <p>Email: {barber.email}</p>
                    <Button variant="outline-primary text-light" onClick={handleUpdate}>
                        Update info
                        </Button>


                </Col>}
                <Col sm={6} md={4} className="text-left pb-5" >
                <Button onClick={handleAddDateTime} variant="primary mt-3 mb-3">Add date & time</Button>
                  {addDateTime &&  <Form onSubmit={handleCreateTime}>

                        <Form.Label>Add a time</Form.Label>
                        <InputGroup >

                            <FormControl
                                required
                                type="text"
                                placeholder="Enter a time"
                                onChange={(e) => setTime(e.target.value)}
                            />

                        </InputGroup>
                        <br />
                        <Form.Label>Select a date</Form.Label>
                        <InputGroup >
                            <FormControl
                                required
                                type="date"
                                placeholder="Enter a time"
                                onChange={(e) => setDate(e.target.value)}
                            />
                        </InputGroup>

                        <Button type="submit" variant="primary mt-3">Submit</Button>

                    </Form>}
                    {times.length !== 0 && <div className="mt-4">
                        <Form onSubmit={handleDeleteTime}>
                            <h3>Recently added date & times</h3>
                            <h3>Date & times</h3>
                            {times.map((time,index) => {
                                return <div>

                                    <Form.Check
                                        className="mb-2"
                                        type="radio"
                                        label={ time.date + " & " + time.time}
                                        value={time.timeId}
                                        name="formHorizontalRadios"
                                        id="formHorizontalRadios3"
                                        key={index}
                                        required
                                        onChange={(e) => setTimeId(e.target.value)}
                                    />
                                </div>
                            })}
                            <hr />
                            <Button type="submit" variant="outline-danger text-light">
                                Delete time
                        </Button>
                            <p className="mt-2">Select a date&time that you want to delete</p>
                        </Form>
                    </div>}

                </Col>

                <Col md={4}>
                    <Button variant="info" onClick={handleCheckAppointmentListButton}>Check AppointmentList</Button>
                    <hr />
                    {isButtonClicked && appointments.length !== 0 && appointments.map((appointment,index) => {
                        return <div key={index} className="border border-light text-left mb-3">
                            <Card.Header>Date & time: {appointment.date + " & " + appointment.time}</Card.Header>
                            <Card.Body>
                                <p>Appointment-ID: {appointment.appointmentId}</p>
                                <p>Customer-Email: {appointment.email}</p>
                                <p>Customer-ID: {appointment.userId}</p>
                                <p>Hair-dresser: {appointment.barberName}</p>
                                <p>Services: {appointment.services}</p>
                                <p>
                                    Appointment was scheduled <ReactTimeAgo date={appointment.localDateTime} locale="en-US" />
                                </p>
                                <div className="text-right">
                                    <Button onClick={handleChangeDelete} value={appointment.appointmentId} variant="danger">
                                        Delete appointment
                                    {loading && appointmentId === appointment.appointmentId && <Spinner animation="border" size="sm" variant="light" />}
                                    </Button>
                                </div>

                            </Card.Body>
                        </div>
                    })}
                    {isButtonClicked && appointments.length === 0 && <p>No appointments available</p>}
                </Col>


            </Row>



            <Modal
                show={show}
                onHide={handleClose}
                backdrop="static"
                keyboard={false}
                centered
                className='bg-dark'
            >
                <Modal.Header closeButton>
                    {isDeleteButton ? <Modal.Title>Delete appointment</Modal.Title> :
                        <Modal.Title>Upload an image</Modal.Title>}
                </Modal.Header>
                {isDeleteButton ? <Modal.Body>
                    Do you want to delete this appointment ?
                </Modal.Body> :
                    <Modal.Body>
                        Do you want to upload an image ?
              </Modal.Body>}

                <Modal.Footer>
                    {isDeleteButton ? <Button type="submit" variant="danger" onClick={handleDeleteButton}>
                        Delete
                        </Button> : <div><Form onSubmit={handleSubmitImageButton}>
                        <input
                            type="file"
                            required
                            accept="image/*"
                            multiple={false}
                            id="upload-button"
                            onChange={(e) => setFile(e.target.files[0])}
                        />
                        <Button type="submit" variant="primary">
                            Submit
                            {loading && <Spinner animation="border" size="sm" variant="light" />}
                        </Button>

                    </Form>
                    </div>}

                </Modal.Footer>
            </Modal>

        </Container>
    </div>
}
export default HairDresserProfile