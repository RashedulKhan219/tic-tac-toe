import React, { useState, useEffect } from 'react'
import Navigation from '../home/Navigation'
import { Container, Form, Button, Spinner } from 'react-bootstrap'
import { useParams, useHistory } from 'react-router-dom';
import { Card } from 'semantic-ui-react';
import CONSTANT from '../utils/CONSTANT';
import BarberApi from '../api/BarberApi';



const UpdateBarber =()=>{

    const {barberId} = useParams();
    const [firstName, setFirstName] = useState();
    const [lastName, setLastName] = useState();
    const [email, setEmail] = useState();
    const [phone, setPhone] = useState();
    const history = useHistory();
    const [loading, setLoading] = useState(false);
    const [logout, setLogout] = useState(false);

    useEffect(()=>{

        const saveLogout = localStorage.getItem(CONSTANT.LOCAL_STORAGE.LOGOUT);
        if(saveLogout){
            setLogout(JSON.parse(saveLogout));
        }
       BarberApi.getBarberById(barberId).then(barber => {
            setFirstName(barber.firstName);
            setLastName(barber.lastName);
            setEmail(barber.email);
            setPhone(barber.phone);
        })

    },[barberId])

    const handleUpdate =()=>{
        BarberApi.updateBarber(barberId, firstName, lastName, phone, email).then( updateBarber => {
            localStorage.setItem(CONSTANT.LOCAL_STORAGE.USER,JSON.stringify(updateBarber));
            if(updateBarber.role === 'BARBER'){
                setLoading(false);
              return  history.push("/hairdresserprofile/" + updateBarber.barberId);
            } else if(updateBarber.role === 'ADMIN'){
                setLoading(false);
              return  history.push("/businessprofile/" + updateBarber.barberId);
            }
        })

    }

    const handleSaveChangesButton=(event)=>{
        event.preventDefault();
        setLoading(true);
        setTimeout(handleUpdate, 1000);

    }
    
if(logout){
    history.push("/businesshome");
} 

    return <div>
    <Navigation/>
    <Container >
        <div className="row">
        <Card className="col-lg-4 ml-auto mr-auto p-5 mt-5 text-left" style={{backgroundColor:'rebeccapurple'}}>
        <Form onSubmit={handleSaveChangesButton} disabled={logout}>
        <h1 className="text-center">update</h1>
            <Form.Label>FirstName:</Form.Label>
            <Form.Control type="text" defaultValue={firstName} className="text-capitalize mb-2" required
            onChange={(e) => setFirstName(e.target.value)}/>
             <Form.Label>LastName:</Form.Label>
             <Form.Control type="text" defaultValue={lastName} className="text-capitalize mb-2" required
              onChange={(e) => setLastName(e.target.value)} /> 
               <Form.Label>Phone:</Form.Label>
             <Form.Control type="text" defaultValue={phone} className="mb-2"  required
              onChange={(e) => setPhone(e.target.value)}/>
               <Form.Label>Email:</Form.Label>
             <Form.Control type="email" defaultValue={email} className="mb-4" required
              onChange={(e) => setEmail(e.target.value.toLowerCase())}/>
              <Button type='submit' disabled={logout} className="col-lg" variant="info">
                  Save all changes
              {loading && <Spinner animation='border' size='sm' variant='white' /> }
              </Button>

        </Form>
        </Card>

        </div>
        
    </Container>

</div>
}
export default UpdateBarber