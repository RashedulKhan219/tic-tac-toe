import axios from 'axios'
import CONSTANT from '../utils/CONSTANT'

class TimeApi {

    static url = CONSTANT.URL.TIME;

    static getAllTimes() {
        return axios.get(this.url).then(response => response.data);
    }

    static getAllTimesByBarberId(barberId) {
        return axios.get(this.url + "/" + barberId).then(response => response.data)
    }

    static createTimeByBarber(time, date, barberId,barberName) {
        const payLoad = { time:time, date:date, barberId:barberId, barberName:barberName }
        return axios.post(this.url,payLoad).then(response => response.data);
    }

    static deleteTimeByTimeId(timeId) {
        return axios.delete(this.url + "/" + timeId).then(response => response.data);
    }

}
export default TimeApi