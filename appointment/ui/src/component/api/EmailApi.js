import axios from 'axios'
import CONSTANT from '../utils/CONSTANT'

class EmailApi {
    static url = CONSTANT.URL.EMAIL;

    static getEmailTokenByUserEmail(email, userId){
        return axios.get(this.url + "/" + email + "/" + userId).then(response => response.data)
    }

    static verifyEmailToken(emailTokenId){
        return axios.put(this.url + "/" + emailTokenId).then(response => response.data)
    }


}

export default EmailApi