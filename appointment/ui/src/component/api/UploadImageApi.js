import axios from 'axios'
import CONSTANT from '../utils/CONSTANT'

class UploadImageApi{

    static url = CONSTANT.URL.UPLOADFILE;

    static getAllFile(){
        return axios.get(this.url).then(response => response.data)
    }

    static getFileById(fileId){
        return axios.get(this.url + "/" + fileId).then(response => response.data)
    }

    static getFileByUserId(userId){
        return axios.get(this.url + "/user/" + userId).then(response => response.data)
    }

    static createFile(file, userId){
        return axios.post(this.url + "/" + userId, file).then(response => response.data);
    }

    static updateFile(fileId, file){
        return axios.put(this.url + "/" + fileId, file).then(response => response.data)
    }


}

export default UploadImageApi