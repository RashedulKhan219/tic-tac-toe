import axios from 'axios'
import CONSTANT from '../utils/CONSTANT'

class BarberApi {

    static url = CONSTANT.URL.BARBER;

    static createBarberProfile(firstName,lastName,phone,email,password,role) {
        const payLoad = {
            firstName: firstName,lastName: lastName,phone: phone,
            email: email,password: password,role: role, currentRole: role
        }
        return axios.post(this.url,payLoad).then(response => response.data);
    }

    static getBarberById(barberId) {
        return axios.get(this.url + "/" + barberId).then(response => response.data)
    }

    static getAllBarber() {
        return axios.get(this.url).then(response => response.data);
    }


    static login(email,password) {
        return axios.get(this.url + "/" + email + "/" + password).then(response => response.data)
    }
    
    static updateBarber(barberId,firstName, lastName, phone, email) {
        const payLoad = {firstName: firstName, lastName: lastName, phone: phone, email: email}
        return axios.put(this.url + "/" + barberId, payLoad).then(response => response.data);
    }

    static resetPassword(password, barberId) {
        return axios.put(this.url + "/" + password + "/" + barberId).then(response => response.data);
    }

    static deleteBarberProfile( barberId){
        return axios.delete(this.url + "/" + barberId).then(response => response.data)
    }

    static validateEmailToGetToken(email){
        return axios.get(this.url + "/email/" + email).then( response => response.data);
    }

}

export default BarberApi