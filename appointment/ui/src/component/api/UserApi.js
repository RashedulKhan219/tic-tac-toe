import axios from 'axios'
import CONSTANT from '../utils/CONSTANT'

class UserApi {

    static url = CONSTANT.URL.USER

    static getAllUser() {
        return axios.get(this.url).then(response => response.data);
    }

    static createUser(firstName,lastName,phone,email,password, role) {
        const payLoad = { firstName: firstName,lastName: lastName,phone: phone,email: email,password: password, role: role}
        return axios.post(this.url,payLoad).then(response => response.data);
    }

    static getUserById(userId){
        return axios.get(this.url + "/" + userId).then(response => response.data);
    }

    static getUserByEmail(email){
        return axios.get(this.url + "/" + email).then(response => response.data);
    }
    static getPastAllAppointments(userId){
        return axios.get(this.url + "/history/" + userId).then(response => response.data);
    }

    static loginUser(email, password){
        return axios.get(this.url +"/" + email + "/" + password).then(response => response.data)
    }

    static updateUser(userId, firstName, lastName, phone, email){
        const payLoad = {firstName: firstName, lastName:lastName, phone:phone, email: email};
        return axios.put(this.url + "/" + userId, payLoad).then(response => response.data);
    }

    static resetPassword(password, userId){
        return axios.post(this.url + "/" + password + "/" + userId).then(response => response.data);
    }

    static validateEmailToGetToken(email){
        return axios.get(this.url + "/email/" + email).then( response => response.data);
    }

    static getAppointmentListByUserId(userId){
        return axios.get(this.url + "/appointmentList/" + userId).then(response => response.data);
    }

}

export default UserApi