import axios from 'axios'
import CONSTANT from '../utils/CONSTANT'

class AppointmentApi{

static url = CONSTANT.URL.APPOINTMENT

static createAppointment(date, time, timeId, gender,barberName,barberId, email,services, userId){
    const payLoad ={date:date, time:time, timeId: timeId, gender:gender,barberName: barberName,barberId: barberId, email: email,
        services: services}
    return axios.post(this.url + "/" + userId, payLoad).then(response => response.data)
}

static getAllAppointment(){
    return axios.get(this.url).then(response => response.data);
}

static getAppointmentById(appointmentId){
    return axios.get(this.url + "/" + appointmentId).then(response => response.data);
}

static getAppointmentByUserId(userId){
    return axios.get(this.url + "/user/" + userId).then(response => response.data);
}

static getAppointmentByEmail(email){
    return axios.get(this.url + "/email/" + email).then(response => response.data);
}

static getAppointmentsByBarberId(barberId){
    return axios.get(this.url + "/appointmentList/" + barberId).then(response => response.data);
}

static updateAppointment(appointmentId,date,time,timeId,gender,barberName, barberId, email){
    const payLoad = {date: date, time: time,timeId:timeId, gender: gender, barberName: barberName, barberId: barberId, email: email}
    return axios.put(this.url + "/" + appointmentId, payLoad).then(response => response.data);
}

static deleteAppointment(appointmentId){
    return axios.delete(this.url + "/" + appointmentId).then(response => response.data);
}

}
export default AppointmentApi