
import React,{ useEffect,useState } from 'react'
import { Navbar,Nav,Modal,Button,Form,FormControl,Alert,Spinner } from 'react-bootstrap'
import { Link,useHistory } from 'react-router-dom'
import { Icon } from 'semantic-ui-react';
import AppointmentApi from '../api/AppointmentApi';
import CONSTANT from '../utils/CONSTANT';

const Navigation = () => {

    const [user,setUser] = useState();
    const history = useHistory();
    const [logout,setLogout] = useState(false);
    const [show,setShow] = useState(false);
    const [appointmentId,setAppointmentId] = useState();
    const [loading,setLoading] = useState(false);
    const [alert,setAlert] = useState(false);


    useEffect(() => {
        const localUser = localStorage.getItem(CONSTANT.LOCAL_STORAGE.USER);
        const saveLogout = localStorage.getItem(CONSTANT.LOCAL_STORAGE.LOGOUT);

        if (localUser) {
            setUser(JSON.parse(localUser));

        }
        if (saveLogout) {
            setLogout(JSON.parse(saveLogout));
        }
    },[])

    const handleSearch = () => {
        AppointmentApi.getAppointmentById(appointmentId).then(appointment => {
            if (appointment) {
                setLoading(false)
                return history.push("/existingappointment/" + appointment.appointmentId + "/" + appointment.userId);
            } else if(!appointment){
                setLoading(false);
                return setAlert(true);
            }


        })
    }

    const handleSearchButton = (event) => {
        event.preventDefault();
        setLoading(true)
        setTimeout(handleSearch,1000);

    }

    const handleLogout = () => {
        localStorage.setItem(CONSTANT.LOCAL_STORAGE.USER,JSON.stringify(null));
        localStorage.setItem(CONSTANT.LOCAL_STORAGE.LOGOUT,JSON.stringify(true));
        if (user && user.role === "USER") {
            setUser(null);
            return history.push("/login");
        } else {
            setUser(null);
            return history.push("/businesshome")
        }


    }


    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);


    return <div>

        <Navbar expand="lg" variant="dark" bg="dark" className="p-3" >

            <Link className="nav-link  text-white" to="/" >
                <h1 className="col-lg font-weight-bolder text-light">
                    <Icon name="text telephone" size="large" color='red' />
                    APPOINTMENT
                </h1>
            </Link>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="ml-auto">
                    {!user && <Link className="nav-link text-white App-link rounded" to="/" >Home</Link>}
                    {!user && <Link className="nav-link text-white App-link mr-2 rounded" to="/businesshome"> Business </Link>}
                    {!user && <Link className="nav-link text-white App-link rounded" to="/signup" >SignUp</Link>}
                    {!user && <Link className="nav-link text-white App-link rounded mr-2" to="/login"> Login</Link>}
                    {user && !logout && user.role === 'ADMIN' &&
                        <Link className="nav-link text-white App-link rounded" to="/userlist"> UserList</Link>}
                    {user && !logout && user.role === 'ADMIN' &&
                        <Link className="nav-link text-white App-link rounded" to="/appointmentlist"> AppointmentList</Link>}
                    {user && !logout && user.role === 'USER' &&
                        <Link to={"/checkhistory/" + user.userId} className="nav-link text-light text-capitalize" >Check history</Link>}
                    {user && !logout &&
                        <Link style={{ color: 'orchid' }}
                            to={user.role === 'ADMIN' ? "/businessprofile/" + user.barberId : user.role === 'BARBER' ?
                                "/hairdresserprofile/" + user.barberId : "/userresponse/" + user.userId}
                            className="nav-link  text-capitalize" >{user.firstName}</Link>}
                    {user && !logout && <Link onClick={handleShow} className="nav-link text-warning" >
                        <Icon name='sign-out' />
                        Logout</Link>}

                    {!user && <Form inline onSubmit={handleSearchButton}>
                        <FormControl type="text" placeholder="Enter appointment-ID" className="mr-sm-2" required
                            onChange={(e) => setAppointmentId(e.target.value)} />
                        <Button type="submit" variant="outline-primary">
                            Search
                            {loading && <Spinner animation="border" size="sm" variant="light" />}
                        </Button>
                    </Form>}

                </Nav>

            </Navbar.Collapse>
           
        </Navbar>
        {alert && <Alert  variant="danger" className="col-lg-3 ml-auto" dismissible
            onClose={() => setAlert(false)}>
            Invalid ID. Enter a valid ID.
                    </Alert>}

        <Modal show={show} onHide={handleClose} centered>
            <Modal.Header closeButton>
                <Modal.Title className="text-capitalize">{user && user.firstName + " " + user.lastName}</Modal.Title>
            </Modal.Header>
            <Modal.Body>Are you sure you want to log out?</Modal.Body>
            <Modal.Footer>
                <Button variant="primary" onClick={handleClose}>
                    Cancel
          </Button>
                <Button variant="danger" onClick={handleLogout}>
                    Log out
          </Button>
            </Modal.Footer>
        </Modal>

    </div>
}

export default Navigation