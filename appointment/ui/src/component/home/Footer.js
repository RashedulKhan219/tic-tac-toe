import React from 'react'
import { Icon } from 'semantic-ui-react';
import { Button } from 'semantic-ui-react'

const Footer = () => {


    return <footer className="bg-dark text-light p-5 absolute">

        <div className="main-footer">
            <div className="container">

                <div className="row mt-4">

                    <div className="col-md-4 col-sm-6">
                        <h4>Location</h4>
                        <ul className="list-unstyled">
                            <li>167th Street, Jamaica</li>
                            <li>New York, NY</li>
                        </ul>

                    </div>

                    <div className="col-md-4 col-sm-6">
                        <h4>Contact us</h4>
                        <ul className="list-unstyled">

                            <p>
                                <Icon name='phone volume' size='large' />
                                7182629500
                            </p>
                        </ul>

                    </div>
                    <div className="col-md-4 col-sm-6">
                        <h4>Follow us on</h4>
                        <Button className="mr-4" href='https://www.facebook.com/' circular size='large' color='facebook' icon='facebook' />
                        <Button className="mr-4" href='https://www.youtube.com/' circular color='youtube' icon='youtube' />
                        <Button className="mr-4" href='https://www.instagram.com/' circular color='instagram' icon='instagram' />
                        <Button href='https://www.google.com/' circular color='google plus' icon='google' />
                    </div>


                </div>

                <div className="row mt-4 mb-4">
                    <div className="col-md-3 col-sm-6 ml-auto mr-auto">

                        <iframe
                            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d445.23885258326914!2d-73.79593634270462!3d40.
                            70995747226013!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c260e03ca6b641%3A0x34f9ed19741a0f88!
                            2sHAQUE%20AKHTER%20ASSOCIATES%20INC.!5e0!3m2!1sen!2sus!4v1619992106365!5m2!1sen!2sus"
                            width="200"
                            height="100"
                            title="map"
                            frameBorder="0"
                            allowFullScreen=""
                            aria-hidden="false"
                            tabIndex="0">
                        </iframe>
                    </div>
                </div>


                <div className="footer-bottom">
                    <p className="text-xs-center"></p>
                    &copy;Copyright {new Date().getFullYear()} - All Rights Reserved

                    </div>

            </div>

        </div>


    </footer>

}
export default Footer