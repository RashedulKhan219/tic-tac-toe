import React from 'react'
import { Carousel } from 'react-bootstrap'
import picture from '../images/NewYorkCity-4.jpeg'
import picture2 from '../images/appointment.png'
import picture3 from '../images/DSC04470.jpeg'
import picture4 from '../images/ny.jpeg'
import picture5 from '../images/shop.jpeg'

const MyCarousel = () => {
  return <Carousel>

    <Carousel.Item interval={1000}>
      <img
        className="d-block w-100"
        height={350}
        src={picture}
        alt="First slide"
      />

      <Carousel.Caption>

        <h3>Schedule appointment @ anytime</h3>
        <p>New York City. A city of dreams</p>
      </Carousel.Caption>
    </Carousel.Item>
    <Carousel.Item interval={500}>
      <img
        className="d-block w-100"
        height={350}
        src={picture2}
        alt="Third slide"
      />
      <Carousel.Caption>
        <h3>Don't make a wish. Make an appointment</h3>
      </Carousel.Caption>
    </Carousel.Item>
    <Carousel.Item>
      <img
        className="d-block w-100"
        height={350}
        src={picture3}
        alt="Third slide"
      />
      <Carousel.Caption>
        <h3>Great services with cheaper rate.</h3>
      </Carousel.Caption>
    </Carousel.Item>
    <Carousel.Item interval={1000}>
      <img
        className="d-block w-100"
        height={350}
        src={picture4}
        alt="First slide"
      />

      <Carousel.Caption>

        <h3>Schedule appointment @ anytime</h3>
        <p>New York City. A city of dreams</p>
      </Carousel.Caption>
    </Carousel.Item>
    <Carousel.Item interval={500}>
      <img
        className="d-block w-100"
        height={350}
        src={picture5}
        alt="Third slide"
      />
      <Carousel.Caption>
        <h3>Don't make a wish. Make an appointment</h3>
      </Carousel.Caption>
    </Carousel.Item>
    <Carousel.Item>
      <img
        className="d-block w-100"
        height={350}
        src={picture3}
        alt="Third slide"
      />
      <Carousel.Caption>
        <h3>Great services with cheaper rate.</h3>
      </Carousel.Caption>
    </Carousel.Item>
  </Carousel>

}

export default MyCarousel