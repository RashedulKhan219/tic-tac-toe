import React,{ useEffect,useState } from 'react'
import Navigation from './Navigation'
import { useParams,useHistory } from 'react-router-dom'
import AppointmentApi from '../api/AppointmentApi';
import { Alert } from 'react-bootstrap';
import { Card } from 'semantic-ui-react';
import UserApi from '../api/UserApi';

const ExistingAppointment = () => {
    const { appointmentId,userId } = useParams();
    const [appointment,setAppointment] = useState();
    const [user,setUser] = useState();
    const history = useHistory();

    useEffect(() => {
        UserApi.getUserById(userId).then(user => setUser(user));
        AppointmentApi.getAppointmentById(appointmentId).then(appointment => setAppointment(appointment))
    },[appointmentId,userId])

    const handleClose = () => {
        history.push("/")
    }

    return <div>
        <Navigation />
        <div className="container">
            {appointment && user && <Card className='col-md-4 col-sm-6 ml-auto mr-auto text-dark mt-5'>
                <Alert variant='success' onClose={handleClose} dismissible>Appointment information</Alert>

                <Card.Content className="text-left">
                    <p>FirstName: {user.firstName} </p>
                    <p>LastName: {user.lastName} </p>
                    <p>Gender: {appointment.gender}</p>
                    <p>Date: {appointment.date} </p>
                    <p>Time: {appointment.time}</p>
                    <p>Hair-dresser: {appointment.barberName}</p>
                    <p>Services: {appointment.services}</p>
                </Card.Content>
            </Card>}
        </div>
    </div>
}

export default ExistingAppointment