import React from 'react';
import { Button, Container } from 'react-bootstrap';
import { useHistory } from 'react-router';

const PageNotFound = () =>{

    const history = useHistory();

    const handleButton =()=>{
        history.push("/");
    }

    return <div>
        <Container className="pt-5">
            <h2>404 Page not found</h2>
            <Button onClick={handleButton}>{'<<--' } Go back to home </Button>
        
        </Container>
    </div>
}

export default PageNotFound