import React,{ useEffect,useState } from 'react'
import { Container,FormControl,Spinner,Form,Alert,Row,Col,InputGroup,Card,Button } from 'react-bootstrap'
import { useHistory} from 'react-router-dom'
import Navigation from './Navigation';
import MyCarousel from './MyCarousel';
import AppointmentApi from '../api/AppointmentApi';
import CONSTANT from '../utils/CONSTANT';



const Home = () => {

    const history = useHistory();
    const [email,setEmail] = useState();
    const [loading,setLoading] = useState(false);
    const [appointment,setAppointment] = useState();
    const [alert,setAlert] = useState(false);
    const [user,setUser] = useState();

    useEffect(() => {

        const localUser = localStorage.getItem(CONSTANT.LOCAL_STORAGE.USER);
        if (localUser) {
            setUser(JSON.parse(localUser));
        }
    },[])


    const handleCheckAppointment = () => {
        AppointmentApi.getAppointmentByEmail(email).then(appointment => {
            setAppointment(appointment)
            setLoading(false);
            if (appointment.appointmentId) {
                history.push("/existingappointment/" + appointment.appointmentId + "/" + appointment.userId)
            }
        })
        if (!appointment) {
            setAlert(true);
            setLoading(false)
        }

    }


    const handleSearchButton = (event) => {
        event.preventDefault();
        setLoading(true);
        setTimeout(handleCheckAppointment,1000);
        event.target.reset();
    }

    const handleLogin = () => {
        if (user && user.role === 'ADMIN') {
            history.push("/businessprofile/" + user.barberId);
        } else if (user && user.role === 'BARBER') {
            history.push("/hairdresserprofile/" + user.barberId);
        } else if (user && user.role === 'USER') {
            history.push("/userresponse/" + user.userId);
        } else {
            history.push("/login")
        }
    }


    return <div>
        <Navigation />
        <Container className="mt-5">

            <Row>
                <Col sm={6} md={8} className="ml-auto mr-auto shadow-lg"  >
                    <Card.Footer className="p-5">
                        <h2 className="text-uppercase">Schedule Appointment</h2>
                        <p >@Anytime</p>
                        <Button style={{ backgroundColor: 'purple' }} onClick={handleLogin}  >BOOK NOW</Button>
                        <hr />

                        <p>Create an account before you schedule an appointment</p>
                        <p>
                            Already have an account ?
                        <Button
                        variant='outline'
                          onClick={handleLogin}
                                className="text-warning">
                                Login
                        </Button>
                        </p>
                    </Card.Footer>

                </Col>

            </Row>

            <Row>
                <Col sm={4} className="ml-auto mr-auto mt-5">
                    <Form onSubmit={handleSearchButton} >
                        <p>Check existing appointment by email.</p>
                        <InputGroup className="mb-3">
                            <FormControl type="email" required placeholder="Enter email address" className="rounded mr-sm-2"
                                onChange={(e) => setEmail(e.target.value.toLowerCase())} />
                            <InputGroup.Append>
                                <Button type="submit" className="rounded" variant="info">
                                    search
                            {loading && <Spinner animation="border" size="sm" variant="white" />}
                                </Button>

                            </InputGroup.Append>
                        </InputGroup>

                    </Form>
                </Col>
            </Row>


            {alert && <Alert variant="danger" dismissible onClose={() => setAlert(false)} className='col-md-4 col-sm-6 ml-auto mr-auto'>
                Appointment doesn't exist in our record's.
                    </Alert>}

            <Row className="mt-5"  >
                <Col sm={6} md={8} className="ml-auto mr-auto">
                    <MyCarousel />
                </Col>

            </Row>


            <Row className="pb-5" >
                <Col sm={8} className=" ml-auto mr-auto  mt-5" >
                    <h1 className=" font-weight-bolder border-bottom border-light text-light pb-3">Beauty-Quote And Descriptions </h1>
                    <p className=" mt-5 text-left " >
                        Beauty is the ascription of a property or characteristic to a person, object, animal, place
                        or idea that provides a perceptual experience of pleasure or satisfaction. Beauty is studied
                        as part of aesthetics, culture, social psychology and sociology.
                        Let that beauty shine into the world, yet always be okay with closing the door when you
                        need solitude; even the daisies rests at night, their petals folded inward. So let it show,
                        be bold, chase your dreams with everything you have - because that is your beauty too.
                        Your thoughts, your creative spark, the way you dance when no-one is watching - it is beautiful...
                        you are beautiful. All I ask is that when the storms come you always remember to open up those delicate
                        petals again and feel the gentle rays of the sun.
                    </p>
                </Col>

            </Row>


        </Container>
    </div>

}

export default Home